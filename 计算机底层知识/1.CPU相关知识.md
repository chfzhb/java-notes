# CPU相关知识

<img src="../../java-notes/img/计算机的组成.png">

计算中最主要的就是 `CPU` 和 `内存`。`CPU` 通过 `总线`(各种总线)读取内存中的数据。


## CPU的组成

`CPU` 包含以下组件

- **程序计数器 - PC - Program Counter**：程序计数器是用于存放下一条指令所在单元的地址的地方。

- **寄存器 - Registers**：寄存器是CPU内部用来存放数据的一些小型存储区域，用来暂时存放指令、数据和位址。寄存器一般有几十个,它们各自有着不同的作用。

- **算术逻辑单元 - ALU** ：算术逻辑单元(arithmetic and logic unit) 是能实现多组算术运算和逻辑运算的组合逻辑电路，简称ALU。

- **缓存 - Cache**：一般分为L1、L2缓存,在多核CPU中还有L3缓存，L3是多核共享的，而L1、L2是独享的。缓存的出现是为了解决，CPU速度太快而内存太慢所造成的CPU经常等待内存的现象。当CPU进行计算时首先会到寄存器中查找数据，没有则去L1中查找，在没有则去L2中查找直至查找到主存。下次CPU使用数据则不需要到内存中查找了，这样CPU的等待时间就减少了。


当执行一条指令时,`CPU`会进行以下操作-[转载自](<https://blog.csdn.net/jizhu4873/article/details/84393905?depth_1-utm_source=distribute.pc_relevant.none-task&utm_source=distribute.pc_relevant.none-task>)：

1. **取指令**

    把`PC`的指令地址装入地址寄存器（AR），送上地址总线，由地址总线找到对应主存单元，取出指令码，然后通过数据总线将指令码传给`CPU`的指令寄存器（IR）。

    <img src="../../java-notes/img/取指令.jpg">

2. **分析/译码**

    把`IR`中的指令交给译码器译码，识别出这是一条`AC`与主存单元相加的指令，将指令中的`EA`（汇编指令的偏移地址）输出到地址总线，地址总线在加法指令的控制下从主存单元读出数据，然后将读出的数据通过数据总线送到`CPU`的暂存寄存器（TR）中。

    在指令译码的同时，`PC`的值会加1，指向下一个指令地址。

    <img src="../../java-notes/img/译码.jpg">

3. **执行**

    将`AC`中的数据和`TR`中的数据传入运算中心（ALU）进行加法计算，再将计算之和通过数据总线传给`AC`，结束指令。

    <img src="../../java-notes/img/执行.jpg">

以上只列出最关键的3个步骤，其中一个指令的操作数是以直接寻址的方式给出的（EA），即译码后可立即进行访存操作。如果指令的操作数是由其他寻址方式给出，则还需要进一步取址得出EA，再由EA去存储器取值。

上面的步骤也可以这样说：

当计算机执行一条指令时，会首先从`PC`中取出指令的地址,并根据其地址从内存中获取指令存入寄存器中，然后对指令进行分许如果发现该指令需要某些数据，则从内存中取出数据放入寄存器中。然后通过 `ALU` - 算术逻辑单元 进行计算。最后将结果回写给内存。

## 储存器的层次结构

<img src="../../java-notes/img/储存器的层次结构.png">

<img src="../../java-notes/img/储存器的速度对比.png">

速度顺序如下：

寄存器 > L1 > L2 > L3 > 主存 > 硬盘

<img src="../../java-notes/img/多核CPU.png">

每个CPU的都有L1、L2缓存，多个核之间共享L3缓存。多块CPU之间共享内存。


## 超线程

在一般不支持超线程的CPU，一般是有几个核就有几个线程，而支持超线程的CPU一般是每个核支持两个线程，如双核四线程、四核八线程等。

首先线程是CPU执行的最基本单位。如果有A、B两个线程，A线程执行时PC中存储着指令，而数据存储在寄存器中使用ALU进行计算。当A线程时间片使用完之后，CPU会将A线程的指令和数据存储到内存/缓存中以便下次恢复线程使用。然后CPU执行B线程。

假如CPU中存在两组寄存器与PC，则第一组可以存储A线程的数据，第二组可以存储B线程的数据。此时ALU可以来回执行AB两个线程的运算，这就叫做超线程。

超线程: 一个ALU对应着多个PC|Registers，也就是双核四线程，四核八线程的由来。


## 缓存行

<img src="../../java-notes/img/缓存行.png">

当`CPU`想要读取`X`时，`CPU`会将`X`所在的行一起读取进来，这样做的好处还有一般程序操作完前一个变量时，还会顺序的操作下一个变量，所以下次在缓存中就能快速的找到`Y`。


`CPU`中的缓存系统是以`缓存行(cache line)`为单位储存的。缓存行是2的整数幂个连续字节，一般为32-256个字节。

缓存行越大，局部空间效率就越高，但读取时间慢。

缓存行越小，局部性空间效率越低，但读取时间快。

所以一般取一个折中值：64字节。


### 伪共享与缓存一致性

当多线程修改互相独立的变量时，如果这些变量共享同一个缓存行，就会无意中影响彼此的性能，这就是伪共享。如上图两个线程共同对`x`进行操作就出现了结果与预期出现偏差的问题。为了解决这个问题出现了缓存一致性协议 - MESI Cache一致性协议(英特尔)。其实缓存一致性协议非常多，各家CPU厂商都有自己的协议

<img src="../../java-notes/img/缓存一致性协议.png">

当一个核心修改了某个缓存行，它会将自身置为 `修改 - Modified` 状态，并写回主存，同时通过CPU的机制通知其他核心将该缓存行置为 `无效 - Invalid` 状态，此时该核心读到该缓存行无效，则会从主存中重新读取，这样得到的就是最新的数据。

如上图所示，如果A核心修改X变量，B核心修改Y变量，XY都被 volatile 修饰。由于XY变量挨得非常近，会导致两个核心都会去内存读取数据，反而会效率会变低。

CPU是如何通知核心缓存行是被修改的？

CPU内部采用了硬件实现的缓存锁，来保证缓存行一致性。当缓存无法容纳这个数据的话则使用总线锁。总线锁是最终的一个保障




### Java中如何解决缓存一致性的问题？

在代码中对变量使用 `Volatile` 关键字修饰。当对该变量进行写操作时，`JVM` 就会向处理器发送一条 `Lock` 前缀的指令来调用缓存锁，实现缓存一致性。

## 缓存行对齐

使用如下代码测试缓存行对齐

```

public class T_CacheLinePadding {

    private static class Padding {
        //此处占用8字节 * 7 = 56 byte
        public volatile long p1, p2, p3, p4, p5, p6, p7;
    }

    private static class T extends Padding {
        //该变量在加上父类的变量共占用 64个字节
        //这些数据会放到一个缓存行中
        public volatile long x = 0L;
    }

    public static T[] arr = new T[2];

    static {
        arr[0] = new T();
        arr[1] = new T();
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000_0000L; i++) {
                arr[0].x = i;
            }
        });


        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000_0000L; i++) {
                arr[1].x = i;
            }
        });

        final long start = System.nanoTime();
        t1.start();
        t2.start();
        t1.join();
        t2.join();

        System.out.println((System.nanoTime() - start) / 100_0000);
    }
}
```
运行结果
```
109
```
删去类 `T` 的继承，即删去 `extends Padding `。  
重新执行代码，结果如下。

```
501
```
### disruptor缓存行对其的实现

`disruptor` 框架中`ring buffer`指针的实现，同样是确保了指针在单独的缓存行中。

<img  src="../../java-notes/img/disruptor-指针.png">

### Java8实现字节填充避免伪共享 

按照如下两步即可避免伪共享

1. 为`JVM`设置参数 `-XX:-RestrictContended `

2. 使用 `@Contended` 注解 `java` 属性字段，自动填充字节，防止伪共享.该注解位于`sun.misc`包内。

## 指令的乱序执行

假如有1,2两条指令，指令2对指令1没有依赖关系。此时指令执行较慢cpu可能需要等地io操作，则cpu有可能去执行指令2。所以执行顺序变为2 1 ，这种情况称之为cpu的乱序执行。

<img src="../../java-notes/img/cpu乱序执行.png">

### 验证CPU的乱序执行

```
public class T_Disorder {

    private static int x = 0, y = 0;
    private static int a = 0, b = 0;
    
    public static void main(String[] args) throws InterruptedException {

        int i = 0;

        for (; ; ) {

            i++;

            x = 0;y = 0;
            a = 0;b = 0;
            Thread one = new Thread(() -> {
                //这两句没有直接关系
                //x = b 不依赖于 a = 1 的执行结果
                a = 1;
                x = b;
            });
            Thread other = new Thread(() -> {
                //同上
                b = 1;
                y = a;
            });
            one.start();
            other.start();

            one.join();
            other.join();

            String result = "第" + i + "次(" + x + ", " + y + ")";

            //当 x = 0 y = 0时，
            //也就证明one 线程中的 x = b 先于 a = 1 执行与
            //other线程中的 y = a 先于 b = 1执行。
            //所以当线程结束时 xy同时等于0则证明了CPU的乱序执行
            if (x == 0 && y == 0) {
                System.out.println("result = " + result);
                break;
            }
        }
    }
}
```
输出
```
result = 第82417次(0, 0)
```

## 合并写

在`CPU`内部还有一个寄存器叫做`WCBuffer`，它位于`ALU - 计算单元` 和 `L1 - 缓存`之间。`WCBuffer` 容量只有四个字节，当`CPU`需要将数据写回缓存时，`CPU`会先将数据写入到 `WCBuffer` 中，当 `WCBuffer` 中被写满后，在一次性地写回`L1`中。

### 使用java验证

```

public class T_WriteCombining {

    private static final int ITERATIONS = Integer.MAX_VALUE;
    private static final int ITEMS = 1 << 24;
    private static final int MASK = ITEMS - 1;

    private static final byte[] arrayA = new byte[ITEMS];
    private static final byte[] arrayB = new byte[ITEMS];
    private static final byte[] arrayC = new byte[ITEMS];
    private static final byte[] arrayD = new byte[ITEMS];
    private static final byte[] arrayE = new byte[ITEMS];
    private static final byte[] arrayF = new byte[ITEMS];

    public static void main(final String[] args) {

        for (int i = 1; i <= 3; i++) {
            System.out.println(i + " SingleLoop duration (ns) = " + runCaseOne());
            System.out.println(i + " SplitLoop  duration (ns) = " + runCaseTwo());
        }
    }

    public static long runCaseOne() {
        long start = System.nanoTime();
        int i = ITERATIONS;

        while (--i != 0) {
            int slot = i & MASK;
            byte b = (byte) i;
            arrayA[slot] = b;
            arrayB[slot] = b;
            arrayC[slot] = b;
            arrayD[slot] = b;
            arrayE[slot] = b;
            arrayF[slot] = b;
        }
        return System.nanoTime() - start;
    }

    public static long runCaseTwo() {
        long start = System.nanoTime();
        int i = ITERATIONS;
        while (--i != 0) {
            int slot = i & MASK;
            byte b = (byte) i;
            arrayA[slot] = b;
            arrayB[slot] = b;
            arrayC[slot] = b;
        }
        i = ITERATIONS;
        while (--i != 0) {
            int slot = i & MASK;
            byte b = (byte) i;
            arrayD[slot] = b;
            arrayE[slot] = b;
            arrayF[slot] = b;
        }
        return System.nanoTime() - start;
    }
}
```
结果没测试出来