
# 什么是调优

1. 根据需求进行JVM规划和预调优

1. 优化运行JVM运行环境（慢、卡顿）

1. 解决JVM运行中出现的问题 - OOM


# OOM解决

我们首先要查看哪部分产生过多的内存占用有下面几种方式

- 通过 jmap 命令查看内存中产生的实例，来分析那部分内存没有释放掉

    打印前20个实例化数量最多的对象

    > jmap -histo pid | head -20

- 通过 jmap 导出堆信息，进行离线分析

    1. 将堆以二进制(format=b)形式导出

        > jmap -dump:format=b,file=2021.03.16.hprof pid

    1. 使用 java Visual VM 进行内存分析

        ![20210316195911](http://qiniu.liulei.life/20210316195911.png)

        我们将其装入后可以查看对象所占用的空间，产生的实例数

        也可以使用 OQL 控制台，查询对象信息

        > select s from java.lang.String s

        ![20210316200201](http://qiniu.liulei.life/20210316200201.png)

    导出将堆信息会对生产产生很大的影响,不过我们可以用以下方式解决

    1. 设定参数 HeadDump，OOM 的时候会自动转储堆文件（不很专业，因为有监控，内存增长会很快）
        > -XX:HeadDumpOnOutOfMemoryError -XX:HeapDumpPath=${目录}

    1. 在高可用集群中，停掉一台服务器，进行离线分析

    1. 在线定位，但是这种需要在 防火墙/内网 打洞
        1. 使用 java Visual VM 远程连接
        1. 使用各种框架如 Spring boot Admin

##  使用阿里的 Arthas 进行调优

- dashbord：动态展示当前资源占用情况

    ![20210316214630](http://qiniu.liulei.life/20210316214630.png)

- thread：查看线程信息

    ![](https://gitee.com/superwow/pic/raw/master/20210317100649.png)

    查看具体线程，详细信息

    > thread 22 

    查看死锁

    > thread -b

    ![](https://gitee.com/superwow/pic/raw/master/20210317111541.png)

- heapdump：等同于 jmap 导出堆信息

    dump到指定文件
    > heapdump /tmp/dump.hprof

    只dump 活着的对象
    > heapdump --live /tmp/dump.hprof

- jad：查看反编译的代码
    > jad 全路径.ClassName

    ![](https://gitee.com/superwow/pic/raw/master/20210317124044.png)

- redefine：加载替换class

    > redefine path/xxx.class

- trace: 方法内部调用路径，并输出方法路径上的每个节点上耗时

    > jad ABC a

    ![](https://gitee.com/superwow/pic/raw/master/20210317130900.png)

    我们可以发现 调用 b 方法时耗时较长，继续跟踪b

    ![](https://gitee.com/superwow/pic/raw/master/20210317130959.png)

    我们跟踪到b方法时看到调用c方法时间很长，继续跟踪

    ![](https://gitee.com/superwow/pic/raw/master/20210317131104.png)

    我们看到就是c方法耗时较长



### Arthas与docker

1. 为镜像添加一下内容，随后重新构建镜像即可

    > COPY --from=hengyunabc/arthas:latest /opt/arthas /opt/arthas

1. 启动arthas-boot来进行诊断

    > docker exec -it saas-vehicle-for-andy /bin/sh -c "java -jar /opt/arthas/arthas-boot.jar"



# 当系统CPU飙高如何定位和解决问题


1. 使用top命令定位进程

    1. 如果是 java 进程 使用 arthas 挂上去，找到具体的线程

    1. 如果不是 java 进程 则使用 `top -H -p pid` 找到具体的线程

1. 确认线程是业务线程还是GC线程

    1. 业务线程：调整业务代码

    1. GC线程：一定是频繁的FullGC，查看日志
        1. 如果是正常回收，

            说明系统压力很大，可以调整垃圾回收器，或者调整内存
            
        1. 如果是非正常回收，

            说明有内存泄露，使用 arthas 查看堆快照/或者jmap等，修复代码





# 常用调优策略


## 选择合适的垃圾回收器

CPU单核，那么毫无疑问Serial 垃圾收集器是你唯一的选择。

CPU多核，关注吞吐量 ，那么选择PS+PO组合。

CPU多核，关注用户停顿时间，JDK版本1.6或者1.7，那么选择CMS。

CPU多核，关注用户停顿时间，JDK1.8及以上，JVM可用内存6G以上，那么选择G1。

参数配置
```
 //设置Serial垃圾收集器（新生代）
 开启：-XX:+UseSerialGC
 ​
 //设置PS+PO,新生代使用功能Parallel Scavenge 老年代将会使用Parallel Old收集器
 开启 -XX:+UseParallelOldGC
 ​
 //CMS垃圾收集器（老年代）
 开启 -XX:+UseConcMarkSweepGC
 ​
 //设置G1垃圾收集器
 开启 -XX:+UseG1GC
```


## 增加内存大小

现象：垃圾收集频率非常频繁。

原因：如果内存太小，就会导致频繁的需要进行垃圾收集才能释放出足够的空间来创建新的对象，所以增加堆内存大小的效果是非常显而易见的。

注意：如果垃圾收集次数非常频繁，但是每次能回收的对象非常少，那么这个时候并非内存太小，而可能是内存泄露导致对象无法回收，从而造成频繁GC。

参数配置：
```
 //设置堆初始值
 指令1：-Xms2g
 指令2：-XX:InitialHeapSize=2048m
 ​
 //设置堆区最大值
 指令1：`-Xmx2g` 
 指令2： -XX:MaxHeapSize=2048m
 ​
 //新生代内存配置
 指令1：-Xmn512m
 指令2：-XX:MaxNewSize=512m
```

## 设置符合预期的停顿时间

现象：程序间接性的卡顿

原因：如果没有确切的停顿时间设定，垃圾收集器以吞吐量为主，那么垃圾收集时间就会不稳定。

注意：不要设置不切实际的停顿时间，单次时间越短也意味着需要更多的GC次数才能回收完原有数量的垃圾.

参数配置：
```
 //GC停顿时间，垃圾收集器会尝试用各种手段达到这个时间
 -XX:MaxGCPauseMillis 
```

## 调整内存区域大小比率


现象：某一个区域的GC频繁，其他都正常。

原因：如果对应区域空间不足，导致需要频繁GC来释放空间，在JVM堆内存无法增加的情况下，可以调整对应区域的大小比率。

注意：也许并非空间不足，而是因为内存泄造成内存无法回收。从而导致GC频繁。

参数配置：

```
 //survivor区和Eden区大小比率
 指令：-XX:SurvivorRatio=6  //S区和Eden区占新生代比率为1:6,两个S区2:6
 ​
 //新生代和老年代的占比
 -XX:NewRatio=4  //表示新生代:老年代 = 1:4 即老年代占整个堆的4/5；默认值=2
```

## 调整对象升老年代的年龄

现象：老年代频繁GC，每次回收的对象很多。

原因：如果升代年龄小，新生代的对象很快就进入老年代了，导致老年代对象变多，而这些对象其实在随后的很短时间内就可以回收，这时候可以调整对象的升级代年龄，让对象不那么容易进入老年代解决老年代空间不足频繁GC问题。

注意：增加了年龄之后，这些对象在新生代的时间会变长可能导致新生代的GC频率增加，并且频繁复制这些对象新生的GC时间也可能变长。

配置参数：
```
 //进入老年代最小的GC年龄,年轻代对象转换为老年代对象最小年龄值，默认值7
 -XX:InitialTenuringThreshol=7 
```

## 调整大对象的标准

现象：老年代频繁GC，每次回收的对象很多,而且单个对象的体积都比较大。

原因：如果大量的大对象直接分配到老年代，导致老年代容易被填满而造成频繁GC，可设置对象直接进入老年代的标准。

注意：这些大对象进入新生代后可能会使新生代的GC频率和时间增加。

配置参数：
```
 //新生代可容纳的最大对象,大于则直接会分配到老年代，0代表没有限制。
  -XX:PretenureSizeThreshold=1000000 
```

## 调整GC的触发时机

现象：CMS，G1 经常 Full GC，程序卡顿严重。

原因：G1和CMS 部分GC阶段是并发进行的，业务线程和垃圾收集线程一起工作，也就说明垃圾收集的过程中业务线程会生成新的对象，所以在GC的时候需要预留一部分内存空间来容纳新产生的对象，如果这个时候内存空间不足以容纳新产生的对象，那么JVM就会停止并发收集暂停所有业务线程（STW）来保证垃圾收集的正常运行。这个时候可以调整GC触发的时机（比如在老年代占用60%就触发GC），这样就可以预留足够的空间来让业务线程创建的对象有足够的空间分配。

注意：提早触发GC会增加老年代GC的频率。

配置参数：
```
 //使用多少比例的老年代后开始CMS收集，默认是68%，如果频繁发生SerialOld卡顿，应该调小
 -XX:CMSInitiatingOccupancyFraction
 ​
 //G1混合垃圾回收周期中要包括的旧区域设置占用率阈值。默认占用率为 65%
 -XX:G1MixedGCLiveThresholdPercent=65 
```

## 调整 JVM本地内存大小
现象：GC的次数、时间和回收的对象都正常，堆内存空间充足，但是报OOM

原因： JVM除了堆内存之外还有一块堆外内存，这片内存也叫本地内存，可是这块内存区域不足了并不会主动触发GC，只有在堆内存区域触发的时候顺带会把本地内存回收了，而一旦本地内存分配不足就会直接报OOM异常。

注意： 本地内存异常的时候除了上面的现象之外，异常信息可能是OutOfMemoryError：Direct buffer memory。 解决方式除了调整本地内存大小之外，也可以在出现此异常时进行捕获，手动触发GC（System.gc()）。

配置参数：
```
 XX:MaxDirectMemorySize
```