# 集合的由来

通常，我们的`Java`程序需在运行时，才知道创建了多少对象。但是在开发阶段我们根本就不知道需要创建多少个对象，甚至不知道它的准确类型。

为了满足这个需求，我们要求能在任意地点创建任意数量的对象，那么这些对象要用什么来容纳呢？我们首先会想到数组，但是数组只能存储既定类型的数据，而且长度是固定的。所以集合应运而生了。


集合框架需要满足以下几个目标：

1. 集合必须是高性能的。基本采用 数组、链表、树、哈希表来进行实现保持高性能。

1. 集合应允许存储不同类型的数据。所以采用`Object` 和 `泛型` 来存储数据。


# Java集合是什么

`Java` 集合类放在 `java.util`包中。它们有以下特点：

1. 集合只能存放对象：由于集合需要满足存储不同类型的数据，所以集合只能存放对象，基本类型会转为包装类进行存储。

1. 集合存放的都是对象的引用，而非对象本身。对象本身还是存放在堆内存中的。

# Java集合框架

<img src="../../java-notes/img/Java集合框架.gif">

如上图所示，`Java` 集合框架主要包含两种类型的容器，一种是集合 (`Collection`)，用来存储元素集合。另一种是图(`Map`)，存储键值对映射。`Collection`继承了`Iterator` 接口所以`List` 和`Set`都能使用迭代器进行遍历，而`Map`不能


1. `Collection` 有三个子接口，`List` 、`Set` 、`Queue`。每个接口下面包含一些抽象类，最后是具体实现类。

    1. `List` 接口下的抽象类为 `AbstractList` 具体实现有：

        `ArrayList`、`LinkedList`、`Vertor` -> `Stack`

    1. `Set` 接口下的抽象类为 `AbstractSet` 具体实现有：

        `HashSet` -> `LinkedHashSet` `、TreeSet`

1. `Map` 接口下包含一个抽象类 `AbstractMap` 具体实现有：

    `HashMap` -> `LinkedHashMap` 、 `TreeMap` 、`HashTable`

1. 工具类有 `Collections` 和 `Arrays`

## Iterator迭代器

迭代器（Iterator Pattern）一种设计模式。这种模式提供一种方法顺序访问聚合对象中的元素，而不暴露/知晓其底部表示。它是一种行为型模式

在`Java`中 `Iterable` 接口是迭代器的顶级接口，`Collection` 靠继承它来实现迭代器模式。

```
public interface Collection<E> extends Iterable<E> {}
```

我们来看一下`Iteable`的方法 `Iterator<T> iterator();` 该方法返回了一个迭代器(接口) `iterator`。而这个迭代器则定义了集合遍历的方法。

<table>
<tr>
    <th>方法名</th>
    <th>说明</th>
</tr>
<tr>
    <td>boolean hasNext()</td>
    <td>判断容器内是否还有可供访问的元素</td>
</tr>
<tr>
    <td>E next()</td>
    <td>返回下一次元素</td>
</tr>
<tr>
    <td>void remove()</td>
    <td>移除当前元素</td>
</tr>
</table>


## 比较器

在`Java`中常见需要对数据进行排序、去重等问题，那么就设计到对象比较的问题。

通常对象之间的比较有两种：

1. 对象地址是否一致，也就是是否引用自同一个对象。这种方式可以用 `==` 来完成

1. 以对象的某个属性做比较。

在 `Java8` 中有三种比较对象的方法：

1. 重写`Object`类的的 `equals` 方法（重写 `equals` 也必须重写 `hashCode`）；

1. 继承 `Comparable` 接口，实现 `compareTo(T o)` 方法。

1. 定义一个单独的对象比较器，继承 `Comparator` 接口，实现 `compare(T o1, T o2)` 方法

根据需要比较对象的不同，来选择比较方式。

第一种：一般用于基本类型数组方面的排序

第二种和第三种一般用来对集合(List)进行排序。



## Collections

`Collections` 对集合的一共工具类，包含如下操作：

1. 排序操作
    - void reverse(List list)：反转

    - void shuffle(List list),随机排序

    - void sort(List list),按自然排序的升序排序

    - void sort(List list, Comparator c);定制排序，由Comparator控制排序逻辑

    - void swap(List list, int i , int j),交换两个索引位置的元素

    - ......

1. 查找替换

    - int binarySearch(List list, Object key), 对List进行二分查找，返回索引，注意List必须是有序的

    - int max(Collection coll),根据元素的自然顺序，返回最大的元素。 类比int min(Collection coll)

    - int max(Collection coll, Comparator c)，根据定制排序，返回最大元素，排序规则由Comparatator类控制。类比int min(Collection coll, Comparator c)

    - void fill(List list, Object obj),用元素obj填充list中所有元素

    - int frequency(Collection c, Object o)，统计元素出现次数

    - int indexOfSubList(List list, List target), 统计targe在list中第一次出现的索引，找不到则返回-1，类比int lastIndexOfSubList(List source, list target).

    - boolean replaceAll(List list, Object oldVal, Object newVal), 用新元素替换旧元素。

1. 同步控制 - 将线程不安全集合转为线程安全集合

     - List<T> synchronizedList(List<T> list)

     - Map<K,V> synchronizedMap(Map<K,V> m) 

     - Set<T> synchronizedSet(Set<T> s)


## Arrays

`Arrays` 是针对数组的工具类，可以进行 排序，查找，复制填充等功能。 

1. 数组复制

1. 排序

1. 搜索

1. 判断是否相同

1. 填充

1. asList - 快速创建List - 但是不能增删。

