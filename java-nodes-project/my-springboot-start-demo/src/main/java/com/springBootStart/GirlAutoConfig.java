package com.springBootStart;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 刘磊
 * @date 2021-02-25 ${time}
 */
@Configuration
@ConditionalOnClass(Girl.class)//有该类是开启加载
public class GirlAutoConfig {

    @Bean
    @ConditionalOnMissingBean(Girl.class)//系统中没有该类时，开启加载
    public Girl girl() {
        Girl girl = new Girl();
        girl.setName("李铁锤");
        girl.setAge(28);
        return girl;
    }
}
