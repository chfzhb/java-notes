package com.springBootStart;

import java.io.Serializable;

/**
 * @author 刘磊
 * @date 2021-02-25 ${time}
 */

public class Girl implements Serializable {

    private String name;
    private Integer age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
