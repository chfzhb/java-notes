package com.annotation;

/**
 * 注解应用
 *
 * @author 刘磊
 * @date 2021-02-20 ${time}
 */
@CourseInfoAnnotation(
        courseName = "剑指Java面试",
        courseTag = "面试",
        courseProfile = "全面")
public class ImoocCourse {

    @PersoninfoAnnotation(name = "LL", age = 27, language = {"java", "C#", "JS"})
    private String author;


    public ImoocCourse() {
    }


    @CourseInfoAnnotation(
            courseName = "商铺",
            courseTag = "实战",
            courseProfile = "真实开发",
            courseIndex = 144)
    public void getCourseInfo() {

    }
}
