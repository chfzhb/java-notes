package com.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段注解
 *
 * @author 刘磊
 * @date 2021-02-20 ${time}
 */
@Target(ElementType.FIELD)//用于字段
@Retention(RetentionPolicy.RUNTIME)//记录在类文件中，可以通过反射获取到
public @interface PersoninfoAnnotation {

    String name() default "";

    int age() default 19;

    String gender() default "男";

    String[] language();
}

