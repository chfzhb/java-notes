package com.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类注解
 * <p>
 * 应用在类和方法上
 *
 * @author 刘磊
 * @date 2021-02-20 ${time}
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CourseInfoAnnotation {


    String courseName() default "";

    String courseTag() default "";

    String courseProfile() default "";

    int courseIndex() default 303;
}
