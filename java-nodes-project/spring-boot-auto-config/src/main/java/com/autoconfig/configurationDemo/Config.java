package com.autoconfig.configurationDemo;

import com.autoconfig.common.Dog;
import com.autoconfig.common.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author 刘磊
 * @date 2021-02-23 ${time}
 */
@Import(MyImportBeanDefinitionRegistrar.class)
@Configuration(proxyBeanMethods = false)
public class Config {


//    @Bean("person01")
//    public Person personBean() {
//        return new Person("01");
//    }


    @Conditional({WindowsCondition.class})
    @Bean(value = "windows")
    public Person z() {
        return new Person("windows");
    }

    @Conditional({LinuxCondition.class})
    @Bean(value = "linux")
    public Person l() {
        return new Person("linux");
    }
}
