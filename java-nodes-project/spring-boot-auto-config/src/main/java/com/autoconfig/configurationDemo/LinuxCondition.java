package com.autoconfig.configurationDemo;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author 刘磊
 * @date 2021-02-24 ${time}
 */
public class LinuxCondition implements Condition {


    /**
     * @param context  判断条件使用的上下文环境
     * @param metadata 注解的注释信息
     * @return
     */
    @Override
    public boolean matches(ConditionContext context,
                           AnnotatedTypeMetadata metadata) {

        //1.能获取到ioc使用的beanFactory
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();

        //2.获取到类加载器
        ClassLoader classLoader = context.getClassLoader();

        //3.获取当前环境信息
        Environment environment = context.getEnvironment();

        //4.获取到bean定义的注册类
        BeanDefinitionRegistry registry = context.getRegistry();

        String osName = environment.getProperty("os.name");

        if (osName.toLowerCase().contains("linux")) {
            System.out.println("current osName = " + osName);
            return true;
        }

        return false;
    }
}

