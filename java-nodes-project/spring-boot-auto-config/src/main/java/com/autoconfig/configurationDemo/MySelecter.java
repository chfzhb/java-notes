package com.autoconfig.configurationDemo;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author 刘磊
 * @date 2021-02-24 ${time}
 */
public class MySelecter implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{
            "com.autoconfig.common.Dog",
                    "com.autoconfig.common.Person"
        };
    }
}
