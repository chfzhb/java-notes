package com.proxy;

/**
 * 被代理类
 * <p>
 * 需要实现代理接口
 *
 * @author 刘磊
 * @date 2021-02-19 ${time}
 */
public class RequestController implements Controller {

    @Override
    public void request() {
        System.out.println("去查询数据库了");
    }
}
