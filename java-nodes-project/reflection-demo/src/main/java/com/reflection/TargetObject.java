package com.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 刘磊
 * @date 2021-02-19 ${time}
 */
public class TargetObject {

    private String value;

    public TargetObject() {
        value = "TargetObject";
    }

    public void publicMethod(String s) {
        System.out.println("publicMethod " + s);
    }

    private void privateMethod() {
        System.out.println("privateMethod " + value);
    }

    /**
     * 测试方法
     *
     * @param args
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws NoSuchFieldException
     */
    public static void main(String[] args)
            throws ClassNotFoundException, IllegalAccessException,
            InstantiationException, NoSuchMethodException,
            InvocationTargetException, NoSuchFieldException {

        /**
         * 获取TargetObject类的Class对象，并且创建TargetObject类实例
         */
        Class<?> targetClass = Class.forName("com.reflection.TargetObject");
        System.out.println("获取Class: " + targetClass.toString());

        TargetObject targetObject = (TargetObject) targetClass.newInstance();

        System.out.println("根据Class实例化: " + targetObject.toString());


        /**
         * 获取所有类中所有定义的方法
         */
        Method[] methods = targetClass.getDeclaredMethods();
        System.out.println("获取所有方法：开始");
        for (Method method : methods) {
            System.out.println(method.getName());
        }
        System.out.println("获取所有方法：结束");

        /**
         * 获取指定方法并调用
         */
        System.out.println("调用方法：开始");
        Method publicMethod = targetClass.getDeclaredMethod("publicMethod",
                String.class);

        publicMethod.invoke(targetObject, "JavaGuide");
        System.out.println("调用方法：结束");

        /**
         * 获取指定参数并对参数进行修改
         */
        System.out.println("设置指定属性：开始");
        Field field = targetClass.getDeclaredField("value");
        //为了对类中的参数进行修改我们取消安全检查
        field.setAccessible(true);
        field.set(targetObject, "JavaGuide");
        Object o = field.get(targetObject);
        System.out.println("属性值被设置：" + o);
        System.out.println("设置指定属性：结束");

        /**
         * 调用 private 方法
         */
        System.out.println("调用private方法：开始" );
        Method privateMethod = targetClass.getDeclaredMethod("privateMethod");
        //为了调用private方法我们取消安全检查
        privateMethod.setAccessible(true);
        privateMethod.invoke(targetObject);
        System.out.println("调用private方法：结束" );
    }
}
