package com.rabbitmq.dealLetter;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.utils.RabbitConstant;
import com.rabbitmq.utils.RabbitUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * @author 刘磊
 * @date 2021-03-28 ${time}
 */
public class DealLetter {
    public static void main(String[] args) throws IOException, TimeoutException {


        //获取TCP长连接
        Connection conn = RabbitUtils.getConnection();
        //创建通信“通道”，相当于TCP中的虚拟连接
        Channel channel = conn.createChannel();

        //创建队列,声明并创建一个队列，如果队列已存在，则使用这个队列
        //第一个参数：队列名称ID
        //第二个参数：是否持久化，false对应不持久化数据，MQ停掉数据就会丢失
        //第三个参数：是否队列私有化，false则代表所有消费者都可以访问，true代表只有第一次拥有它的消费者才能一直使用，其他消费者不让访问
        //第四个：是否自动删除,false代表连接停掉后不自动删除掉这个队列
        //其他额外的参数, null


        //ttl queue
        Map<String, Object> argumentsQueue = new HashMap<String, Object>();
        //投递到该队列的消息如果没有消费都将在6秒之后被投递到死信交换机
        argumentsQueue.put("x-message-ttl", 6000);
        //设置当消息过期后投递到对应的死信交换机
        argumentsQueue.put("x-dead-letter-exchange", "my-dlx-exchange");
        channel.queueDeclare(
                "my-ttl-dlx-queue",
                false,
                false,
                false,
                argumentsQueue);


        //max queue
        Map<String, Object> maxArgumentsQueue = new HashMap<String, Object>();
        //设置当消息过期后投递到对应的死信交换机
        argumentsQueue.put("x-dead-letter-exchange", "my-dlx-exchange");
        //投递到该队列的消息最多2个消息，如果超过则最早的消息被删除投递到死信交换机
        argumentsQueue.put("x-max-length", "2");
        channel.queueDeclare(
                "my-max-dlx-queue",
                false,
                false,
                false,
                maxArgumentsQueue);

        //正常交换机
        Map<String, Object> my_normal_argumentsExchange = new HashMap<String, Object>();
        my_normal_argumentsExchange.put("my-ttl-dlx-queue", "my-ttl-dlx-queue");
        my_normal_argumentsExchange.put("my-max-dlx-queue", "my-max-dlx-queue");
        channel.exchangeDeclare(
                "my-normal-exchange",
                BuiltinExchangeType.FANOUT,
                false,
                false,
                false,
                my_normal_argumentsExchange);

        //死信队列
        channel.queueDeclare(
                "my-dlx-queue",
                false,
                false,
                false,
                null);

        //死信类型交换机
        Map<String, Object> argumentsExchange = new HashMap<String, Object>();
        argumentsExchange.put("my-ttl-dlx-queue", "my-dlx-queue");
        argumentsExchange.put("my-max-dlx-queue", "my-dlx-queue");
        channel.exchangeDeclare(
                "my-dxl-exchange",
                BuiltinExchangeType.FANOUT,
                false,
                false,
                false,
                argumentsExchange);


        channel.basicPublish("my-normal-exchange", "my-ttl-dlx-queue", null, "ttl消息".getBytes());
        channel.close();
        conn.close();
        System.out.println("===发送成功===");
    }
}
