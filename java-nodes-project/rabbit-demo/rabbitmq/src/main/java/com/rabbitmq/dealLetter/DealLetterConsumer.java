package com.rabbitmq.dealLetter;

import com.rabbitmq.client.*;
import com.rabbitmq.utils.RabbitConstant;
import com.rabbitmq.utils.RabbitUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 刘磊
 * @date 2021-03-28 ${time}
 */
public class DealLetterConsumer {

    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = RabbitUtils.getConnection();
        final Channel channel = connection.createChannel();
        channel.queueDeclare("my-dlx-queue", false, false, false, null);

        channel.queueBind("my-dlx-queue", "my-dxl-exchange", "my-ttl-queue");

        channel.basicQos(1);
        channel.basicConsume("my-dlx-queue", false, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        });

    }
}

