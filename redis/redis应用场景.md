
1. 热点数据的储存

    结合 expire ，我们可以设置热点数据过期时间，随后进行缓存更新。

1. 限时业务

    redis中可以使用  expire 命令设置一个键的生存时间，到时间后 redis 会删除它。利用这一特性，我们可以运用在显示的优惠活动信息，手机验证码等场景

1. 技术器相关

    通过 incrby 命令，可以运用在秒杀活动，接口限制访问速度等

1. 排行榜相关问题

    关系型数据库在排行榜方面查询速度普遍偏慢，所以可以借助redis的SortedSet进行热点数据的排序。

1. 分布式锁

    这个主要利用redis的setnx命令进行，setnx："set if not exists"就是如果不存在则成功设置缓存同时返回1，否则返回0。

1. 分页、模糊搜索

    redis的set集合中提供了一个zrangebylex方法，语法如下：

    ZRANGEBYLEX key min max [LIMIT offset count]

    通过ZRANGEBYLEX zset - + LIMIT 0 10 可以进行分页数据查询，其中- +表示获取全部数据

    zrangebylex key min max 这个就可以返回字典区间的数据，利用这个特性可以进行模糊查询功能，这个也是目前我在redis中发现的唯一一个支持对存储内容进行模糊查询的特性。