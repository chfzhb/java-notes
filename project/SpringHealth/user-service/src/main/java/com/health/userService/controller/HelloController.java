package com.health.userService.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 刘磊
 * @date 2021-03-02 ${time}
 */
@RestController
public class HelloController {


    @GetMapping("/")
    public String hello() {
        return "hello";
    }

}
