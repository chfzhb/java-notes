package com.health.userService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author 刘磊
 * @date 2021-03-02 ${time}
 */
@EnableEurekaClient
@SpringBootApplication
public class UserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }


    @LoadBalanced
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate restTemplate;


    @PostConstruct
    public void init() throws InterruptedException {
        Thread.sleep(500);
        //获取服务列表
        List<String> serviceNames = discoveryClient.getServices();
        for (String serviceName : serviceNames) {
            System.out.println("serviceName = " + serviceName);

            //进一步获取服务实例信息
            List<ServiceInstance> serviceInstances = discoveryClient.getInstances(serviceName);
            System.out.println("serviceInstances = " + serviceInstances);
            System.out.println("------------------------------------");

        }
    }
}
