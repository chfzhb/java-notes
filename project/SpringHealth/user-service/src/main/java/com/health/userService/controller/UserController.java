package com.health.userService.controller;

import com.health.common.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Random;

/**
 * @author 刘磊
 * @date 2021-03-02 ${time}
 */
@RestController

@RequestMapping(value = "users")
@Slf4j
public class UserController {
    @Autowired
    private HttpServletRequest request;


    @RequestMapping(value = "/{userName}", method = RequestMethod.GET)
    public User getUserByUserName(@PathVariable("userName") String userName) throws Throwable {
//        Random random = new Random();
//        int i = random.nextInt(8);
//        Thread.sleep(i*1000);
//
//        if(i%3==0){
//            throw  new Throwable("主动出错");
//        }


        User user = new User();
        user.setId(001L);
        user.setUserCode("mockUser");
        user.setUserName(userName);
        log.info("接收调用 - ",user);
        return user;
    }

}

