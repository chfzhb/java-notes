package com.health.intervention.service;

import com.health.common.bean.User;
import com.health.intervention.client.UserServiceClient;
import com.netflix.hystrix.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 刘磊
 * @date 2021-03-05 ${time}
 */
//@Component
public class GetUserCommand extends HystrixCommand<User> {


    //远程调用 user-service 的客户端工具类
    @Autowired
    private UserServiceClient userServiceClient;

    public GetUserCommand() {

        super(Setter.withGroupKey(
                //设置命令组
                HystrixCommandGroupKey.Factory.asKey("springHealthGroup"))
                //设置命令键
                .andCommandKey(HystrixCommandKey.Factory.asKey("interventionKey"))
                //设置线程池键
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("aaa"))
                //设置命令属性
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.Setter()
                                .withCircuitBreakerRequestVolumeThreshold(10)//至少有10个请求，熔断器才进行错误率的计算
                                .withCircuitBreakerSleepWindowInMilliseconds(5000)//熔断器中断请求5秒后会进入半打开状态,放部分流量过去重试
                                .withCircuitBreakerErrorThresholdPercentage(50)//错误率达到50开启熔断保护
                                .withExecutionTimeoutEnabled(true)//开启执行超时
                                .withExecutionTimeoutInMilliseconds(5000))//超时时间
                //设置线程池属性
                .andThreadPoolPropertiesDefaults(
                        HystrixThreadPoolProperties.Setter()
                                .withMaxQueueSize(10)//最大队列
                                .withCoreSize(2))
        );

        System.out.println("GetUserCommand 启动");
    }

    @Override
    protected User run() {
        User user = userServiceClient.getUser("old-HystrixCommand");
        return user;
    }

    @Override
    protected User getFallback() {
        return new User(1L, "user1", "fail");
    }
}
