package com.health.zuu.config;

import com.health.zuu.filter.FilterPre;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 拦截器配置
 *
 * @author 刘磊
 * @date 2021-03-03 ${time}
 */
@Configuration
public class FilterConfig {


    /**
     * 自定义拦截器
     * <p>
     * 鉴权
     *
     * @return
     */
    @Bean
    public FilterPre filterPre() {
        return new FilterPre();
    }

    /**
     * cors 跨域放行
     *
     * @return
     */
    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        config.setMaxAge(18000L);
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
