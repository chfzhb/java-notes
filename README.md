
test


# JVM
- [JAVA内存区域](2.Java基础/JAVA内存区域.md)

- [垃圾回收与算法](2.Java基础/垃圾回收与算法.md)

- [类加载机制](2.Java基础/类加载机制.md)

- [JVM调优](2.Java基础/JVM调优.md)

- [JVM调优工具](2.Java基础/JVM调优工具.md)

# Java基础


todo

- [HashCode和equals](2.Java基础/HashCode和equals.md)

- [反射](2.Java基础/反射.md)

# Java集合

- [集合概述](4.Java集合/集合概述.md)

- [ArrayList](4.Java集合/ArrayList.md)

- [HashMap](4.Java集合/HashMap.md)

- [ConcurrentHashMap](4.Java集合/ConcurrentHashMap.md)

- [LinkedHashMap](4.Java集合/LinkedHashMap.md)



# Java并发

java并发主要包括线程、锁机制、volatiale、juc等部分

## 并发概述



- [并发的优势与风险-todo](3.Java并发/1.Java内存模型/并发的优势与风险.md) 

- [CPU多级缓存](3.Java并发/1.Java内存模型/CPU多级缓存.md)

- [理解mutex,semaphore,monitor](3.Java并发/1.Java内存模型/理解mutex,semaphore,monitor.md)


- [Java内存模型](3.Java并发/1.Java内存模型/Java内存模型.md)

## 线程

- [进程和线程](3.Java并发/2.线程/进程和线程.md)
- [创建线程的方式](3.Java并发/2.线程/创建线程的方式.md)
- [线程的生命周期](3.Java并发/2.线程/线程的生命周期.md)
- [线程终止的四种方式](3.Java并发/2.线程/线程终止的四种方式.md)
- [线程相关的API](3.Java并发/2.线程/线程相关的API.md)
- [线程的上下文切换](3.Java并发/2.线程/线程的上下文切换.md)
- [Java中的线程调度](3.Java并发/2.线程/Java中的线程调度.md)
- [进程调度算法](3.Java并发/2.线程/进程调度算法.md)

## synchronized和volatile


- [synchronized使用](3.Java并发/3.锁和volatile/synchronized使用.md)
- [synchronized的消除粗化降级](3.Java并发/3.锁和volatile/synchronized的消除粗化降级.md)
- [Java对象内存布局](3.Java并发/3.锁和volatile/Java对象内存布局.md)
- [synchronized原理](3.Java并发/3.锁和volatile/synchronized原理.md)
- [死锁](3.Java并发/3.锁和volatile/死锁.md)
- [等待通知模型](3.Java并发/3.锁和volatile/等待通知模型.md)
- [volatile](3.Java并发/3.锁和volatile/volatile.md)


## 线程安全
- [线程不安全的类](3.Java并发/4.线程安全/线程不安全的类.md)
- [对象发布与逸出](3.Java并发/4.线程安全/对象发布与逸出.md)
- [单例模式](3.Java并发/4.线程安全/单例模式.md)
- [不可变对象](3.Java并发/4.线程安全/不可变对象.md)
- [同步容器](3.Java并发/4.线程安全/同步容器.md)
- [ThreadLocal](3.Java并发/4.线程安全/ThreadLocal.md)

- [spring线程安全]() - todo


## JUC
- [JUC概述](3.Java并发/5.JUC/JUC概述.md) 
- unsafe - todo
- [CAS](3.Java并发/5.JUC/CAS.md)
- [AQS](3.Java并发/5.JUC/AQS.md)
- 锁 - todo
- 队列 - todo
- [线程池](3.Java并发/5.JUC/线程池.md)



- 并发集合 - todo
- 异步组合编程 - todo
- 工具类 - todo

## 并发最佳实践

todo



# JavaIO

# spring 

## 死磕Spring系列

- [深入理解IOC](Spring/深入理解IOC.md)

- [Spring统一资源加载策略](Spring/Spring统一资源加载策略.md)


## Spring注解

- [容器相关注解](Spring/容器相关注解.md)

- [AOP相关注解](Spring/AOP相关注解.md)

## 慕课网Spring源码笔记


- [spring模块梳理](Spring/慕课网/spring模块梳理.md)

- [spring源码编译](Spring/慕课网/spring源码编译.md)

- [门面模式](Spring/慕课网/门面模式.md)

- [Ioc容器实现](Spring/慕课网/Ioc容器实现.md)


- [SpringIoC源码解析](Spring/慕课网/SpringIoC源码解析.md)



# 设计模式



- [设计模式原则](架构师3期/设计模式/设计模式原则.md)

- [工厂模式](架构师3期/设计模式/工厂模式.md)

- [单例模式](架构师3期/设计模式/单例模式.md)

- [原型模式](架构师3期/设计模式/原型模式.md)

- [代理模式](架构师3期/设计模式/代理模式.md)

- [JDK代理原理](架构师3期/设计模式/JDK代理原理.md)

- CGlib代理原理 - todo

- [委派模式](架构师3期/设计模式/委派模式.md)

- [策略模式](架构师3期/设计模式/策略模式.md)

- [模板方法模式](架构师3期/设计模式/模板方法模式.md)

- [适配器模式](架构师3期/设计模式/适配器模式.md)

- [装饰者模式](架构师3期/设计模式/装饰者模式.md)

- [观察者模式](架构师3期/设计模式/观察者模式.md)




# Spring&Mybatis源码分析

## 框架知识前置

- [servlet](架构师3期/源码分析/框架知识前置/servlet.md)

- [反射](架构师3期/源码分析/框架知识前置/反射.md)

- [注解](架构师3期/源码分析/框架知识前置/注解.md)

- [控制反转与依赖注入](架构师3期/源码分析/框架知识前置/控制反转与依赖注入.md)


## Spring源码分析

- [Spring历史与源码构建](架构师3期/源码分析/Spring源码分析/Spring历史与源码构建.md)

- [Spring五大体系](架构师3期/源码分析/Spring源码分析/Spring五大体系.md)



- [加载BeanDefition概述](架构师3期/源码分析/Spring源码分析/加载BeanDefition概述.md)

- [加载Bean概述](架构师3期/源码分析/Spring源码分析/加载Bean概述.md)

- [Bean加载全流程](架构师3期/源码分析/Spring源码分析/Bean加载全流程.md)

- [SpringBean循环依赖](架构师3期/源码分析/Spring源码分析/SpringBean循环依赖.md)



- [Aware接口](架构师3期/源码分析/Spring源码分析/Aware接口.md)

- [BeanPostProcessor接口](架构师3期/源码分析/Spring源码分析/BeanPostProcessor接口.md)

- [InitializingBean和init-method](架构师3期/源码分析/Spring源码分析/InitializingBean和init-method.md)

- [SpringBean的生命周期](架构师3期/源码分析/Spring源码分析/SpringBean的生命周期.md)

- [Spring的refresh方法](架构师3期/源码分析/Spring源码分析/Spring的refresh方法.md)
 
- [SpringAOP](架构师3期/源码分析/Spring源码分析/SpringAOP.md)


- [Spring事务](架构师3期/源码分析/Spring源码分析/Spring事务.md)


- [SpringMVC](架构师3期/源码分析/Spring源码分析/SpringMVC.md)

- [SpringBoot的自动装配](架构师3期/源码分析/Spring源码分析/SpringBoot的自动装配.md)

## MyBatis源码分析
- [mybatis架构设计](架构师3期/源码分析/mybatis源码分析/mybatis架构设计.md)


# 并发&分布式基础

# 分布式&微服务

- [微服务架构](分布式&微服务/微服务架构.md)

- [SpringCloud介绍](分布式&微服务/SpringCloud介绍.md)

## 服务治理

- [理解服务治理](分布式&微服务/理解服务治理.md)

- [SpringCloudEureka服务端](分布式&微服务/SpringCloudEureka.md)

- [SpringCloudEureka客户端](分布式&微服务/SpringCloudEureka客户端.md)

- [Ribbon使用](分布式&微服务/Ribbon使用.md)

- [Ribbon架构和原理](分布式&微服务/Ribbon架构和原理.md)

## 远程调用

- [远程调用](分布式&微服务/远程调用.md)

## 网关

- [基于Zuul构建网关](分布式&微服务/基于Zuul构建网关.md)

- [Zuul的工作原理](分布式&微服务/Zuul的工作原理.md)

## 服务容错

- [服务容错](分布式&微服务/服务容错.md)

- [使用SpringCloudCircuitBreaker实现服务容错](分布式&微服务/使用SpringCloudCircuitBreaker实现服务容错.md)



- [Hystrix原理](分布式&微服务/Hystrix原理.md)

## 分布式配置中心 - todo

## 分布式链路追踪

- [分布式链路追踪](分布式&微服务/分布式链路追踪.md)





# MySql

- [索引](MySql/索引.md)

- [join使用和原理](MySql/join使用和原理.md)



# 分布式事务

- [分布式事务](分布式事务/分布式事务.md)

# redis
- [redis](redis/redis.md)

- [redis数据类型](redis/redis数据类型.md)

- [redis应用场景](redis/redis应用场景.md)

- [redis持久化](redis/redis持久化.md)

- [redis数据删除策略](redis/redis数据删除策略.md)

- [redis事务](redis/redis事务.md)

- [redis主从复制](redis/redis主从复制.md)

- [redis解决方案](redis/redis解决方案.md)

# MQ
- [rabbitMq](rabbitMq/rabbitMq.md)


# 电商

# 操作系统

## 计算机组成原理

- [计算机是什么](操作系统/计算机是什么.md)

# 总结
- [总结](总结/总结.md)







