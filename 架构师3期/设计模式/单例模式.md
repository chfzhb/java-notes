
# 概念

单例模式（Singleton Pattern）属于创建类型的一种常用的软件设计模式。通过单例模式的方法创建的类在当前进程中只有一个实例（根据需要，也有可能一个线程中属于单例，如：仅线程上下文内使用同一个实例）

# 饿汉模式

```java
public class HungerySingleton  implements Serializable{
    //加载的时候就产生的实例对象
    private static final HungerySingleton instance = new HungerySingleton();
    private HungerySingleton(){
    }

    //返回实例对象
    public static HungerySingleton getInstance(){
        return instance;
    }
}
```

在加载的时候已经被实例化，是**线程安全**的，但是不能抵挡反射和序列化的攻击。





# 懒汉模式

```java
public class LazySingleton {
    private static LazySingleton instance = null;

    private LazySingleton() {
    }

    public static LazySingleton getInstance() {
        if (null == instance)
            instance = new LazySingleton();
        return instance;
    }
}
```

**线程安全：**

这种模式下是非线程安全的，因为在 `instance` 实例化未完成时，第二个线程在判断 `instance` 依然为 `null` 值所以会实例化两次。

**解决方法：**

为 `getInstance` 添加 `synhronized` 修饰，但是这样，就会将方法执行串行化，降低性能，所以应使用其他模式。

# 双重检查模式

双重检查模式是懒汉模式的升级版，是为了解决在多线程中JVM指令重排序的问题

```java
public class DCL {
    private static  DCL instance = null;
    private DCL(){
    }
    public static DCL getInstance(){
        if(null==instance)
            synchronized (DCL.class){
                if(null==instance)
                    instance=new DCL();
            }
        return instance;
    }
}
```
**线程安全：**

这种方式依然是非线程安全的，虽然已经使用 `synchronized` 关键字来同步代码块，但是。`instance` 的 `new` 操作会经历三个过程

1. 分配内存
2. 执行Singleton构造函数
3. 将实例指向分配的内存

由于是`synchronized`修饰的，所以不保证操作的顺序，只保证原子性.
如果按照 1-> 3 -> 2去执行的话，当线程A执行完3的时候，`instance` 是有值了，但是还没执行 `DCL` 构造函数初始化实例，然后线程B在`synchronized`外面的if判断时，由于`instance`非空，所以直接返回，获取到一个没有实例化完成的对象.

**解决方法：**

既然是由于 `jvm` 可能发生的指令重排产生的错误，那么使用 `volatile` 修饰 `instance` 即可禁止指令重排，从而避免获取到没有实例化完成的对象。

```java
private static valatile  DCL instance = null;
``` 

# 静态内部类模式

```java
public class HolderDemo {
    private static class Holder{
        private static HolderDemo instance = new HolderDemo();
    }

    public static HolderDemo getInstance(){
        return Holder.instance;
    }
}
```
**线程安全：**

该种方式是线程安全的，并且由于 `java` 的静态内部类的机制(在被调用时才初始化的机制)，是懒加载模式的。



# 枚举模式 - 推荐
```java
public enum Enum2 {

    INSTANCE;

    public void doSomething() {
        System.out.println("doSomething");
    }
}
```

单例的枚举实现在 `Effective Java` 一书中提到。因为其功能完善，使用简介，无偿地提供了序列化机制，在面对复杂的序列化或者反射攻击时任然可以绝对防止多次实例化等优点，被作者所推崇。

# 反射破坏单例

在单例中我们一般将构造函数使用 `private` 修饰，避免外界访问，但是反射可以通过设置访问权限来调用构造函数。

```java
public static void main(String[] args) 
    throws IllegalAccessException, InstantiationException,
        ClassNotFoundException, NoSuchMethodException, InvocationTargetException {

    //用过 getInstance 获取单例
    StaticInnerClassDemo instance = StaticInnerClassDemo.getInstance();
    System.out.println(instance);
    Class<?> clazz = Class.forName("demo.pattern.singleton.StaticInnerClassDemo");

    //使用Class直接创建对象
    Object o1 = clazz.newInstance();
    System.out.println(o1);

    //使用Constructor创建对象
    Constructor<?> constructor = clazz.getDeclaredConstructor();
    constructor.setAccessible(true);
    Object o = constructor.newInstance();
    System.out.println(o);
}
```
输出
```
demo.pattern.singleton.StaticInnerClassDemo@56ef9176
demo.pattern.singleton.StaticInnerClassDemo@4566e5bd
demo.pattern.singleton.StaticInnerClassDemo@1ed4004b
```
我们可以看到三个对象的值是不同的，这意味着创建了三个实例，我们的构造函数被执行了三次，我们的对象，被反射击破了。

## 解决反射攻击单例问题

由于构造函数执行了三次创建了三个实例，那么我们可以通过阻止构造函数的多次执行来达到，只创建一个实例的目的。

我们将通过以下两步解决这个反射攻击问题

1. 在实例对象变量中使用`final` 或 `volatiale` 修饰，避免 JVM 对其进行重排序

1. 在构造器中检测实例是否被创建
    ```java
    //构造器必须私有  不然直接new就可以创建
    //构造器判断，防止反射攻击
    //insatnse 需要被 fiale / volatiale 修饰
    if(insatnse != null){
        throw new IllegalStateException();
    }
    ```


# 序列化破坏单例

我们将对象写入到磁盘后，进行读取,称之为序列化与反序列化

```java
 public static void main(String[] args) throws IOException, ClassNotFoundException {
        
    StarvingSingleton s1 = null;
    StarvingSingleton s2 = StarvingSingleton.getInstance();

    //序列化
    // 把内存中的状态通过转换成字节码的形式
    // 从而转换一个 IO 流， 写入到其他地方(可以是磁盘、 网络 IO)
    // 内存中状态给永久保存下来了
    FileOutputStream fos = new FileOutputStream("a.obj");
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(s2);
    oos.flush();
    oos.close();
    
    //反序列化
    // 将已经持久化的字节码内容， 转换为 IO 流
    // 通过 IO 流的读取， 进而将读取的内容转换为 Java 对象
    // 在转换过程中会重新创建对象 new
    FileInputStream fis = new FileInputStream("a.obj");
    ObjectInputStream ois = new ObjectInputStream(fis);
    s1 = (StarvingSingleton) ois.readObject();
    ois.close();
    System.out.println(s1);
    System.out.println(s2);
    System.out.println(s1 == s2);

}
```
输出
```
demo.pattern.singleton.StarvingSingleton@6c3708b3
demo.pattern.singleton.StarvingSingleton@20398b7c
false
```
我们可以看到，输出的对象并不是同一个，所以我们的单例对象又被击破了。



## 解决序列化破坏单例

在我们的对象内部增加一个方法即可解决反序列化问题
```java
private Object readResolve(){
    //返回单例对象
    return INSTANCE;
}
```
这个是因为在反序列化中，反射调用了该方法，如果该方法未定义，则使用反射创建新对象,如果定义了该方法则使用该方法的返回值进行返回

# 单例模式最佳实践

单例模式的最佳实践是采用枚举类。



在 JVM 规范中规定，每个枚举类型及其定义的枚举变量在 `JVM` 中都是唯一的，因此在枚举类型的序列化和反序列化上，`Java` 做了特殊的规定。

- 序列化：`Java` 将枚举对象的 `name` 属性输出到结果中,也就是是说序列化时只是将枚举类名称输出

- 反序列化：通过 `java.lang.Enum.valueOf()` 方法根据名字在 `JVM` 中查找枚举对
象.



所以枚举单例模式，用法简单，并且能够具备防御反射和序列化的攻击

```java
public enum Enum2 {

    INSTANCE;

    public void doSomething() {
        System.out.println("doSomething");
    }
}
```

# ThreadLocal 线程单例

ThreadLocal 不能保证其
创建的对象是全局唯一，但是能保证在单个线程中是唯一的，天生的线程安全