# 概念

策略模式 （Strategy Pattern） 一个类的行为或其算法可以在运行时被更改(通过传入参数的不同)。

if 就是最简单的策略模式。


# 应用场景

1. 如果系统中有很多类，他们的区别仅仅是行为不同

1. 一个系统需要动态的在几种算法中选择一种


# 策略模式实现

接下来我们实现以下策略模式,我们使用活动促销的场景，当双十一、618等活动时，我们需要采取不同的策略，这里我们使用策略模式进行优化


## 基础代码

优惠策略抽象

```java
public interface PromotionStrategy {
    void doPromotion();
}
```
无活动
```java
public class EmptyStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("无促销活动");
    }
}
```
优惠券
```java
public class CouponStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("领取优惠券,课程的价格直接减优惠券面值抵扣");
    }
}
```
促销返现
```java
public class CashbackStrategy implements PromotionStrategy {

    @Override
    public void doPromotion() {
        System.out.println("返现促销,返回的金额转到支付宝账号");
    }
}
```
## 简单调用
```java
public class PromotionActivity {

    private PromotionStrategy promotionStrategy;


    public PromotionActivity(PromotionStrategy promotionStrategy) {
        this.promotionStrategy = promotionStrategy;
    }

    public void execute() {
        promotionStrategy.doPromotion();
    }

    public static void main(String[] args) {
        PromotionActivity activity618 = new PromotionActivity(new CouponStrategy());
        PromotionActivity activity1111 = new PromotionActivity(new CashbackStrategy());
        activity618.execute();
        activity1111.execute();
    }

}
```
输出
```
领取优惠券,课程的价格直接减优惠券面值抵扣
返现促销,返回的金额转到支付宝账号
```
我们可以看到，我们的两个活动都执行了，但是这样仍然无法动态切换活动，所以我们使用工厂模式 + 表驱动 来动态调用促销活动

## 工厂模式+表驱动调用

```java
public class PromotionStrategyFactory {
    private static Map<String, PromotionStrategy> PROMOTION_STRATEGY_MAP = 
        new HashMap<String, PromotionStrategy>();

    static {
        PROMOTION_STRATEGY_MAP.put(PromotionKey.COUPON, new CouponStrategy());
        PROMOTION_STRATEGY_MAP.put(PromotionKey.CASHBACK, new CashbackStrategy());
        PROMOTION_STRATEGY_MAP.put(PromotionKey.GROUPBUY, new GroupbuyStrategy());
    }

    private static final PromotionStrategy NON_PROMOTION = new EmptyStrategy();

    private PromotionStrategyFactory() {
    }

    public static PromotionStrategy getPromotionStrategy(String promotionKey) {
        PromotionStrategy promotionStrategy = PROMOTION_STRATEGY_MAP.get(promotionKey);
        return promotionStrategy == null ? NON_PROMOTION : promotionStrategy;
    }

    private interface PromotionKey {
        String COUPON = "COUPON";
        String CASHBACK = "CASHBACK";
        String GROUPBUY = "GROUPBUY";
    }


    public static void main(String[] args) {
        String promotionKey = "CASHBACK";
        PromotionActivity promotionActivity =
                new PromotionActivity(
                    PromotionStrategyFactory.getPromotionStrategy(promotionKey));
        promotionActivity.execute();
    }

}
```
我们可以看到在 `static` 代码块中，我们提前定义了我们所有的优惠策略并添加到map中，这样我们可以传入策略的key来动态的调用策略。

在工厂中我们可以使用反射来创建对象，这样我们的工厂的修改也关闭了，当我们添加新行为时只需要继承策略接口即可。
