

我们前几篇文章描述了bean的加载，以及循环依赖的解决。知道Bean的加载使用了`AbstractAutowireCapableBeanFactory` 类的 `doCreateBean(final String beanName, final RootBeanDefinition mbd, final Object[] args)`共分为以下三步：

1. 实例化 bean 对象：`createBeanInstance(String beanName, RootBeanDefinition mbd, Object[] args)` 方法。

1. 属性注入：`populateBean(String beanName, RootBeanDefinition mbd, BeanWrapper bw)` 方法。

1. 初始化 bean 对象：`initializeBean(final String beanName, final Object bean, RootBeanDefinition mbd)` 方法。

我们前面解释了bean的实例化以及属性注入，现在我们来看一下，初始化bean对象：

1. 激活Aware方法

1. 处理后置处理器的应用

1. 激活自定义的init方法

# Aware接口

首先我们来看下接口定义
```java
package org.springframework.beans.factory;

public interface Aware {
}
```

`Aware` 接口是`Spring`的**顶级接口**，是一个具有**标识**性质的接口，实现了该接口的Bean，具有被Spring通知的能力，通知方式采用**回调**。


`Aware` 接口是一个空接口，实际的方法签名由各个子接口来确定，且该接口通常只会有一个接收单参数的 set 方法，该 set 方法的命名方式为 set + 去掉接口名中的 Aware 后缀，即 XxxAware 接口，则方法定义为 setXxx()，

例如
 - BeanNameAware（setBeanName），
 
 - ApplicationContextAware（setApplicationContext）。


Aware 的子接口需要提供一个 setXxx 方法，我们知道 set 是设置属性值的方法，即 Aware 类接口的 setXxx 方法其实就是设置 xxx 属性值的。 Aware 的含义是感知的、感应的，那么在 Spring 容器中是如何实现感知并设置属性值得呢？我们可以从初始化 bean 中的激活 Aware 的方法 `AbstractAutowireCapableBeanFactory.invokeAwareMethods(final String beanName, final Object bean)` 中看到，代码如下：
```java
private void invokeAwareMethods(final String beanName, final Object bean) {
    if (bean instanceof Aware) {
        if (bean instanceof BeanNameAware) {
            ((BeanNameAware) bean).setBeanName(beanName);
        }
        if (bean instanceof BeanClassLoaderAware) {
            ClassLoader bcl = getBeanClassLoader();
            if (bcl != null) {
                ((BeanClassLoaderAware) bean).setBeanClassLoader(bcl);
            }
        }
        if (bean instanceof BeanFactoryAware) {
            ((BeanFactoryAware) bean).setBeanFactory(AbstractAutowireCapableBeanFactory.this);
        }
    }
}
```

1. 首先，判断 bean 实例是否属于 Aware 接口的范畴

1. 如果是的话，则调用实例的 setXxx() 方法给实例设置 xxx 属性值，

1. 在 `invokeAwareMethods(...)` 方法，主要是设置 beanName，beanClassLoader、BeanFactory 中三个属性值。

# 常用的Aware子类
我们一般常用的有 `BeanClassLoaderAware`、`BeanFactoryAware`、`BeanNameAware`、`ApplicationContextAware`

## BeanClassLoaderAware

```java
public interface BeanClassLoaderAware extends Aware {

    /**
    * 将 BeanClassLoader 提供给 bean 实例回调
    * 在 bean 属性填充之后、初始化回调之前回调，
    * 例如InitializingBean的InitializingBean.afterPropertiesSet（）方法或自定义init方法
    */
    void setBeanClassLoader(ClassLoader classLoader);
}
```
## BeanFactoryAware
```java
public interface BeanFactoryAware extends Aware {
    /**
    * 将 BeanFactory 提供给 bean 实例回调
    * 调用时机和 setBeanClassLoader 一样
    */
    void setBeanFactory(BeanFactory beanFactory) throws BeansException;

}
```

## BeanNameAware
```java
public interface BeanNameAware extends Aware {

    /**
    * 在创建此 bean 的 bean工厂中设置 beanName
    */
    void setBeanName(String name);

}
```

## ApplicationContextAware
```java
public interface ApplicationContextAware extends Aware {

    /**
     * 设置此 bean 对象的 ApplicationContext，通常，该方法用于初始化对象
     */
    void setApplicationContext(ApplicationContext applicationContext)
        throws BeansException;

}
```
## 示例

```java
@Component
public class MyApplicationAware implements 
        BeanNameAware, 
        BeanFactoryAware,
        BeanClassLoaderAware,
        ApplicationContextAware {

    private String beanName;

    private BeanFactory beanFactory;

    private ClassLoader classLoader;

    private ApplicationContext applicationContext;

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        System.out.println("调用了 BeanClassLoaderAware 的 setBeanClassLoader 方法");
        this.classLoader = classLoader;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("调用了 BeanFactoryAware 的 setBeanFactory 方法");
        this.beanFactory = beanFactory;
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("调用了 BeanNameAware 的 setBeanName 方法");
        this.beanName = name;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("调用了 ApplicationContextAware 的 setApplicationContext 方法");
        this.applicationContext = applicationContext;
    }

    public void display(){
        System.out.println("beanName:" + beanName);
        System.out.println("是否为单例：" + beanFactory.isSingleton(beanName));
        System.out.println("系统环境为：" + applicationContext.getEnvironment());
    }
}
```

输出
```java
调用了 BeanNameAware 的 setBeanName 方法
调用了 BeanClassLoaderAware 的 setBeanClassLoader 方法
调用了 BeanFactoryAware 的 setBeanFactory 方法
调用了 ApplicationContextAware 的 setApplicationContext 方法
```

# 小结

从这了我们基本上就可    以 Aware 真正的含义是什么了？感知，其实是 Spring 容器在初始化主动检测当前 bean 是否实现了 Aware 接口，如果实现了则回调其 set 方法将相应的参数设置给该 bean ，这个时候该 bean 就从 Spring 容器中取得相应的资源。

最后文章末尾列出部分常用的 Aware 子接口，便于日后查询：

- LoadTimeWeaverAware：加载Spring Bean时织入第三方模块，如AspectJ
- BeanClassLoaderAware：加载Spring Bean的类加载器
- BootstrapContextAware：资源适配器BootstrapContext，如JCA,CCI
- ResourceLoaderAware：底层访问资源的加载器
- BeanFactoryAware：声明BeanFactory
- PortletConfigAware：PortletConfig
- PortletContextAware：PortletContext
- ServletConfigAware：ServletConfig
- ServletContextAware：ServletContext
- MessageSourceAware：国际化
- ApplicationEventPublisherAware：应用事件
- NotificationPublisherAware：JMX通知
- BeanNameAware：声明Spring Bean的名字