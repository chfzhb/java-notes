
# SpringBoot特点

- **依赖管理**：当我们就某种场景，引入一个 starter 后，这个场景的常规依赖都被自动引入了，也无需关注版本号，版本自动仲裁，也可以自定义覆盖。

- **自动配置**：当我们引入某个组件后，无需设置各种初始化的数据，如当引入Spring MVC 时，会自动配置好各种常用组件，如 DispatcherServlet 、字符编码组件等。（SpringBoot所有的自动配置功能都在 spring-boot-autoconfigure 包里面）




# 配置相关注解

Spring boot 在实现自动配置时，需要以下注解的支持

## 组件添加

- **@Configuration**：告诉 `Spring` 该类为一个配置类，相当于一个配置文件

- **@ComponentScan**：该注解会扫描指定包下，所有的配置类(默认扫描当前类，所在包路径)，并将其注册到 `BeanFactory` ，后续统一实例化。

- **@Import**：用来导入配置类或者 一些 需要前置加载的类.一般用于导入组件

- **@Condition**：条件装配：满足 `Conditional` 指定的条件，则进行组件注入


### @Configuration - 配置

我们需要注意的有以下几点：

1. 被 `@Configuration` 修饰的类里面的方法使用 `@Bean` 修饰，默认为单例

1. Full模式：`proxyBeanMethods = true` 时，使用 `@Bean` 修饰的方法返回 单例对象。

    Lite模式：`proxyBeanMethods = false` 时，使用 `@Bean` 修饰的方法返回 多例对象。

    该属性默认为 `true`. 组件依赖必须使用Full模式默认。其他默认是否Lite模式

测试

配置类
```java
@Configuration(proxyBeanMethods = true)
public class Config {

    @Bean("person01")
    public Person personBean() {
        return new Person("01");
    }
}
```
启动类
```java
@SpringBootApplication
public class SpringBootAutoConfigApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SpringBootAutoConfigApplication.class, args);


        //1. 从容器中获取组件
        Person person01 = run.getBean("person01", Person.class);
        Person person02 = run.getBean("person02", Person.class);

        System.out.println("getBean 获取：" + (person01 == person02));


        //2. 调用配置文件实例的 @Bean 修饰的方法
        Config bean = run.getBean(Config.class);
        person01 = bean.personBean();
        person02 = bean.personBean();

        System.out.println("调用方法 获取：" + (person01 == person02));

    }

}
```


当 `proxyBeanMethods` 属性为 `true` 输出
```
getBean 获取：true
调用方法 获取：true
```


当 `proxyBeanMethods` 属性为 `false` 输出

```
getBean 获取：true
调用方法 获取：false
```

## @ComponentScan - 组件扫描

该注解会扫描指定包下，所有的配置类(默认扫描当前类，所在包路径)，并将其注册到`BeanFactory`，后续统一实例化。

常用属性

- `value` - 要扫描的包路径
- `excludeFilters` - 指定要排除的组件
- `includeFilters` - 指定要注入的组件

Filters使用规则
```java
@ComponentScan(
    includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, 
                                classes = Controller.class)  
    },useDefaultFilters = false
)
```
- `@ComponentScan.Filter`: 指定排除/引入方式

- `classes`: 指定排除/引入类型

- `useDefaultFilters`: 启用默认规则

### @Import - 导入

- `@Import({Dog.class})`: 将对应的 `Class` 注册到 `Spring` 中

- **ImportSelector方式【重点】**：

    配置类
    ```java
    @Import(MySelecter.class)
    @Configuration()
    public class Config {
    }
    ```
    实现 ImportSelector 接口类
    ```java
    public class MySelecter implements ImportSelector {
        @Override
        public String[] selectImports(AnnotationMetadata    importingClassMetadata) {
            //这里是全类名
            return new String[]{
                    "com.autoconfig.common.Dog",
                            "com.autoconfig.common.Person"
            };
        }
    }
    ```
    测试
    ```java
    @SpringBootApplication
    public class SpringBootAutoConfigApplication {

        public static void main(String[] args) {
            ConfigurableApplicationContext run = SpringApplication
                .run(SpringBootAutoConfigApplication.class, args);

            Person person01 = run.getBean(Person.class);
            System.out.println("person01 = " + person01);

            Dog dog = run.getBean(Dog.class);
            System.out.println("dog = " + dog);
        }
    }
    ```
    输出
    ```
    person01 = Person(name=null)
    dog = Dog(name=null)
    ```

-  实现 `ImportBeanDefinitionRegistrar` 并导入:手动注册bean到容器中

    接口实现类
    ```java
    public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

        /**
        * @param importingClassMetadata 当前类的注解信息
        * @param registry               bean定义的注册类,把所有添加到容器的bean，
        *                               通过该类来自定义注册 - registry.registerBeanDefinition();
        */
        @Override
        public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
                                            BeanDefinitionRegistry registry) {
            
                BeanDefinition beanDefinition = new RootBeanDefinition(Blue.class);
                registry.registerBeanDefinition("blue", beanDefinition);
        }
    }
    ```
    配置类
    ```java
    @Import(MyImportBeanDefinitionRegistrar.class)
    @Configuration(proxyBeanMethods = false)
    public class Config {
    }
    ```


### @Conditional - 条件装配

Spring4 新提供的注解，它的作用是按照一定的条件进行判断，满足条件给容器注册bean。并且是springboot底层大量使用的注解

conditional 注解及其派生：

![conditional-annotation](http://qiniu.liulei.life/conditional-annotation.png)

代码示例：

Condition 条件
```java
public class LinuxCondition implements Condition {


    /**
     * @param context  判断条件使用的上下文环境
     * @param metadata 注解的注释信息
     * @return
     */
    @Override
    public boolean matches(ConditionContext context,
                           AnnotatedTypeMetadata metadata) {

        //1.能获取到ioc使用的beanFactory
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();

        //2.获取到类加载器
        ClassLoader classLoader = context.getClassLoader();

        //3.获取当前环境信息
        Environment environment = context.getEnvironment();

        //4.获取到bean定义的注册类
        BeanDefinitionRegistry registry = context.getRegistry();

        String osName = environment.getProperty("as.name");

        if (osName.toLowerCase().contains("linux")) {
            System.out.println("current osName = " + osName);
            return true;
        }

        return false;
    }
}
```
```java
public class WindowsCondition implements Condition {

    /**
     * @param conditionContext:判断条件能使用的上下文环境
     * @param annotatedTypeMetadata:注解所在位置的注释信息
     * */
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        //获取ioc使用的beanFactory
        ConfigurableListableBeanFactory beanFactory = conditionContext.getBeanFactory();
        //获取类加载器
        ClassLoader classLoader = conditionContext.getClassLoader();
        //获取当前环境信息
        Environment environment = conditionContext.getEnvironment();
        //获取bean定义的注册类
        BeanDefinitionRegistry registry = conditionContext.getRegistry();

        //获得当前系统名
        String property = environment.getProperty("as.name");
        //包含Windows则说明是windows系统，返回true
        if (property.contains("Windows")){
            return true;
        }
        return false;
    }
}
```
配置
```java
@Configuration(proxyBeanMethods = false)
public class Config {

    @Conditional({WindowsCondition.class})
    @Bean(value = "windows")
    public Person z() {
        return new Person("windows");
    }

    @Conditional({LinuxCondition.class})
    @Bean(value = "linux")
    public Person l() {
        return new Person("linux");
    }
}
```
jvm启动参数，这里是和代码中对应的
```
-Dos.name=linux
```
随后启动Spring容器即可

输出
```
current osName = linux
```




## @ImportResource - 引入原生配置文件
```xml
======================beans.xml=========================
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">

    <bean id="haha" class="com.atguigu.boot.bean.User">
        <property name="name" value="zhangsan"></property>
        <property name="age" value="18"></property>
    </bean>

    <bean id="hehe" class="com.atguigu.boot.bean.Pet">
        <property name="name" value="tomcat"></property>
    </bean>
</beans>
```
测试类
```java
@ImportResource("classpath:beans.xml")
public class MyConfig {}

======================测试=================
        boolean haha = run.containsBean("haha");
        boolean hehe = run.containsBean("hehe");
        System.out.println("haha："+haha);//true
        System.out.println("hehe："+hehe);//true
```


## @ConfigurationProperties - 配置绑定

```java
/**
 * 只有在容器中的组件，才会拥有SpringBoot提供的强大功能
 */
@Component
@ConfigurationProperties(prefix = "mycar")
public class Car {

    private String brand;
    private Integer price;
}
```

上面还可以写成下面这样
```java
//1、开启Car配置绑定功能
//2、把这个Car这个组件自动注册到容器中
@EnableConfigurationProperties(Car.class)
public class MyConfig {
}
```

# 自动配置

我们知道 `@SpringBootApplication` 注解是标注在Spring boot 工程的启动类上的。

我们来看一下它的构成
```java
@SpringBootConfiguration//标注类为配置类
@EnableAutoConfiguration//重点注解，自动配置
@ComponentScan(excludeFilters = { @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
        @Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })//指定扫描哪些，Spring注解；
```

其中 `@EnableAutoConfiguration` 是重点，我们来看一下它的源码

```java
@AutoConfigurationPackage
@Import(AutoConfigurationImportSelector.class)
```

`@AutoConfigurationPackage` 注解是导入 `Registrar` ，随后利用该组件给容器倒入一系列组件

`@Import` 注解 导入了 `AutoConfigurationImportSelector` 组件，我们来看一下该组件

1. 利用 `getAutoConfigurationEntry(annotationMetadata);` 给容器中批量导入一些组件

1. 调用 `List<String> configurations = getCandidateConfigurations(annotationMetadata, attributes)` 获取到所有需要导入到容器中的配置类

1. 利用工厂加载 `Map<String, List<String>> loadSpringFactories(@Nullable ClassLoader classLoader)；` 得到所有的组件

1. 从 `META-INF/spring.factories` 位置来加载一个文件。

    默认扫描我们当前系统里面所有META-INF/spring.factories位置的文件
    spring-boot-autoconfigure-2.3.4.RELEASE.jar包里面也有META-INF/spring.factories
    
    ```
    # Auto Configure
    org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
    org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration,\
    org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
    。。。。。。。。。。。。。。。。
    。。。。。。。。。。。。。。。。。
    。。。。。。。。。。。。。。。。。。。。。
    ```

## 按需开启自动配置项


在 `Spring-boot-autoconfigure` 包中 包含着大量的 xxxxAutoConfiguration 类，这些类按照装配规则，决定是否加载组件。

以 `AopAutoConfiguration` 为例:
1. 首先该类导入了 `Advice` 类,但是我们并没有引入 AOP 包，所以该类并不存在
    ```java
    import org.aspectj.weaver.Advice;
    ```
1. 在类上使用了 `@Configuration` 标识该类为配置类，并且使用了 `@ConditionalOnProperty` 注解决定是否加载该组件
    ```java
    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(prefix = "spring.aop", name = "auto", havingValue = "true", matchIfMissing = true)
    public class AopAutoConfiguration {
        ....
    }
    ```
1. 在内部类和方法上也使用了大量的 `@Conditionalxxxx`注解，来按需加载Bean
    ```java
    @Configuration(proxyBeanMethods = false)
    @ConditionalOnClass(Advice.class)
    static class AspectJAutoProxyingConfiguration {

        @Configuration(proxyBeanMethods = false)
        @EnableAspectJAutoProxy(proxyTargetClass = false)
        @ConditionalOnProperty(prefix = "spring.aop", name = "proxy-target-class", havingValue = "false",
                matchIfMissing = false)
        static class JdkDynamicAutoProxyConfiguration {

        }

        @Configuration(proxyBeanMethods = false)
        @EnableAspectJAutoProxy(proxyTargetClass = true)
        @ConditionalOnProperty(prefix = "spring.aop", name = "proxy-target-class", havingValue = "true",
                matchIfMissing = true)
        static class CglibAutoProxyConfiguration {

        }

    }
    ```

## 修改默认配置

```java
@Bean
@ConditionalOnBean(MultipartResolver.class)  //容器中有这个类型组件
@ConditionalOnMissingBean(name = DispatcherServlet.MULTIPART_RESOLVER_BEAN_NAME) //容器中没有这个名字 multipartResolver 的组件
public MultipartResolver multipartResolver(MultipartResolver resolver) {
    //给@Bean标注的方法传入了对象参数，这个参数的值就会从容器中找。
    //SpringMVC multipartResolver。防止有些用户配置的文件上传解析器不符合规范
    // Detect if the user has created a MultipartResolver but named it incorrectly
    return resolver;
}
给容器中加入了文件上传解析器；
```

SpringBoot默认会在底层配好所有的组件。但是如果用户自己配置了以用户的优先
```java
@Bean
@ConditionalOnMissingBean
public CharacterEncodingFilter characterEncodingFilter() {
}
```

总结：
- SpringBoot先加载所有的自动配置类  xxxxxAutoConfiguration
- 每个自动配置类按照条件进行生效，默认都会绑定配置文件指定的值。xxxxProperties里面拿。xxxProperties和配置文件进行了绑定
- 生效的配置类就会给容器中装配很多组件
- 只要容器中有这些组件，相当于这些功能就有了
- 定制化配置
- 用户直接自己@Bean替换底层的组件
- 用户去看这个组件是获取的配置文件什么值就去修改。


xxxxxAutoConfiguration ---> 组件  ---> xxxxProperties里面拿值  ----> application.properties

![20210225105410](http://qiniu.liulei.life/20210225105410.png)

# 自定义start

1. 引入 `spring-boot-autoconfigure` 包
    ```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-autoconfigure</artifactId>
    </dependency>
    ```

1. 编写 组件
    ```java
    public class Girl implements Serializable {
        private String name;
        private Integer age;
    }
    ```
1. 编写自动装配类
    ```java
    @Configuration
    @ConditionalOnClass(Girl.class)//有该类是开启加载
    public class GirlAutoConfig {

        @Bean
        @ConditionalOnMissingBean(Girl.class)//系统中没有该类时，开启加载
        public Girl girl() {
            Girl girl = new Girl();
            girl.setName("李铁锤");
            girl.setAge(28);
            return girl;
        }
    }
    ```

1. 编写 `META-INF/spring.factories` 文件
    ```
    org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.springBootStart.Girl
    ```
1. 创建 Spring boot 项目引入该Start 并测试
    ```java
    Girl bean = run.etBean(Girl.class);
    System.out.println("bean = " + bean);
    ```
1. 输出
    ```
    bean = com.springBootStart.Girl@7e97551f
    ```