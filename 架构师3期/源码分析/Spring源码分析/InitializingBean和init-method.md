
Spring 在 bean 初始化时进行三个检测扩展，也就是说我们可以对 bean 进行三个不同的定制化处理，分别是 Aware接口、BeanPostProcessor接口、以及 InitializingBean 接口和 init-method 方法

# InitializingBean&DisposableBean
我们首先看一下它的定义

```java
package org.springframework.beans.factory;
public interface InitializingBean {

    //该方法在 BeanFactory 设置完了所有属性之后被调用
    //该方法允许 bean 实例设置了所有 bean 属性时执行初始化工作，
    //  如果该过程出现了错误则需要抛出异常
    void afterPropertiesSet() throws Exception;
}
```
Spring 在对对象完成实例化，设置完所有属性后，进行Aware接BeanPostProcessor前置处理后，会接着检测bean对象，是否实现了 InitializingBean接口，如果是则调用  `afterPropertiesSet` 方法，进一步调整 bean 对象的状态。

## 实例
```java
public class InitializingBeanTest implements InitializingBean {

    private String name;

    public InitializingBeanTest(String name) {
        this.name = name;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBeanTest initializing...");
        this.name = "chenssy 2 号";
    }

    public String getName() {
        return name;
    }
}
```
调用
```java
@Configuration
class Test {

    @Bean
    public InitializingBeanTest init() {
        return new InitializingBeanTest("LL");
    }

    @Autowired
    InitializingBeanTest test;


    @PostConstruct
    public void testMain(){
        String name = test.getName();
        System.out.println("name = " + name);
    }
}
```
输出
```
InitializingBeanTest initializing...
name = chenssy 2 号
```

在上面的代码中我们改变了bean属性的值，这相当于Spring容器给我们提供了一种修改Bean对象的办法

## invokeInitMethods

我们在初始化Bean阶段 首先调用`initializeBean(final String beanName, final Object bean, RootBeanDefinition mbd)` 方法,Spring会检查Bean是否实现了  `InitializingBean` 接口，如果实现了，则会调用其接口方法 - `afterPropertiesSet`。 

这个调用动作是由 `invokeInitMethods(String beanName, final Object bean, @Nullable RootBeanDefinition mbd)` 方法来完成的。

```java
// AbstractAutowireCapableBeanFactory.java

protected void invokeInitMethods(String beanName, final Object bean, @Nullable RootBeanDefinition mbd)
        throws Throwable {
    // 首先会检查是否是 InitializingBean ，如果是的话需要调用 afterPropertiesSet()
    boolean isInitializingBean = (bean instanceof InitializingBean);
    if (isInitializingBean && (mbd == null || !mbd.isExternallyManagedInitMethod("afterPropertiesSet"))) {
        if (logger.isTraceEnabled()) {
            logger.trace("Invoking afterPropertiesSet() on bean with name '" + beanName + "'");
        }
        if (System.getSecurityManager() != null) { // 安全模式
            try {
                AccessController.doPrivileged((PrivilegedExceptionAction<Object>) () -> {
                    // 属性初始化的处理
                    ((InitializingBean) bean).afterPropertiesSet();
                    return null;
                }, getAccessControlContext());
            } catch (PrivilegedActionException pae) {
                throw pae.getException();
            }
        } else {
            // 属性初始化的处理
            ((InitializingBean) bean).afterPropertiesSet();
        }
    }

    if (mbd != null && bean.getClass() != NullBean.class) {
        // 判断是否指定了 init-method()，
        // 如果指定了 init-method()，则再调用制定的init-method
        String initMethodName = mbd.getInitMethodName();
        if (StringUtils.hasLength(initMethodName) &&
                !(isInitializingBean && "afterPropertiesSet".equals(initMethodName)) &&
                !mbd.isExternallyManagedInitMethod(initMethodName)) {
            // 激活用户自定义的初始化方法
            // 利用反射机制执行
            invokeCustomInitMethod(beanName, bean, mbd);
        }
    }
}
```

1. 首先检测当前Bean是否实现了 InitializingBean 接口，如果实现了则调用其方法 - afterPropertiesSet

1. 随后再检查是否制定了  `init-method` 方法，如果制定了则通过反射调用


我们一般不实现 InitializingBean 接口，因为这是具有侵入性的，我们一般使用 init-method 方法

# init-method&destroy-method

## 实例
```java
public class InitializingBeanTest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOtherName(){
        System.out.println("InitializingBeanTest setOtherName...");
        this.name = "chenssy 3 号";
    }
}
```
配置
```xml
<bean id="initializingBeanTest" class="org.springframework.core.test.InitializingBeanTest"
        init-method="setOtherName">
    <property name="name" value="chenssy 1 号"/>
</bean>
```
输出
```
InitializingBeanTest setOtherName...
chenssy 3 号
```

# 总结

InitializingBean 和 DisposableBean 接口可以指定初始化和销毁的的钩子方法，由于实现了接口带来了侵入性，所以可以使用基于XML方法的  init-method 和 destroy-method 方法。

但是现在普遍舍弃了XML配置方式，转而采用注解方法 我们就可以采用   `@PostConstruct` 和 `@PreDestory` 注解 来代替它们。
