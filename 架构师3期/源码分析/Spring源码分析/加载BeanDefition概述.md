
# 加载beanDefition概述

首先我们看下面一段代码

```java
//1. 找到配置文件,并加载为Resource
ClassPathResource resource = new ClassPathResource("bean.xml"); // <1>

//2. 获取BeanFactory
DefaultListableBeanFactory factory = new DefaultListableBeanFactory(); // <2>

//3. 使用新构造的 Beanfactory 创建 一个 BeanDefitionReader 对象
XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory); // <3>

//4. 将资源转化为beanDefition
reader.loadBeanDefinitions(resource); // <4>
```

以上整个过程可以分为三个步骤，资源定位、装载、注册

<img src="../../../../java-notes/img/BeanDefition加载-1.jpg">

- **资源定位**：我们现在一般使用XML、java Bean等来描述Bean对象，所以在IoC容器初始化第一步就是定位资源

- **BeanDefinition 的装载和解析**：也就是BeanDefition的装入。BeanDefitionReader 读取、解析Resource资源、并将Resource转换为BeanDefition

    - 在IoC内部维护着一个BeanDefition map的数据结构

    - 在配置文件中每一个 `<bean>`、`@Bean` 都对应着一个BeanDefition 对象

- **BeanDefinition注册**：向IoC容器注册在解析好的 BeanDefition ，这个过程是通过 BeanDefitionRegistry 实现的。IoC使用Map维护这些 BeanDefition

    - 注意此时并没有进行 `依赖注入`（bean创建） ，Bean创建是第一次调用getBean 时

    - 我们可以通过 `lazyinit = false` 属性，让这个bean 的依赖注入，在容器初始化时完成


# Resource定位

我们之前讲过Spring提供了统一资源抽象接口 - Resource 和 统一资源加载工具 - ResourceLoader。接下来我们使用Spring完成资源定位

在上面的代码中前两步都是为了，前面的两步只是定义了资源 - Resource，第三行代码则定义了 ResourceLoader
```java
//3. 使用新构造的Beanfactory创建一个BeanDefitionReader对象
XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory); // <3>
```
我来来看一下 `XmlBeanDefinitionReader` 的构造方法

```java
// XmlBeanDefinitionReader.java
public XmlBeanDefinitionReader(BeanDefinitionRegistry registry) {
    super(registry);
}
```
它调用了父类构造方法
```java
protected AbstractBeanDefinitionReader(BeanDefinitionRegistry registry) {
    Assert.notNull(registry, "BeanDefinitionRegistry must not be null");
    this.registry = registry;

    // 核心 - 确定要是用的ResourceLoader
    //如果设置了Resourceloader 则使用设置的
    if (this.registry instanceof ResourceLoader) {
        this.resourceLoader = (ResourceLoader) this.registry;
    }else {
        //如果没有设置则使用 PathMatchingResourcePatternResolver 
        this.resourceLoader = new PathMatchingResourcePatternResolver();
    }

    // 继承环境
    if (this.registry instanceof EnvironmentCapable) {
        this.environment = ((EnvironmentCapable) this.registry).getEnvironment();
    }
    else {
        this.environment = new StandardEnvironment();
    }
}
```
# BeanDefinition 的载入和解析

在下面这行代码中我们将载入和解析，上面定义的资源。

```java
//4. 将资源转化为beanDefition
reader.loadBeanDefinitions(resource); // <4
```
将XML转换为bean
```java
// XmlBeanDefinitionReader.java
@Override
public int loadBeanDefinitions(Resource resource) 
    throws BeanDefinitionStoreException {
    return loadBeanDefinitions(new EncodedResource(resource));
}
```

接续向下看

```java
public int loadBeanDefinitions(EncodedResource encodedResource) 
    throws BeanDefinitionStoreException {
    Assert.notNull(encodedResource, "EncodedResource must not be null");
    if (logger.isTraceEnabled()) {
        logger.trace("Loading XML bean definitions from " + encodedResource);
    }

    Set<EncodedResource> currentResources = this.resourcesCurrentlyBeingLoaded.get();

    if (!currentResources.add(encodedResource)) {
        throw new BeanDefinitionStoreException(
                "Detected cyclic loading of " + encodedResource + 
                    " - check your import definitions!");
    }

    //将XML文件转化为IO流
    try (InputStream inputStream = encodedResource.getResource().getInputStream()) {
        // 从 InputStream 中得到 XML 的解析源
        InputSource inputSource = new InputSource(inputStream);
        if (encodedResource.getEncoding() != null) {
            inputSource.setEncoding(encodedResource.getEncoding());
        }
        // ... 具体的读取过程
        return doLoadBeanDefinitions(inputSource, encodedResource.getResource());
    }
    catch (IOException ex) {
        throw new BeanDefinitionStoreException(
                "IOException parsing XML document from " 
                    + encodedResource.getResource(), ex);
    }
    finally {
        currentResources.remove(encodedResource);
        if (currentResources.isEmpty()) {
            this.resourcesCurrentlyBeingLoaded.remove();
        }
    }
}
```
从 encodedResource 源中获取 xml 的解析源，然后调用 #doLoadBeanDefinitions(InputSource inputSource, Resource resource) 方法，执行具体的解析过程。
```java
// XmlBeanDefinitionReader.java

protected int doLoadBeanDefinitions(InputSource inputSource, Resource resource)
            throws BeanDefinitionStoreException {
    try {
        // 获取 XML Document 实例
        Document doc = doLoadDocument(inputSource, resource);
        // 根据 Document 实例，注册 Bean 信息
        int count = registerBeanDefinitions(doc, resource);
        return count;
    }
    // 省略捕获异常
}
```
在该方法中主要做两件事：

1. 根据 xml 解析源获取相应的 Document 对象

1. 调用 #registerBeanDefinitions(Document doc, Resource resource) 方法，开启 BeanDefinition 的解析注册过程


## 转换为 Document 对象

1. 调用` #doLoadDocument(InputSource inputSource, Resource resource)` 方法，会将 Bean 定义的资源转换为 Document 对象。代码如下：

    ```java
    // XmlBeanDefinitionReader.java

    protected Document doLoadDocument(InputSource inputSource, Resource resource) 
        throws Exception {
        return this.documentLoader.loadDocument(
                    inputSource, 
                    getEntityResolver(),
                    this.errorHandler,
                    getValidationModeForResource(resource),
                    isNamespaceAware());
    }
    ```
    参数解析：

    - inputSource ：加载 Document 的 Resource 源。

    - entityResolver ：解析文件的解析器。

    - errorHandler ：处理加载 Document 对象的过程的错误。
    - validationMode ：验证模式。

    - namespaceAware ：命名空间支持。如果要提供对 XML 名称空间的支持，则为 true

1. 调用 `loadDocument(...)` 方法，在类 DefaultDocumentLoader 中提供了实现。代码如下：
    ```java
    // DefaultDocumentLoader.java

    @Override
    public Document loadDocument(InputSource inputSource, 
                                EntityResolver entityResolver,
                                ErrorHandler errorHandler, 
                                int validationMode, 
                                boolean namespaceAware) throws Exception {
        // 创建 DocumentBuilderFactory
        DocumentBuilderFactory factory = 
            createDocumentBuilderFactory(validationMode, namespaceAware);
        // 创建 DocumentBuilder
        DocumentBuilder builder = createDocumentBuilder(factory, 
            entityResolver, errorHandler);
        // 解析 XML InputSource 返回 Document 对象
        return builder.parse(inputSource);
    }
    ```
## 将Document 对象解析为BeanDefition对象

上面我们已经将XML文件转换为Document 对象，下一步我们将Document 对象解析为beanDefiton对象，并注册到IoC容器当中。

```java
// 根据 Document 实例，注册 Bean 信息
int count = registerBeanDefinitions(doc, resource);
```
继续向下看

```java
// XmlBeanDefinitionReader.java

public int registerBeanDefinitions(Document doc, Resource resource) 
    throws BeanDefinitionStoreException {
    // 创建 BeanDefinitionDocumentReader 对象
    BeanDefinitionDocumentReader documentReader = createBeanDefinitionDocumentReader();
    // 获取已注册的 BeanDefinition 数量
    int countBefore = getRegistry().getBeanDefinitionCount();
    // 创建 XmlReaderContext 对象
    // 注册 BeanDefinition
    documentReader.registerBeanDefinitions(doc, createReaderContext(resource));
    // 计算新注册的 BeanDefinition 数量
    return getRegistry().getBeanDefinitionCount() - countBefore;
}
```
1. 首先我们创建了 `BeanDefinitionDocumentReader` 对象，用于解析  `Document`对象

1. 随后调用 `documentReader.registerBeanDefinitions(doc, createReaderContext(resource));` 方法开启解析过程，，具体的实现由子类DefaultBeanDefinitionDocumentReader 完成。代码如下：

    ```java
    // DefaultBeanDefinitionDocumentReader.java

    @Override
    public void registerBeanDefinitions(Document doc ,
                                        XmlReaderContext readerContext) {
        this.readerContext = readerContext;
        // 获得 XML Document Root Element
        // 执行注册 BeanDefinition
        doRegisterBeanDefinitions(doc.getDocumentElement());
    }
    ```

### 对 Document 对象的解析

我们查看 doRegisterBeanDefinitions 方法实现，注意我们这里传入了 Document 的root节点
```java
protected void doRegisterBeanDefinitions(Element root) {

    BeanDefinitionParserDelegate parent = this.delegate;

    // 解析前处理 - 空实现
    preProcessXml(root);
    //解析
    parseBeanDefinitions(root, this.delegate);
    //解析后处理 - 空实现
    postProcessXml(root);

    this.delegate = parent;
}
```
- `preProcessXml`、`postProcessXml` 为前置、后置增强处理，目前 Spring 中都是空实现。

- `parseBeanDefinitions` 是对根元素 root 的解析注册过程。代码如下：
    ```java
    // DefaultBeanDefinitionDocumentReader.java

    protected void parseBeanDefinitions(Element root, 
                                    BeanDefinitionParserDelegate delegate) {
        // 如果根节点使用默认命名空间，执行默认解析
        if (delegate.isDefaultNamespace(root)) {
            // 遍历子节点
            NodeList nl = root.getChildNodes();
            for (int i = 0; i < nl.getLength(); i++) {
                Node node = nl.item(i);
                if (node instanceof Element) {
                    Element ele = (Element) node;
                    // 如果该节点使用默认命名空间，执行默认解析
                    if (delegate.isDefaultNamespace(ele)) {
                        parseDefaultElement(ele, delegate);
                    // 如果该节点非默认命名空间，执行自定义解析
                    } else {
                        delegate.parseCustomElement(ele);
                    }
                }
            }
        // 如果根节点非默认命名空间，执行自定义解析
        } else {
            delegate.parseCustomElement(root);
        }
    }
    ```

#### 默认标签解析
调用 `parseDefaultElement` 方法进行解析
```java
// DefaultBeanDefinitionDocumentReader.java

private void parseDefaultElement(Element ele, 
                                BeanDefinitionParserDelegate delegate) {
    if (delegate.nodeNameEquals(ele, IMPORT_ELEMENT)) { // import
        importBeanDefinitionResource(ele);
    } else if (delegate.nodeNameEquals(ele, ALIAS_ELEMENT)) { // alias
        processAliasRegistration(ele);
    } else if (delegate.nodeNameEquals(ele, BEAN_ELEMENT)) { // bean
        processBeanDefinition(ele, delegate);
    } else if (delegate.nodeNameEquals(ele, NESTED_BEANS_ELEMENT)) { // beans
        // recurse
        doRegisterBeanDefinitions(ele);
    }
}
```
对四大标签：`<import>`、`<alias>`、`<bean>`、`<beans>` 进行解析。


#### 自定义解析
对于自定义标签则由 `parseCustomElement` 方法，负责解析。代码如下：

```java
// BeanDefinitionParserDelegate.java

@Nullable
public BeanDefinition parseCustomElement(Element ele) {
    return parseCustomElement(ele, null);
}

@Nullable
public BeanDefinition parseCustomElement(Element ele, 
                                        @Nullable BeanDefinition containingBd) {
    // 获取 namespaceUri
    String namespaceUri = getNamespaceURI(ele);
    if (namespaceUri == null) {
        return null;
    }
    // 根据 namespaceUri 获取相应的 Handler
    NamespaceHandler handler = this.readerContext
                        .getNamespaceHandlerResolver().resolve(namespaceUri);
    if (handler == null) {
        error("Unable to locate Spring NamespaceHandler for XML schema namespace [" 
            + namespaceUri + "]", ele);
        return null;
    }
    // 调用自定义的 Handler 处理
    return handler.parse(ele, 
                        new ParserContext(this.readerContext, this, containingBd));
}
```
获取节点的 `namespaceUri`，然后根据该 `namespaceUri` 获取相对应的 `NamespaceHandler`，最后调用 `NamespaceHandler` 的 #`parse(Element element, ParserContext parserContext)` 方法，即完成自定义标签的解析和注入。

### 注册 BeanDefinition
在上面我们已经对Document 对象的解析，现在我们将其特定的子节点 -  Element，转换为beanDefition
```java
protected void processBeanDefinition(Element ele, 
                                    BeanDefinitionParserDelegate delegate) {
    //1. 我们进行xml的Bean节点解析
    //   当解析成功后返回 BeanDefinitionHolder 对象
    //   BeanDefinitionHolder 对象内封装了beanDefiton对象和别名 - alias
    BeanDefinitionHolder bdHolder = delegate.parseBeanDefinitionElement(ele);
    if (bdHolder != null) {

        //进行自定义的 - 标签处理 - 采用装饰器模式
        bdHolder = delegate.decorateBeanDefinitionIfRequired(ele, bdHolder);
        try {
            
            // 进行 BeanDefinition 的注册
            BeanDefinitionReaderUtils.registerBeanDefinition(bdHolder, 
                                                getReaderContext().getRegistry());
        }
        catch (BeanDefinitionStoreException ex) {
            getReaderContext()
                .error("Failed to register bean definition with name '" 
                +bdHolder.getBeanName() + "'", ele, ex);
        }
        // 发出响应事件，通知相关的监听器，已完成该 Bean 标签的解析
        getReaderContext().fireComponentRegistered(new BeanComponentDefinition(bdHolder));
    }
}
```
调用 `registerBeanDefinition()` 方法。其实就是调用 `BeanDefinitionRegistry` 的 `registerBeanDefinition`方法，来注册 `BeanDefinition` 。不过，最终的实现是在 `DefaultListableBeanFactory` 中实现，代码如下：

```java
public static void registerBeanDefinition(
        BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry)
        throws BeanDefinitionStoreException {

    // Register bean definition under primary name.
    String beanName = definitionHolder.getBeanName();
    //调用BeanDefinitionRegistry的registerBeanDefinition方法
    //具体实现由DefaultListableBeanFactory进行实现
    registry.registerBeanDefinition(beanName, definitionHolder.getBeanDefinition());

    //注册别名
    String[] aliases = definitionHolder.getAliases();
    if (aliases != null) {
        for (String alias : aliases) {
            registry.registerAlias(beanName, alias);
        }
    }
}
```


具体实现
```java
// DefaultListableBeanFactory.java
@Override
public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition)
        throws BeanDefinitionStoreException {
    // ...省略校验相关的代码
    // 从缓存中获取指定 beanName 的 BeanDefinition
    BeanDefinition existingDefinition = this.beanDefinitionMap.get(beanName);
    // 如果已经存在
    if (existingDefinition != null) {
        // 如果存在但是不允许覆盖，抛出异常
        if (!isAllowBeanDefinitionOverriding()) {
             throw new BeanDefinitionOverrideException(beanName, 
                beanDefinition, 
                existingDefinition);
        } else {
           // ...省略 logger 打印日志相关的代码
        }
        // 【重点】允许覆盖，直接覆盖原有的 BeanDefinition 到 beanDefinitionMap 中。
        this.beanDefinitionMap.put(beanName, beanDefinition);
    // 如果未存在
    } else {
        // ... 省略非核心的代码
        // 【重点】添加到 BeanDefinition 到 beanDefinitionMap 中。
        this.beanDefinitionMap.put(beanName, beanDefinition);
    }
    // 重新设置 beanName 对应的缓存
    if (existingDefinition != null || containsSingleton(beanName)) {
        resetBeanDefinition(beanName);
    }
}
```
这段代码最核心的部分是这句` this.beanDefinitionMap.put(beanName, beanDefinition)` 代码段。所以，注册过程也不是那么的高大上，就是利用一个 Map 的集合对象来存放：key 是 beanName ，value 是 BeanDefinition 对象。

# 总结

<img src="../../../../java-notes/img/beandefition加载流程.jpg">

