
# 事务

事务是逻辑上的**一组操作**，要么都执行，要么都不执行。

## 事务特性ACID

- **原子性（Atomicity）**：事务是最小的执行单位，不允许分割。事务的原子性保证要么全部完成，要么完全不起作用

- **一致性（Consistency）**：是指事务执行结束后，数据库的完整性约束没有被破坏，事务执行的前后都是合法的数据状态。

- **隔离性（Isolation）**：并发访问数据库时，一个用户的事务不被其他事务所干扰，各个并发事务之间数据库是独立的。

- **持久性（Durability）**：一个事务被提交后。它对数据库的数据改变时持久的，即使数据库发生故障不应该对其有任何影响。

# Spring事务使用

由于现在普遍使用Spring Boot 进行开发，这里不再记录XML配置方式。

## 开启事务

在启动类上添加注解： 

> @EnableTransactionManagement

![20201130155047](http://qiniu.liulei.life/20201130155047.png)

不过该注解在大多数情况下并不是必须的，因为 `Spring boot` 在 `TransactionAutoConfiguration` 类里为我们自动配置启用了`@EnableTransactionManagement`注解。不过自动启用该注解有两个前提条件，分别是：`@ConditionalOnBean(PlatformTransactionManager.class)`和`@ConditionalOnMissingBean(AbstractTransactionManagementConfiguration.class)，`而一般情况下，这两个条件都是满足的，所以一般的，我们在启动类上写不写注解都行。本人这里还是建议写出来。


## 使用事务
我们将 `@Transactional` 标注在方法上即可
![20201130155536](http://qiniu.liulei.life/20201130155536.png)

`@Transactional` 注解有以下几个属性


- **readOnly**：

    该属性设置当前事务为只读事务，这跟sql执行无关Spring 会优化只读事务的效率

- **rollbackClassName**：

    该属性设置进行回滚的异常类名称数组，当方法中抛出指定异常时，进行事务回滚。
    
    例如：`@Transactional(rollbackForClassName={"RuntimeException","Exception"}`

- **rollbackFor**：

    该属性设置进行回滚的异常类数组，当方法中抛出指定异常时，进行事务回滚。
    
    例如：`@Transactional(rollbackFor={RuntimeException.class, Exception.class}`

- **noRollbackForClassName**：

    该属性设置不需要进行回滚的异常类名称数组

    例如：`@Transactional(noRollbackForClassName={"RuntimeException","Exception"}`

- **noRollbackFor**：

    该属性设置不需要进行回滚的异常数组

    例如：`@Transactional(noRollbackFor={RuntimeException.class, Exception.class}`

- **propagation**：

    该属性设置事务传播行为。有枚举类 `Propagation` 或者使用 `TransactionDefinition` 中定义的常量

    - **REQUIRED** ：如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。 

    - **SUPPORTS** ：如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。 

    - **MANDATORY** ：如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。 

    - **REQUIRES_NEW** ：创建一个新的事务，如果当前存在事务，则把当前事务挂起。 

    - **NOT_SUPPORTED** ：以非事务方式运行，如果当前存在事务，则把当前事务挂起。 

    - **NEVER** ：以非事务方式运行，如果当前存在事务，则抛出异常。 

    - **NESTED** ：如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于 REQUIRED 。 

    指定方法：通过使用 propagation 属性设置，例如：@Transactional(propagation = Propagation.REQUIRED)

- **isolation**：

    事务隔离级别

    有枚举类 `Isolation ` 和 `TransactionDefinition` 中定义的常量

    - `DEFAULT` ：默认值，表示使用底层数据库的默认隔离级别。大部分数据库为`READ_COMMITTED`(MySql默认隔离级别为REPEATABLE) 

    - `READ_UNCOMMITTED` ：该隔离级别表示一个事务可以读取另一个事务修改但还没有提交的数据。该级别不能防止脏读和不可重复读，因此很少使用该隔离级别。 

    - `READ_COMMITTED` ：该隔离级别表示一个事务只能读取另一个事务已经提交的数据。该级别可以防止脏读，这也是大多数情况下的推荐值。 

    - `REPEATABLE_READ` ：该隔离级别表示一个事务在整个过程中可以多次重复执行某个查询，并且每次返回的记录都相同。即使在多次查询之间有新增的数据满足该查询，这些新增的记录也会被忽略。该级别可以防止脏读和不可重复读。 

    - `SERIALIZABLE` ：所有的事务依次逐个执行，这样事务之间就完全不可能产生干扰，也就是说，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下也不会用到该级别。 

- **timeout**

    该属性用于设置事务的超时秒数，默认值为-1表示永不超时


在复杂场景中我们可能需要手动回滚事务，我们直接注入使用 `DataSourceTransactionManager` 对象即可
```java
@Autowired
DataSourceTransactionManager transactionManager;
```


# Spring事务管理接口

Spring 主要定义了以下三个接口，进行事务的管理：

- **PlatformTransactionManager**：事务管理器

- **TransactionDefinition**：事务定义信息(事务隔离级别、传播行为、超时、只读、回滚规则)

- **TransactionStatus**：事务运行状态

事务管理机制就是 - 按照给定的事务规则提交或者回滚操作。


## PlatformTransactionManager - 事务管理器接口

**Spring 并不直接管理事务，而是提供了接口**，具体的职责是由 Hibernate 或者 JTA等框架来实现的。具体实现如下：

![20201126155548](http://qiniu.liulei.life/20201126155548.png)

我们继续来看一下这个接口的定义：

```java
//org.springframework.transaction.PlatformTransactionManager

Public interface PlatformTransactionManager()...{  
    //根据指定的传播行为，返回当前活动的事务或创建一个新事务。
    TransactionStatus getTransaction(TransactionDefinition definition); 

    //使用事务目前的状态提交事务
    Void commit(TransactionStatus status);  

    //对执行的事务进行回滚
    Void rollback(TransactionStatus status);  
}
```

当我们使用 JDBC 或者 MyBatis 进行数据持久化操作时，我们的XML一般这样配置
```xml
<!-- 事务管理器 -->
<bean id="transactionManager"
    class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <!-- 数据源 -->
    <property name="dataSource" ref="dataSource" />
</bean>
```

## TransactionDefinition - 事务属性定义接口

事务管理器接口 `PlatformTransactionManager` 通过` getTransaction(TransactionDefinition definition)` 方法来得到一个事务，这个方法里面的参数是 `TransactionDefinition`  ，这个类就定义了一些基本的事务属性。


事务属性包含以下五个方面：

![20201126161034](http://qiniu.liulei.life/20201126161034.png)


### 接口方法定义

我们首先来看接口定义的方法
```java
public interface TransactionDefinition {

    // 返回事务的传播行为
    int getPropagationBehavior(); 

    // 返回事务的隔离级别，
    //      事务管理器根据它来控制另外一个事务可以看到本事务内的哪些数据
    int getIsolationLevel(); 

    //返回事务的名字
    String getName()；

    // 返回事务必须在多少秒内完成
    int getTimeout();  

    // 返回是否优化为只读事务。
    boolean isReadOnly();
} 
```

### 事务隔离级别

在典型的应用程序中，多个事务并发运行，当操作相同的数据时，会产生了以下并发问题，针对这些并发问题，MYSQL 提供了事务隔离级别，Spring 的事务机制基于MYSQL等数据库也提供了事务隔离级别。


#### 数据库并发问题

- **脏读（Dirty read）**：当有A、B两个事务执行时，A事务首先对数据进行修改，但此时并未提交到数据库，这时B事务，也访问了这个数据，但是读取到A事务修改后的数据。那么我们认为B数据读取到的是**脏数据**。

- **丢失修改（lost to modify）**：当有A、B两个事务，A B 同时读取数据，A首先进行数据修改，随后B进行数据修改，B修改后的数据覆盖掉A的修改数据。A的修改结果丢失，这就称之为修改丢失.

    实例：A B 事务读取 数据a = 20，A 修改 a = a - 1，B也修改 a = a - 1 ，最终结果为19，事务A的修改丢失。

- **不可重复读（Unrepeatable read）**：这是由于查询时系统中其他事务提交引起的。例如T1 读取某一个数据。T2 读取并修改了该数据，T1再次读取该数据得到了不同的结果。

- **幻读（Phantom read）**：幻读是指当事务不是独立执行时发生的一种现象。事务A读取与搜索条件相匹配的若干行。事务B以插入相同条件的数据。事务A发现了事务B提交的数据，称之为幻读

#### MySQL事务隔离级别
<table>
    <tr>
        <th>事务隔离级别</th>
        <th>脏读</th>
        <th>不可重复读</th>
        <th>幻读</th>
    </tr>
    <tr>
        <td>读未提交 - read-uncommitted</td>
        <td>是</td>
        <td>是</td>
        <td>是</td>
    </tr>
    <tr>
        <td>读已提交 - read-committed</td>
        <td>否</td>
        <td>是</td>
        <td>是</td>
    </tr>
    <tr>
        <td>可重复读 - repeatable-read</td>
        <td>否</td>
        <td>否</td>
        <td>是</td>
    </tr>
    <tr>
        <td>串行化 - serializable</td>
        <td>否</td>
        <td>否</td>
        <td>否</td>
    </tr>
</table>


MySql 默认的事务隔离级别为 `repeatable-read`

![20201127112956](http://qiniu.liulei.life/20201127112956.png)

查看和设置隔离级别使用以下命令

- 查看当前的隔离级别:

    > SELECT @@tx_isolation;
    
- 设置当前 mySQL 连接的隔离级别:

    > set transaction isolation level read committed;


- 设置数据库系统的全局的隔离级别:

    > set global transaction isolation level read committed;




#### Spring事务隔离级别

在Spring 中 `TransactionDefinition` 几口定义了以下五个常量表示隔离级别：


- **TransactionDefinition.ISOLATION_DEFAULT**：使用数据库默认隔离级别。如 MySQL 就是 `repeatable-read` ，Oracle 默认则是 `read-committed`

- **TransactionDefinition.ISOLATION_READ_UNCOMMITTED**：最低的隔离级别，允许读取尚未提交的数据变更，可能会导致**脏读、幻读或不可重复读**

- **TransactionDefinition.ISOLATION_READ_COMMITTED**： 允许读取并发事务已经提交的数据，**可以阻止脏读，但是幻读或不可重复读仍有可能发生**

- **TransactionDefinition.ISOLATION_REPEATALE_READ**：对同一字段多次读取是一致的，除非数据是被本身事务自己所修改，**可以阻止脏读和不可重复读，但可能发生幻读**

- **TransactionDefinition.ISOLATION_SERIALIZABLE**：最高的隔离级别，所有事务依次执行，这样事务之间完全不可能产生干扰，**可以阻止脏读、不可重复读、幻读，但性能严重下降**

### 事务传播行为

当事务方法调用另一个事务方法时，必须指定执行事务应该如何传播。例如 事务A 方法 调用B方法，B方法有可能在A方法的事务中运行，也可能开启一个新事务中运行。

`TransactionDefinition` 中定义了几个表示事务传播行为的常量，这些传播行为分为以下几个情况：

**支持当前事务**

- **TransactionDefinition.PROPAGATION_REQUIRED**：如果当前存在事务，则加入该事务，如果当前没有事务则创建一个新的事务
 
- **TransactionDefinition.PROPAGATION_SUPPORTS**：如果当前存在事务，则以事务的方式运行，如果不存在则以非事务方式运行

- **TransactionDefinition.PROPAGATION_MANDATORY**：如果当前存在事务则加入该事务，如果没有则抛出异常


**不支持当前事务**

- **TransactionDefinition.PROPAGATION_REQUIRES_NEW**：创建一个新的事务，如果当前存在事务，就把当前事务挂起

- **TransactionDefinition.PROPAGATION_NOT_SUPPORTED**：以非事务运行，如果当前存在事务，则把当前事务挂起

- **TransactionDefinition.PROPAGATION_NEVWR**：以非事务运行，如果存在当前事务，则抛出异常

**其他情况**

- **TransactionDefinition.PROPAGATION_NESTED**：创建一个事务，如果当前存在事务则以嵌套方式，否则正常运行

这里需要说明的是，`TransactionDefinition.PROPAGATION_NESTED` 的嵌套事务(如果存在外部事务的话)，并不是一个独立的事务，它依赖外部的事务，只有外部事务的提交，才能引起内部事务的提交，嵌套的事务不能单独提交。如果了解JDBC中的保存点 - SavePoint的概念，那嵌套事务就很容易理解了，其实嵌套的子事务就是保存点的一个应用，一个事务中可以包括多个保存点。另外事务的回滚也会引起子事务的回滚。




### 事务超时

事务超时，就是指一个事务所允许执行的最长时间，如果超过时间该事务还没有完成，则自动回滚该事务。

在 `TransactionDefinition` 中以 int 值来表示超时时间，其单位是秒。


### 事务只读

spring中readOnly的定义，并不是不能在事务中进行修改等DML操作，它只是一个“暗示”，提示数据库驱动程序和数据库系统，这个事务并不包含更改数据的操作，那么JDBC驱动程序和数据库就有可能根据这种情况对该事务进行一些特定的优化，比方说不安排相应的数据库锁，以减轻事务对数据库的压力，毕竟事务也是要消耗数据库的资源的。

**只读事务仅仅是一个性能优化的推荐配置而已，并非强制你非要这样处理不可。**

结合源码的一个解读分析具体是通过什么方式来进行优化：

如果是只读事务，Spring会将transactionContext的 isFlushModeNever 设置为true；

当事务被标识为只读事务时，Spring可以对某些可以针对只读事务进行优化的资源就可以执行相应的优化措施，上面Spring告之hibernate的session在只读事务模式下不用尝试检测和同步持久对象的状态的更新。

总结：
如果在使用事务的情况下，所有操作都是读操作，那建议把事务设置成只读事务，或者事务的传播途径最好能设置为 supports （运行在当前的事务范围内，如果当前没有启动事务，那么就不在事务范围内运行）或者 not supports （不在事务范围内执行，如果当前启动了事务，那么挂起当前事务），这样不在事务下，就不会调用transactionContext.managedFlush(); 方法。
所有只读事务与读写事务的比较大的运行性能区别就是只读事务避免了Hibernate的检测和同步持久对象的状态的更新,提升了运行性能。

### 回滚规则

这些规则定义了哪些异常会导致事务回滚而哪些不会。默认情况下，事务只有遇到运行期异常时才会回滚，而在遇到检查型异常时不会回滚（这一行为与EJB的回滚行为是一致的）。

但是你可以声明事务在遇到特定的检查型异常时像遇到运行期异常那样回滚。同样，你还可以声明事务遇到特定的异常不回滚，即使这些异常是运行期异常。也可以手动抛出异常来让事务进行回滚。



## TransactionStatus - 事务状态

`TransactionStatus` 接口用来记录事务的状态 该接口定义了一组方法,用来获取或判断事务的相应状态信息.

`PlatformTransactionManager.getTransaction(…)` 方法返回一个 `TransactionStatus` 对象。该 对象可能代表一个新的或已经存在的事务（如果在当前调用堆栈有一个符合条件的事务）。

`TransactionStatus` 接口接口内容如下：
```java
public interface TransactionStatus{
    boolean isNewTransaction(); // 是否是新的事物
    boolean hasSavepoint(); // 是否有恢复点
    void setRollbackOnly();  // 设置为只回滚
    boolean isRollbackOnly(); // 是否为只回滚
    boolean isCompleted; // 是否已完成
} 
```

## 事务拦截器
我们知道 Spring Aop 对目标类的增强是通过创建代理类，调用 `拦截器链` 实现的。在事务中则是通过 `TransactionInterceptor` - 事务拦截器来实现事务的创建、提交回滚的。

# Spring事务源码
在我们使用JDBC编程时，想要开启事务，就要按照以下步骤：

1. 获取连接 Connection con = DriverManager.getConnection()
1. 开启事务con.setAutoCommit(true/false);
1. 编写sql，执行CRUD
1. 提交事务/回滚事务 con.commit() / con.rollback();
1. 关闭连接 conn.close();


在Spring事务中，我们可以不在写 1、2、4、5的代码，而是由Spring帮我们自动完成，接下来我们来看一下源码


## 加载事务组件

我们知道事务是通过 AOP 来实现的，而Spring 核心就是 IoC 注入，所以我们需要解析配置，然后在通过AOP生成代理，最后注入。接下来我们来看一下是如何加载 AOP 组件的。

xml 与 注解除了解析资源（一个是解析xml，一个是扫描类）略有不同，但加载方式是相同的，都是通过 `TxNamespaceHandler` 来进行加载的。

```java
public class TxNamespaceHandler extends NamespaceHandlerSupport {
    static final String TRANSACTION_MANAGER_ATTRIBUTE = "transaction-manager";
    static final String DEFAULT_TRANSACTION_MANAGER_BEAN_NAME = "transactionManager";
    static String getTransactionManagerName(Element element) {
        return (element.hasAttribute(TRANSACTION_MANAGER_ATTRIBUTE) ?
                element.getAttribute(TRANSACTION_MANAGER_ATTRIBUTE) : 
                DEFAULT_TRANSACTION_MANAGER_BEAN_NAME);
    }

    @Override
    public void init() {
        //注册解析器
        registerBeanDefinitionParser("advice", 
            new TxAdviceBeanDefinitionParser());
        registerBeanDefinitionParser("annotation-driven", 
            new AnnotationDrivenBeanDefinitionParser());
        registerBeanDefinitionParser("jta-transaction-manager", 
            new JtaTransactionManagerBeanDefinitionParser());
    }
}
```
通过该类注册的解析器，来进一步向IoC，注册事务管理器、后置处理器（生成事务代理类）、事务拦截器等类。具体的可以看一下上面注册的这三个解析器，我们这里不再赘述。




## 执行事务增强器

`BeanFactoryTransactionAttributeSourceAdvisor` 就是事务的增强器