
# 概念

Spring 为我们提供了丰富的扩展点，其中 `BeanPostProcessor` 接口能够，帮助我们在Bean实例化后，对其进行一些配置，增加一些自己的处理逻辑。






# BeanPostProcessor基本使用
```java
@Configuration
public class BeanPostProcessorDemo implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) 
        throws BeansException {

        System.out.println("Before bean = " + bean);
        // 这里一定要返回 bean，不能返回 null
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) 
        throws BeansException {

        System.out.println("after bean = " + bean);
        return bean;
    }
}
```
输出
```java
Before bean = com.example.demoprototype.DemoPrototypeApplication$$EnhancerBySpringCGLIB$$df3fac14@55f45b92
after bean = com.example.demoprototype.DemoPrototypeApplication$$EnhancerBySpringCGLIB$$df3fac14@55f45b92
.省略
.省略
.省略
Before bean = org.springframework.boot.task.TaskSchedulerBuilder@72be135f
after bean = org.springframework.boot.task.TaskSchedulerBuilder@72be135f
```
在每一个Bean前后执行 before 和 after 方法

# BeanPostProcessor原理

我们首先开看一下定义
```java
package org.springframework.beans.factory.config;

public interface BeanPostProcessor {


    //执行 InitializingBean 接口前执行
    @Nullable
    default Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }


    //执行 InitializingBean 接口后执行
    @Nullable
    default Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

}
```

`beanPostProcessor ` 在本质上就是一个工厂钩子(类似的工厂钩子还有 Aware、 InitalizingBean、DisposableBean)。它对Spring创建的Bean能够进行定制化修改，比较 常见的有处理标记接口实现类、或者为当前对象提供代理如AOP。

一般普通的 beanFactory 是不支持 注册beanPostProcessor的需要手动调用 `addBeanPostProcessor(BeanPostProcessor beanPostProcessor)` 方法进行注册。注册后的 `BeanPostProcessor` 适用于所有的 `Bean`。但是 **ApplicationContext 可以在其Bean定义中自动检测所有的beanPostProcessor,并自动完成注册，同时应用到创建的bean中**

调用步骤如下：

1. 实例化Bean

1. 激活 Aware

1. BeanPostProcessor前置处理

1. 调用实现自 InitializingBean 接口的Bean 的 afterPropertiesSet() 方法

1. BeanPostProcessor后置处理

```java
protected Object initializeBean(String beanName, 
                                Object bean, 
                                @Nullable RootBeanDefinition mbd) {

    if (System.getSecurityManager() != null) {
        AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
            invokeAwareMethods(beanName, bean);
            return null;
        }, getAccessControlContext());
    }
    else {
        invokeAwareMethods(beanName, bean);
    }

    Object wrappedBean = bean;
    if (mbd == null || !mbd.isSynthetic()) {
        //调用前置处理器
        wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);
    }

    try {
        //调用初始化 - 方法
        invokeInitMethods(beanName, wrappedBean, mbd);
    }
    catch (Throwable ex) {
        throw new BeanCreationException(
                (mbd != null ? mbd.getResourceDescription() : null),
                beanName, "Invocation of init method failed", ex);
    }
    if (mbd == null || !mbd.isSynthetic()) {
        //调用后置处理器
        wrappedBean = applyBeanPostProcessorsAfterInitialization(wrappedBean, beanName);
    }

    return wrappedBean;
}
```
## 注册beanPostProcessor

在普通的BeanFactory中我们必须手动调用 `addBeanPostProcessor(BeanPostProcessor beanPostProcessor)` 方法来注册，在 `ApplicationContext` 中会自动帮我们检测并注册。

```java
// AbstractApplicationContext.java

/**
* 实例化并调用已经注入的 BeanPostProcessor
* 必须在应用中 bean 实例化之前调用
*/
protected void registerBeanPostProcessors(ConfigurableListableBeanFactory beanFactory) {
    PostProcessorRegistrationDelegate.registerBeanPostProcessors(beanFactory, this);
}

// PostProcessorRegistrationDelegate.java

public static void registerBeanPostProcessors(
        ConfigurableListableBeanFactory beanFactory, AbstractApplicationContext applicationContext) {

    // 获取所有的 BeanPostProcessor 的 beanName
    // 这些 beanName 都已经全部加载到容器中去，但是没有实例化
    String[] postProcessorNames = beanFactory.getBeanNamesForType(BeanPostProcessor.class, true, false);

    // Register BeanPostProcessorChecker that logs an info message when
    // a bean is created during BeanPostProcessor instantiation, i.e. when
    // a bean is not eligible for getting processed by all BeanPostProcessors.
    // 记录所有的beanProcessor数量
    int beanProcessorTargetCount = beanFactory.getBeanPostProcessorCount() + 1 + postProcessorNames.length;
    // 注册 BeanPostProcessorChecker，它主要是用于在 BeanPostProcessor 实例化期间记录日志
    // 当 Spring 中高配置的后置处理器还没有注册就已经开始了 bean 的实例化过程，这个时候便会打印 BeanPostProcessorChecker 中的内容
    beanFactory.addBeanPostProcessor(new BeanPostProcessorChecker(beanFactory, beanProcessorTargetCount));

    // Separate between BeanPostProcessors that implement PriorityOrdered,
    // Ordered, and the rest.
    // PriorityOrdered 保证顺序
    List<BeanPostProcessor> priorityOrderedPostProcessors = new ArrayList<>();
    // MergedBeanDefinitionPostProcessor
    List<BeanPostProcessor> internalPostProcessors = new ArrayList<>();
    // 使用 Ordered 保证顺序
    List<String> orderedPostProcessorNames = new ArrayList<>();
    // 没有顺序
    List<String> nonOrderedPostProcessorNames = new ArrayList<>();
    for (String ppName : postProcessorNames) {
        // PriorityOrdered
        if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
            // 调用 getBean 获取 bean 实例对象
            BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
            priorityOrderedPostProcessors.add(pp);
            if (pp instanceof MergedBeanDefinitionPostProcessor) {
                internalPostProcessors.add(pp);
            }
        } else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
            // 有序 Ordered
            orderedPostProcessorNames.add(ppName);
        } else {
            // 无序
            nonOrderedPostProcessorNames.add(ppName);
        }
    }

    // First, register the BeanPostProcessors that implement PriorityOrdered.
    // 第一步，注册所有实现了 PriorityOrdered 的 BeanPostProcessor
    // 先排序
    sortPostProcessors(priorityOrderedPostProcessors, beanFactory);
    // 后注册
    registerBeanPostProcessors(beanFactory, priorityOrderedPostProcessors);

    // Next, register the BeanPostProcessors that implement Ordered.
    // 第二步，注册所有实现了 Ordered 的 BeanPostProcessor
    List<BeanPostProcessor> orderedPostProcessors = new ArrayList<>();
    for (String ppName : orderedPostProcessorNames) {
        BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
        orderedPostProcessors.add(pp);
        if (pp instanceof MergedBeanDefinitionPostProcessor) {
            internalPostProcessors.add(pp);
        }
    }
    // 先排序
    sortPostProcessors(orderedPostProcessors, beanFactory);
    // 后注册
    registerBeanPostProcessors(beanFactory, orderedPostProcessors);

    // Now, register all regular BeanPostProcessors.
    // 第三步注册所有无序的 BeanPostProcessor
    List<BeanPostProcessor> nonOrderedPostProcessors = new ArrayList<>();
    for (String ppName : nonOrderedPostProcessorNames) {
        BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
        nonOrderedPostProcessors.add(pp);
        if (pp instanceof MergedBeanDefinitionPostProcessor) {
            internalPostProcessors.add(pp);
        }
    }
    // 注册，无需排序
    registerBeanPostProcessors(beanFactory, nonOrderedPostProcessors);

    // Finally, re-register all internal BeanPostProcessors.
    // 最后，注册所有的 MergedBeanDefinitionPostProcessor 类型的 BeanPostProcessor
    sortPostProcessors(internalPostProcessors, beanFactory);
    registerBeanPostProcessors(beanFactory, internalPostProcessors);

    // Re-register post-processor for detecting inner beans as ApplicationListeners,
    // moving it to the end of the processor chain (for picking up proxies etc).
    // 加入ApplicationListenerDetector（探测器）
    // 重新注册 BeanPostProcessor 以检测内部 bean，因为 ApplicationListeners 将其移动到处理器链的末尾
    beanFactory.addBeanPostProcessor(new ApplicationListenerDetector(applicationContext));
}
```
上面代码可以分为以下几步

1. 从BeanFactory找到实现了 beanPostProcessor的Bean名称

1. 排序

    迭代这些Bean，并将其按照  PriorityOrdered、Ordered、无序的顺序添加到相应的List集合中

1. 注册

    调用 `AbstractBeanFactory#addBeanPostProcessor(BeanPostProcessor beanPostProcessor)` 方法完成注册

# 小结


1. BeanPostProcessor 的作用是容器级别的，它之和所在的容器有关，当它完成注册后，它会应用于所有跟它所在同一个容器的Bean

1. BeanFactory和ApplicationContext对beanPostProcessor的处理不同，beanFactory需要手动注册，而ApplicationContext可以自动检测并注册beanPostProcessor

1. ApplicationContext 的BeanPostProcessor 支持Ordered，而BeanFactory不支持。
