
# 概述
Spring 作为 IoC框架，实现了依赖注入，有一个中心化的Bean工厂来负责各个Bean的实例化和依赖管理，各个Bean不需要关心各自复杂的创建过程。


Spring 在实现以上功能时，分为两个阶段：容器初始化阶段和加载bean阶段

1. **容器初始化阶段：**

    - 首先，加载元数据，如XML文件，扫描类文件
        (使用 `Resource` 和 `ResourceLoader` 体系)

    - 然后，容器会对元数据进行分解和分析，并将分析的信息组成 `BeanDefition`

    - 最后，将 `BeanDefition` 保存注册到相应的 `BeanDefitionRegisty` 中。至此Spring IoC初始化完成

1. **加载Bean阶段：**

    - 当我们显式或者隐式调用 `BeanFactory.getBean(..)` 时，则会触发`Bean`的加载

    - 当 `Bean` 开始加载时,首先会检查 Bean 是否在容器中有注册过(缓存)，如果没有则会依据 `beanDefinition` 实例化 `Bean`,并将其注册到容器中，随后返回。

上述加载流程可以描述为下图

![20201029104722](http://qiniu.liulei.life/20201029104722.png)

我们知道对象是存在单例和原型(多例)的，和互相依赖的所以对象有着以下两方面的约束：
- **作用域**：单例全局初始化一次即可，原型每次引用都会创建。

- **依赖关系**：一个Bean如果有依赖，我们需要初始化依赖，然后进行关联和注入。如果多个Bean之间存在循环依赖，A依赖B，B依赖C，C又依赖A，需要解决这种循环问题。

当我们首次调用Spring提供的方法时，Bean加载开启：
```java
ApplicationContext context = new ClassPathXmlApplicationContext("hello.xml");
HelloBean helloBean = (HelloBean) context.getBean("hello");
helloBean.sayHello();
```
我们接下来以`getBean`方法作为入口，去理解Bean加载的流程，以及对作用域和依赖关系的处理。

# Bean加载流程

![111611182407_0Bean初始化流程(1)_1](http://qiniu.liulei.life/111611182407_0Bean初始化流程(1)_1.jpg)

从上面的流程图看一个Bean的加载可以分为这么几个阶段

1. **获取BeanName**：对传入的name进行解析，转化为可以从Map中获取BeanDefition的bean name（在xml中bean有id，alias等）

1. **从缓存中加载Bean**：根据转化的Bean name从存储Bean对象的容器中获取BeanDefinition。这一步如果加载到Bean则直接返回

1. **合并Bean定义**：对父类的定义进行 合并和覆盖，如果父类还有父类，则进行递归合并，以获取完整的Bean定义信息

1. **实例化Bean**：使用构造方法或者工厂创建Bean实例

1. **属性填充**：寻找并注入依赖，依赖的Bean还会递归调用getBean方法

1. **初始化**：触发Aware、BeanPostProcessor、调用自己定义的初始化方法

1. **获取最终的Bean**：如果是FactoryBean需要调用getObject方法，如果需要类型转换，则调用TypeConverter进行转换


这就是Bean初始化的简单步骤

## 转换BeanName

我们使用 Map存储BeanDefinition，BeanName作为key，见 `DefaultListableBeanFactory`

```java
/** Map of bean definition objects, keyed by bean name */
private final Map<String, BeanDefinition> beanDefinitionMap = 
                new ConcurrentHashMap<String, BeanDefinition>(256);
```
`BeanFactory.getBean` 中传入的 name，有可能是这几种情况：

- **bean name**: 可以直接获取到BeanDefinition

- **alias name**: 别名，需要转化

- **factoryBean name 带&前缀**: 通过它获取BeanDefinition需要去除 & 前缀

![20201030090013](http://qiniu.liulei.life/20201030090013.png)

调用：A`bstractBeanFactory.doGetBean(String name)`
```java
protected <T> T doGetBean ... {
    ...    
    // 转化工作 
    final String beanName = transformedBeanName(name);
    ...
}
```
### 别名-aliasName

在解析阶段，`alias name` 和 `bean name` 的映射关系被注册到 `SimpleAliasRegistry` 中。从该注册器中取到 `beanName`。见 `SimpleAliasRegistry.canonicalName`：
```java
public String canonicalName(String name) {
    ...
    resolvedName = this.aliasMap.get(canonicalName);
    ...
}
```
### 工厂名-factorybeanName

带有`&`符号的，表示这是个工厂 bean。直接把前缀去掉。见 `BeanFactoryUtils.transformedBeanName` :
```java
public static String transformedBeanName(String name) {
    Assert.notNull(name, "'name' must not be null");
    String beanName = name;
    while (beanName.startsWith(BeanFactory.FACTORY_BEAN_PREFIX)) {
        beanName = beanName.substring(BeanFactory.FACTORY_BEAN_PREFIX.length());
    }
    return beanName;
}
```
## 从缓存中尝试加载Bean
我们知道Spring有三个Map 分别是(earlySingletonObjects、singletonFactories、singletonObjects)，当从缓存中加载Bean时便是从这三个Map中尝试加载

## 标记Bean状态为创建中
`AbstractBeanFactory.doGetBean`

```java
if (!typeCheckOnly) {
    markBeanAsCreated(beanName);
}
```
来看一下 `markBeanAsCreated` 方法

```java
protected void markBeanAsCreated(String beanName) {
    if (!this.alreadyCreated.contains(beanName)) {
        synchronized (this.mergedBeanDefinitions) {
            if (!this.alreadyCreated.contains(beanName)) {
                // Let the bean definition get re-merged now that we're actually creating
                // the bean... just in case some of its metadata changed in the meantime.
                clearMergedBeanDefinition(beanName);
                this.alreadyCreated.add(beanName);
            }
        }
    }
}

//用于存放 创建中的BeanName
/** Names of beans that have already been created at least once. */
private final Set<String> alreadyCreated = 
    Collections.newSetFromMap(new ConcurrentHashMap<>(256));
```
这里我们使用了 `Set` 集合来保存创建中的 `BeanName`



## 合并BeanDefinition

我们从配置文件中读取到的`BeanDefinition`是 `GenericBeanDefinition`。它记录了类的一些属性和构造参数，但是对于父类只用了一个 `parentName` 来记录。
```java
public class GenericBeanDefinition extends AbstractBeanDefinition {
    ...
    private String parentName;
    ...
}
```
接下来会发现一个问题，在后续实例化 Bean 的时候，使用的 BeanDefinition 是 `RootBeanDefinition` 类型而非 `GenericBeanDefinition`。这是为什么？

这是因为 `GenericBeanDefinition` 在有继承关系的情况下，定义的信息不足：

- 如果不存在继承关系，GenericBeanDefinition 存储的信息是完整的，可以直接转化为 RootBeanDefinition。

- 如果存在继承关系，GenericBeanDefinition 存储的是 增量信息 而不是 全量信息。

**为了能够正确初始化对象，需要完整的信息才行。需要递归 合并父类的定义：**


![20201030144821](http://qiniu.liulei.life/20201030144821.png)

```
//AbstractBeanFactory 

protected <T> T doGetBean ... {
    ...
    
    // 合并父类定义
    final RootBeanDefinition mbd = getMergedLocalBeanDefinition(beanName);
        
    ...
        
    // 使用合并后的定义进行实例化
    bean = getObjectForBeanInstance(prototypeInstance, name, beanName, mbd);
        
    ...
}
```
看 `AbstractBeanFactory.getMergedLocalBeanDefinition` 方法：
```java
protected RootBeanDefinition getMergedLocalBeanDefinition(String beanName) 
                                                        throws BeansException {
    //从缓存中取出BeanDefinition
    RootBeanDefinition mbd = this.mergedBeanDefinitions.get(beanName);
    //如果缓存存在，并且不需要合并返回
    if (mbd != null && !mbd.stale) {
        return mbd;
    }
    //递归获取，合并的BeanDefinition
    return getMergedBeanDefinition(beanName, getBeanDefinition(beanName));
}
```
看 `AbstractBeanFactory.getMergedBeanDefinition` 方法：
```java
//AbstractBeanFactory
protected RootBeanDefinition getMergedBeanDefinition(
        String beanName, BeanDefinition bd, BeanDefinition containingBd)
        throws BeanDefinitionStoreException {

    ...
    
    String parentBeanName = transformedBeanName(bd.getParentName());

    ...
    
    // 递归调用，继续合并父类定义
    pbd = getMergedBeanDefinition(parentBeanName);
    
    ...

    // 使用合并后的完整定义，创建 RootBeanDefinition
    mbd = new RootBeanDefinition(pbd);
    
    // 使用当前定义，对 RootBeanDefinition 进行覆盖
    mbd.overrideFrom(bd);

    ...
    return mbd;
}
```

每次合并完父类定义后，都会调用 `RootBeanDefinition.overrideFrom` 对父类的定义进行覆盖，获取到当前类能够正确实例化的 全量信息。




## 创建实例并放入容器

我们已经获取到完整的 `RootBeanDefintion`，然后就可以拿这份定义信息来实例化出具体的 `Bean`


首先来看一下代码
```java
if (mbd.isSingleton()) {
    sharedInstance = getSingleton(beanName, () -> {
        。。。
        return createBean(beanName, mbd, args);
        。。。
    });
    ...
}
```
这行代码中包含两部分，

1. `getSingleton`：该部分维护容器相关操作，`三个Map`、`检测构造循环依赖`等

1.  `getSingleton` 调用 匿名函数 - 执行`createBean` 方法：该部分用于创建 `Bean` 或 `代理Bean`

### getSingleton
```java
public Object getSingleton(String beanName, ObjectFactory<?> singletonFactory) {
    Assert.notNull(beanName, "Bean name must not be null");
    synchronized (this.singletonObjects) {
        Object singletonObject = this.singletonObjects.get(beanName);
        if (singletonObject == null) {
            。。。
            //检测构造循环依赖
            beforeSingletonCreation(beanName);
            boolean newSingleton = false;
            。。。
            try {
                singletonObject = singletonFactory.getObject();
                newSingleton = true;
            }
            //...异常处理
            finally {
                if (recordSuppressedExceptions) {
                    this.suppressedExceptions = null;
                }
                afterSingletonCreation(beanName);
            }
            
            if (newSingleton) {
                //新建的Bean，从工厂缓存/提前曝光缓存转移到 单例缓存 
                addSingleton(beanName, singletonObject);
            }
        }
        return singletonObject;
    }
}
```
上面的步骤包含

1. 检测构造循环依赖

1. 创建 `Bean`，并将 `刚创建的Bean` （未初始化） 放入到 `工厂Bean缓存` 中

1. 最后将 `创建好并且初始化好的Bean`  转移到 `单例缓存`中




### 创建实例



具体实例创建见 `AbstractAutowireCapableBeanFactory.createBeanInstance` ，返回 `Bean` 的包装类 `BeanWrapper` ,一共有三种策略：

- **使用工厂方法创建**: `instantiateUsingFactoryMethod`

- **使用有参构造函数创建**: `autowireConstructor`

- **使用无参构造函数创建**: `instantiateBean`

#### 使用工厂方法创建

当 `RootBeanDefintion` 包含工厂方法时，会使用工厂方法创建对象。当使用工厂方法创建对象时，会先试用 `getBean` 获取工厂类，然后通过参数找到匹配的工厂方法去创建对象。

1. 首先来看调用：
    ```java
    //`AbstractAutowireCapableBeanFactory.createBeanInstance`
    // 当 Bean定义中包含工厂方法时调用工厂方法进行创建Bean
    if (mbd.getFactoryMethodName() != null) {
        return instantiateUsingFactoryMethod(beanName, mbd, args);
    }
    ```
    接下来看一下 `instantiateUsingFactoryMethod` 方法
    ```java
    //AbstractAutowireCapableBeanFactory.instantiateUsingFactoryMethod 
    protected BeanWrapper instantiateUsingFactoryMethod(
            String beanName, RootBeanDefinition mbd,
            @Nullable Object[] explicitArgs) {

        return new ConstructorResolver(this).
            instantiateUsingFactoryMethod(beanName, mbd, explicitArgs);
    }
    ```
    这里创建了 `ConstructorResolver` 类并调用了同名方法，我们来看一下该方法
    ```java
    //ConstructorResolver.instantiateUsingFactoryMethod

    public BeanWrapper instantiateUsingFactoryMethod(
        String beanName, RootBeanDefinition mbd, @Nullable Object[] explicitArgs) {

        .......
        String factoryBeanName = mbd.getFactoryBeanName();
        if (factoryBeanName != null) {
            ....    
            //从容器中获取工厂Bean
            factoryBean = this.beanFactory.getBean(factoryBeanName);
            .......
        }
        else {
            .....
            //如果工厂BeanName为空则获取Class对象
            factoryClass = mbd.getBeanClass();
            .....
        }
        。。。。。。
        //如果上面没有获取到工厂Bean的话，这里尝试使用反射创建
        。。。。。。
        //2. 获取构造参数
        ........

        //通过instantiate实例化对象，并放入包装类中
        bw.setBeanInstance(
            instantiate(beanName, mbd, factoryBean, factoryMethodToUse, argsToUse));
        return bw;
    }
    ```
1. 接下来看一下 `instantiate` 方法
    ```java
    private Object instantiate(String beanName, 
                            RootBeanDefinition mbd,
                            @Nullable Object factoryBean, 
                            Method factoryMethod, Object[] args) {

        try {
            if (System.getSecurityManager() != null) {
                return AccessController.doPrivileged((PrivilegedAction<Object>) () ->
                        //可以发现是通过调用 BeanFactory的instantiate方法
                        this.beanFactory.getInstantiationStrategy()
                            .instantiate(mbd,beanName, this.beanFactory, 
                                        factoryBean, factoryMethod, args),
                        this.beanFactory.getAccessControlContext());
            }
            else {
                //可以发现是通过调用 BeanFactory的instantiate方法
                return this.beanFactory.getInstantiationStrategy().instantiate(
                        mbd, beanName, this.beanFactory, factoryBean, factoryMethod, args);
            }
        }
        catch (Throwable ex) {
            throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                    "Bean instantiation via factory method failed", ex);
        }
    }
    ```
1. 最后使用 `SimpleInstantiationStrategy.instantiate` 反射构建 `Bean` 即可

    ```java
    @Override
    public Object instantiate(。。。。。。) {

        try {
            //反射前设置访问权限
            try {
                //方法，反射创建对象
                Object result = factoryMethod.invoke(factoryBean, args);
                //如果对象从创建为空，则返回 NullBean 的实例
                if (result == null) {
                    result = new NullBean();
                }
                return result;
            }
        }
        catch (各种异常捕捉)
    }
    ```

##### 工厂方法创建Bean总结

1. 得到完整的 `BeanDefinition` - `RootBeanDefinition`

1. 如果 `BeanDefinition` 中包含工厂方法,则 `getBean(factoryBean)` 

1. 如果有构造参数，与下方`有参构造创建bean` 一致

1. 通过反射调用 `method` 构造对象 （有可能抛出异常）

1. 如果构造出来的对象为 `null` ,返回 `NullBean` 的实例


#### 有参构造创建Bean

使用有参构造就比较复杂了，分为三步， 

- **第一**： 确定构造方法 

- **第二**： 获取构造方法参数：如果有参数并且使用 `@Lazy` 修饰，则创建代理对象并返回，否则向容器索要依赖

- **第三**： 传入上面获取的构造参数，执行构造方法，构造对象，返回。


当我们获取构造方法时有以下两种情况

1. **构造参数不使用 `@Lazy` (懒加载)修饰**：Spring 会先使用getBean(beanName) ,加载构造依赖

1. **构造参数使用 `@Lazy` (懒加载)修饰**：那么在创建对象时，并不直接创建真实对象，而是返回一个代理对象，当执行时使用反射创建真实对象，并执行这也是 `Spring`解决`构造依赖` 和 `原型依赖` 的方案

下面我们根据源码来分析这两种情况

1. 首先我们来看入口

    ```java
    //AbstractAutowireCapableBeanFactory.createBeanInstance
    protected BeanWrapper createBeanInstance(.....) {
        //确定构造方法
        Constructor<?>[] ctors = 
                determineConstructorsFromBeanPostProcessors(beanClass, beanName);
        if (ctors != null 
            || mbd.getResolvedAutowireMode() == AUTOWIRE_CONSTRUCTOR 
            || mbd.hasConstructorArgumentValues() 
            || !ObjectUtils.isEmpty(args)) {
            //使用自动注入构造对象
            return autowireConstructor(beanName, mbd, ctors, args);
        }
    }
    ```
    关于如何确定构造函数可以看这篇文章 <https://segmentfault.com/a/1190000022521744>

1. 进入 `autowireConstructor` 方法，发现调用了 `ConstructorResolver` 类的同名方法
    ```java
    protected BeanWrapper autowireConstructor(........) {
        return new ConstructorResolver(this)
            .autowireConstructor(beanName, mbd, ctors, explicitArgs);
    }
    ```

1. 继续看 `ConstructorResolver.autowireConstructor` 方法
    ```java
    public BeanWrapper autowireConstructor(String beanName, 
                                            RootBeanDefinition mbd,
                                            @Nullable Constructor<?>[] chosenCtors, 
                                            @Nullable Object[] explicitArgs) {

        BeanWrapperImpl bw = new BeanWrapperImpl();
        this.beanFactory.initBeanWrapper(bw);
        。。。。。。        

        ArgumentsHolder argsHolder;
        。。。。。。            
        //获取构造参数
        argsHolder = createArgumentArray(beanName, mbd, resolvedValues, 
                    bw, paramTypes, paramNames,
                    getUserDeclaredConstructor(candidate), 
                    autowiring, candidates.length == 1);
        。。。。。。                   
        //构建对象
        bw.setBeanInstance(instantiate(beanName, mbd, constructorToUse, argsToUse));
        return bw;
    }
    ```
1. 我们来看 `Spring` 是如何获取构造参数的: `createArgumentArray`
    ```java
    @Nullable
    protected Object resolveAutowiredArgument(M。。。。) {

        //这里调用了 beanFactory  的 resolveDependency 来获取依赖
        return this.beanFactory.resolveDependency(
                    new DependencyDescriptor(param, true),
                    beanName, autowiredBeanNames, typeConverter);
        。。。。。。。
        //异常处理
    }
    ```

1. 我们继续向下寻找: `DefaultListableBeanFactory.resolveDependency`
    ```java
    @Override
    @Nullable
    public Object resolveDependency(DependencyDescriptor descriptor, 
                                    @Nullable String requestingBeanName,
                                    @Nullable Set<String> autowiredBeanNames, 
                                    @Nullable TypeConverter typeConverter) 
                                        throws BeansException {

        descriptor.initParameterNameDiscovery(getParameterNameDiscoverer());
        if (Optional.class == descriptor.getDependencyType()) {
            return createOptionalDependency(descriptor, requestingBeanName);
        }
        else if (ObjectFactory.class == descriptor.getDependencyType() ||
                ObjectProvider.class == descriptor.getDependencyType()) {
            return new DependencyObjectProvider(descriptor, requestingBeanName);
        }
        else if (javaxInjectProviderClass == descriptor.getDependencyType()) {
            return new Jsr330Factory()
                .createDependencyProvider(descriptor, requestingBeanName);
        }
        //以上是兼容性处理，如 Jsr330 标准，以及Spring的其他处理方式
        else {
            //这里检测参数是否使用 @Lazy 修饰，是的话返回 代理对象，否则返回 null
            Object result = getAutowireCandidateResolver().
                    getLazyResolutionProxyIfNecessary(descriptor,
                                                    requestingBeanName);
            if (result == null) {
                //以getBean方式寻找依赖
                result = doResolveDependency(descriptor, 
                                requestingBeanName, 
                                autowiredBeanNames, 
                                typeConverter);
            }
            return result;
        }
    }
    ```
1. 我们来看一下上面返回代理和向容器索要参数是如何实现的。

    - **返回代理**：` getAutowireCandidateResolver().getLazyResolutionProxyIfNecessary`
        ```java
        //ContextAnnotationAutowireCandidateResolver
        public Object getLazyResolutionProxyIfNecessary(
            DependencyDescriptor descriptor, @Nullable String beanName) {
            //判断是否使用@Lazy 修饰，
            //    没有返回 null，
            //    有的话通过 buildLazyResolutionProxy 返回代理对象这个看名字也能看出来
            return (isLazy(descriptor) ?
                buildLazyResolutionProxy(descriptor, beanName) : null);
        }
        ```
        我们继续来看一下 `buildLazyResolutionProxy` 方法
        ```java
        protected Object buildLazyResolutionProxy(
                final DependencyDescriptor descriptor, 
                final @Nullable String beanName) {
          
            ProxyFactory pf = new ProxyFactory();
            pf.setTargetSource(ts);
            Class<?> dependencyType = descriptor.getDependencyType();
            if (dependencyType.isInterface()) {
                pf.addInterface(dependencyType);
            }
            //这里通过 BeanFactory 的加载器创建代理对象
            return pf.getProxy(beanFactory.getBeanClassLoader());
        }
        ```

    - **从容器中获取参数**： `doResolveDependency(descriptor, requestingBeanName, autowiredBeanNames, typeConverter);`

        ```java
        public Object doResolveDependency(.....){
            if (instanceCandidate instanceof Class) {
                //获取参数
                instanceCandidate = 
                    descriptor.resolveCandidate(autowiredBeanName, type, this);
            }
        }
        ```
        我们继续向下看来到 `DependencyDescriptor.resolveCandidate` 中
        ```java
        public Object resolveCandidate(.....){
            //通过BeanFactory.getBean() 方法返回参数对象
            return beanFactory.getBean(beanName);
        }
        ```
1. 最后通过 `SimpleInstantiationStrategy` 的 `instantiate` 方法 ,反射创建类即可

#### 无参构造创建Bean
我们发现这三种创建Bean的方式都会 ，最后都会走 `getInstantiationStrategy().instantiate(...)`,实现类: `SimpleInstantiationStrategy.instantiate`，接下来我们看一下源码。



首先我们来先看下无参构造的入口：
```java
//AbstractAutowireCapableBeanFactory.createBeanInstance
//使用无参构造创建对象
// No special handling: simply use no-arg constructor.
return instantiateBean(beanName, mbd);
```
来看 该方法
```java
protected BeanWrapper instantiateBean(String beanName, 
                                        RootBeanDefinition mbd) {
    try {
        Object beanInstance;
        if (System.getSecurityManager() != null) {
            beanInstance = AccessController.doPrivileged(
                    (PrivilegedAction<Object>) () -> 
                        //使用反射创建对象
                        getInstantiationStrategy().instantiate(mbd, beanName, this),
                        getAccessControlContext());
        }
        else {
            //使用反射创建对象
            beanInstance = getInstantiationStrategy().instantiate(mbd, beanName, this);
        }
        BeanWrapper bw = new BeanWrapperImpl(beanInstance);
        initBeanWrapper(bw);
        return bw;
    }
    ...处理异常
}
```
我们继续向下跟进 `getInstantiationStrategy().instantiate(mbd, beanName, this)`
```java
@Override
public Object instantiate(RootBeanDefinition bd, 
                        @Nullable String beanName, 
                        BeanFactory owner) {
    // Don't override the class with CGLIB if no overrides.
    if (!bd.hasMethodOverrides()) {
        Constructor<?> constructorToUse;
        .....
        //
        return BeanUtils.instantiateClass(constructorToUse);
    }
    else {
        //如果用户使用了， replace 和 lookup 的配置方法，用到了动态代理加入对应的逻辑
        // Must generate CGLIB subclass.
        return instantiateWithMethodInjection(bd, beanName, owner);
    }
}
```
如果用户使用了 replace 和 lookup 的配置方法，用到了动态代理加入对应的逻辑。如果没有的话，直接使用反射来创建实例。

关于replace 和 lookup <https://blog.csdn.net/lyc_liyanchao/article/details/82432993>


## 注入属性

实例创建完后开始进行属性的注入，如果涉及到外部依赖的实例，会自动检索并关联到该当前实例。

属性填充的入口: `AbstractAutowireCapableBeanFactory.populateBean`。
```java
protected void populateBean ... {
    PropertyValues pvs = mbd.getPropertyValues();
    
    ...
    // InstantiationAwareBeanPostProcessor 前处理
    for (BeanPostProcessor bp : getBeanPostProcessors()) {
        if (bp instanceof InstantiationAwareBeanPostProcessor) {
            InstantiationAwareBeanPostProcessor ibp = 
                (InstantiationAwareBeanPostProcessor) bp;
            if (!ibp.postProcessAfterInstantiation(bw.getWrappedInstance(), beanName)) {
                continueWithPropertyPopulation = false;
                break;
            }
        }
    }
    ...
    
    // 根据名称注入
    if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_NAME) {
        autowireByName(beanName, mbd, bw, newPvs);
    }

    // 根据类型注入
    if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_TYPE) {
        autowireByType(beanName, mbd, bw, newPvs);
    }

    ... 
    // InstantiationAwareBeanPostProcessor 后处理
    for (BeanPostProcessor bp : getBeanPostProcessors()) {
        if (bp instanceof InstantiationAwareBeanPostProcessor) {
            InstantiationAwareBeanPostProcessor ibp = 
                (InstantiationAwareBeanPostProcessor) bp;
            pvs = ibp.postProcessPropertyValues(pvs, 
                                                filteredPds, 
                                                bw.getWrappedInstance(), 
                                                beanName);
            if (pvs == null) {
                return;
            }
        }
    }
    
    ...
    
    // 应用属性值
    applyPropertyValues(beanName, mbd, bw, pvs);
}
```

可以看到主要的处理环节有：

- 应用 `InstantiationAwareBeanPostProcessor` 处理器，在属性注入前后进行处理。假设我们使用了 `@Autowire` 注解，这里会调用到 `AutowiredAnnotationBeanPostProcessor` 来对依赖的实例进行检索和注入的，它是 `InstantiationAwareBeanPostProcessor` 的子类。

- 根据名称或者类型进行自动注入，存储结果到 `PropertyValues` 中。

- 应用 `PropertyValues`，填充到 `BeanWrapper`。这里在检索依赖实例的引用的时候，会递归调用 `BeanFactory.getBean` 来获得。


## 初始化
初始化分为三步：

1. 触发 `Aware`

1.  触发 `BeanPostProcessor` 前置处理

1. 触发自定义 `init`

1.  触发 `BeanPostProcessor` 后置处理

我们来看下入口： `AbstractAutowireCapableBeanFactory.doCreateBean`
```java
exposedObject = initializeBean(beanName, exposedObject, mbd);
```
继续向下跟进
```java
protected Object initializeBean(。。。。) {
    ...
    //如果实现了aware 接口，触发aware
    invokeAwareMethods(beanName, bean);
    

    Object wrappedBean = bean;
    if (mbd == null || !mbd.isSynthetic()) {
        //应用beanPostProcessor
        wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);
    }

    。。。。
    //调用init方法，如果定义了
    invokeInitMethods(beanName, wrappedBean, mbd);
    。。。
    return wrappedBean;
}
```

### 触发Aware


```java
private void invokeAwareMethods(final String beanName, final Object bean) {
    if (bean instanceof Aware) {
        if (bean instanceof BeanNameAware) {
            ((BeanNameAware) bean).setBeanName(beanName);
        }
        if (bean instanceof BeanClassLoaderAware) {
            ((BeanClassLoaderAware) bean).setBeanClassLoader(getBeanClassLoader());
        }
        if (bean instanceof BeanFactoryAware) {
            ((BeanFactoryAware) bean)
                .setBeanFactory(AbstractAutowireCapableBeanFactory.this);
        }
    }
}
```


### 触发BeanPostProcessor
在 `Bean` 的初始化前或者初始化后，我们如果需要进行一些增强操作怎么办？

这些增强操作比如打日志、做校验、属性修改、耗时检测等等。 `Spring` 框架提供了 `BeanPostProcessor` 来达成这个目标。比如我们使用注解 `@Autowire` 来声明依赖，就是使用 `AutowiredAnnotationBeanPostProcessor` 来实现依赖的查询和注入的。接口定义如下：
```java
public interface BeanPostProcessor {

    // 初始化前调用
    Object postProcessBeforeInitialization(Object bean, String beanName);

    // 初始化后调用
    Object postProcessAfterInitialization(Object bean, String beanName);
}
```
**实现该接口的 Bean 都会被 Spring 注册到 beanPostProcessors 中**，见 `AbstractBeanFactory `:
```java
/** BeanPostProcessors to apply in createBean */
private final List<BeanPostProcessor> beanPostProcessors = 
    new ArrayList<BeanPostProcessor>();
```
在 `Bean` 实例化前后，`Spring` 会去调用我们已经注册的 `beanPostProcessors` 把处理器都执行一遍。

```java
public abstract class AbstractAutowireCapableBeanFactory ... {
        
    ...
    
    @Override
    public Object applyBeanPostProcessorsBeforeInitialization ... {

        Object result = existingBean;
        for (BeanPostProcessor beanProcessor : getBeanPostProcessors()) {
            result = beanProcessor.postProcessBeforeInitialization(result, beanName);
            if (result == null) {
                return result;
            }
        }
        return result;
    }

    @Override
    public Object applyBeanPostProcessorsAfterInitialization ... {

        Object result = existingBean;
        for (BeanPostProcessor beanProcessor : getBeanPostProcessors()) {
            result = beanProcessor.postProcessAfterInitialization(result, beanName);
            if (result == null) {
                return result;
            }
        }
        return result;
    }
    
    ...
}
```


###  触发自定义 init

自定义初始化有两种方式可以选择：

- 实现 InitializingBean。在属性设置完成后再加入自己的初始化逻辑。

- 定义 init 方法。自定义的初始化逻辑。

见 `AbstractAutowireCapableBeanFactory.invokeInitMethods` ：

```java
protected void invokeInitMethods ... {

    boolean isInitializingBean = (bean instanceof InitializingBean);
    if (isInitializingBean && 
        (mbd == null || !mbd.isExternallyManagedInitMethod("afterPropertiesSet"))) {
        ...
        
        ((InitializingBean) bean).afterPropertiesSet();
        ...
    }

    if (mbd != null) {
        String initMethodName = mbd.getInitMethodName();
        if (initMethodName != null && !(isInitializingBean 
            && "afterPropertiesSet".equals(initMethodName)) &&
                !mbd.isExternallyManagedInitMethod(initMethodName)) {
            invokeCustomInitMethod(beanName, bean, mbd);
        }
    }
}
```
## 类型转换

`Bean` 已经创建完毕，属性也填充好了，初始化也完成了。

在返回给调用者之前，还留有一个机会对 `Bean` 实例进行类型的转换。见 `AbstractBeanFactory.doGetBean` ：
```java
protected <T> T doGetBean ... {
    ...
    if (requiredType != null && bean != null && !requiredType.isInstance(bean)) {
        ...
        return getTypeConverter().convertIfNecessary(bean, requiredType);
        ...
    }
    return (T) bean;
}

```

# 总结

抛开一些细节处理和扩展功能，一个 Bean 的创建过程无非是：

获取完整定义 -> 实例化 -> 依赖注入 -> 初始化 -> 类型转换。


其实Spring也处理了原型模式的Bean，这些放在循环依赖中叙述。