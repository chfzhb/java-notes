# 注解

注解在编译时自动继承了 `java.lang.Annotation` , 在编译中，就如同所有类都是 `Object` 的子类一样

# 注解的功能

- 作为特定的标记用于告诉编译器一些信息，如 `@override` 是告诉编译器重写了基类方法。

- 编译时动态处理，如自动生成代码

- 运行时动态处理，作为额外信息的载体，如获取注解信息

# 注解分类

注解分为标准注解、元注解、自定义注解

## 标准注解

- Override - 重载

- Deprecated - 不建议使用

- SuppressWarnings - 忽略指定的警告

## 元注解

用来定义注解的注解

- @Target - 注解的作用目标(ElementType)

    - packagers、types(类、接口、枚举、Annotation)

    - 类型成员(方法、构造函数、成员变量、枚举值)

    - 方法参数和本地变量（循环变量、catch参数）

- @Retention - 注解的生命周期(RetentionPolicy)

    - RetentionPolicy.SOURCE - 在源文件中保留，在class文件中被去除掉

    - RetentionPolicy.CLASS - 在源文件和Class文件中中保留

    - RetentionPolicy.RUNTIME - 该类能够在运行时获得该注解的信息

- @Inherited - 是否允许子类继承该注解

- @Documented - 注解是否应该被包含在JavaDoc文档中

## 自定义注解

```java
public @interface 注解名{
    修饰符 返回值 属性名 默认值;

    修饰符 返回值 属性名 默认值;
}
```


# 反射与注解

与反射相关的工具类均直接或间接实现了 `AnnotatedElement` 接口.

- `Class` 类实现了 `AnnotatedElement` 接口.

- `Constractor` 继承 `Executable` , `Executable` 继承 `AccessibleObject` ，`AccessibleObject` 实现了 `AnnotatedElement` 接口.

 - `Field` 继承了 `AccessibleObject` 接口

 - `Method` 继承了 `AccessibleObject` 接口

## AnnotatedElement常用方法


- 获取所有注解 - 包含继承的注解
    `Annotation[] getAnnotations();`

- 获取特定注解 - 包含继承的注解
    `<T extends Annotation> T getAnnotation(Class<T> annotationClass);`
- 是否存在注解
    `boolean isAnnotationPresent(Class<? extends Annotation> annotationClass)`

## 注解获取属性值的底层实现

- JVM会为注解生成代理对象

使用JVM参数`-Djdk.proxy.ProxyGenerator.saveGeneratedFiles=true` 就可以在`main` 函数运行时，生成代理类的class，可以通过idea查看

1. 代码
    注解类
    ```java
    //类注解
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface CourseInfoAnnotation {


        String courseName() default "";

        String courseTag() default "";

        String courseProfile() default "";

        int courseIndex() default 303;
    }

    //字段注解
    @Target(ElementType.FIELD)//用于字段
    @Retention(RetentionPolicy.RUNTIME)//记录在类文件中，可以通过反射获取到
    public @interface PersoninfoAnnotation {

        String name() default "";

        int age() default 19;

        String gender() default "男";

        String[] language();
    }

    //方法注解
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface CourseInfoAnnotation {


        String courseName() default "";

        String courseTag() default "";

        String courseProfile() default "";

        int courseIndex() default 303;
    }
    ```
    使用注解
    ```java
    @CourseInfoAnnotation(
        courseName = "剑指Java面试",
        courseTag = "面试",
        courseProfile = "全面")
    public class ImoocCourse {

        @PersoninfoAnnotation(name = "LL", age = 27, language = {"java", "C#", "JS"})
        private String author;


        public ImoocCourse() {
        }


        @CourseInfoAnnotation(
                courseName = "商铺",
                courseTag = "实战",
                courseProfile = "真实开发",
                courseIndex = 144)
        public void getCourseInfo() {

        }
    }
    ```
    测试
    ```java
    public class AnnotationParser {

        /**
        * 解析类的注解
        */
        public static void parseTypeAnnotation() throws ClassNotFoundException {
            Class clazz = Class.forName("demo.annotation.ImoocCourse");

            //这里获取是类的注解
            Annotation[] annotations = clazz.getAnnotations();

            for (Annotation annotation : annotations) {
                if (annotation instanceof CourseInfoAnnotation) {
                    CourseInfoAnnotation courseInfoAnnotation = (CourseInfoAnnotation) annotation;
                    String courseName = courseInfoAnnotation.courseName();
                    System.out.println(courseName);
                    System.out.println(courseInfoAnnotation.courseTag());
                    System.out.println(courseInfoAnnotation.courseProfile());
                    System.out.println(courseInfoAnnotation.courseIndex());
                }


            }
        }

        /**
        * 解析成员变量的注解
        */
        public static void parseFieldeAnnotation() throws ClassNotFoundException {
            Class clazz = Class.forName("demo.annotation.ImoocCourse");

            for (Field field : clazz.getDeclaredFields()) {
                boolean present = field.isAnnotationPresent(PersoninfoAnnotation.class);
                if (present) {
                    PersoninfoAnnotation annotation = field.getAnnotation(PersoninfoAnnotation.class);
                    System.out.println(annotation.name());
                    System.out.println(annotation.age());
                    System.out.println(annotation.gender());
                    for (String s : annotation.language()) {
                        System.out.println(s);
                    }

                }
            }
        }

        /**
        * 解析方法的注解
        */
        public static void parseMethodeAnnotation() throws ClassNotFoundException {
            Class clazz = Class.forName("demo.annotation.ImoocCourse");

            for (Method method : clazz.getMethods()) {
                boolean annotationPresent = method.isAnnotationPresent(CourseInfoAnnotation.class);
                if (annotationPresent) {
                    CourseInfoAnnotation annotation = method.getAnnotation(CourseInfoAnnotation.class);
                    String courseName = annotation.courseName();
                    System.out.println(courseName);
                    System.out.println(annotation.courseTag());
                    System.out.println(annotation.courseProfile());
                    System.out.println(annotation.courseIndex());
                }
            }
        }

        public static void main(String[] args) throws ClassNotFoundException {
            //        parseTypeAnnotation();
            System.out.println("------" );
            parseFieldeAnnotation();
            System.out.println("------" );
            //        parseMethodeAnnotation();
        }
    }
    ```
    



1. 设置参数

    <img src="../../../../java-notes/img/jvm-paramproxy.png">

1. 运行main方法，生成代理类

    <img src="../../../../java-notes/img/jvm-paramproxy-class.png">

    生成的代码
    ```java
    package com.sun.proxy;

    import demo.annotation.PersoninfoAnnotation;
    import java.lang.reflect.InvocationHandler;
    import java.lang.reflect.Method;
    import java.lang.reflect.Proxy;
    import java.lang.reflect.UndeclaredThrowableException;

    public final class $Proxy1 extends Proxy implements PersoninfoAnnotation {
        private static Method m1;
        private static Method m4;
        private static Method m3;
        private static Method m2;
        private static Method m5;
        private static Method m6;
        private static Method m7;
        private static Method m0;

        public $Proxy1(InvocationHandler var1) throws  {
            super(var1);
        }

        public final boolean equals(Object var1) throws  {
            try {
                return (Boolean)super.h.invoke(this, m1, new Object[]{var1});
            } catch (RuntimeException | Error var3) {
                throw var3;
            } catch (Throwable var4) {
                throw new UndeclaredThrowableException(var4);
            }
        }

        public final String[] language() throws  {
            try {
                return (String[])super.h.invoke(this, m4, (Object[])null);
            } catch (RuntimeException | Error var2) {
                throw var2;
            } catch (Throwable var3) {
                throw new UndeclaredThrowableException(var3);
            }
        }

        public final String name() throws  {
            try {
                return (String)super.h.invoke(this, m3, (Object[])null);
            } catch (RuntimeException | Error var2) {
                throw var2;
            } catch (Throwable var3) {
                throw new UndeclaredThrowableException(var3);
            }
        }

        public final String toString() throws  {
            try {
                return (String)super.h.invoke(this, m2, (Object[])null);
            } catch (RuntimeException | Error var2) {
                throw var2;
            } catch (Throwable var3) {
                throw new UndeclaredThrowableException(var3);
            }
        }

        public final String gender() throws  {
            try {
                return (String)super.h.invoke(this, m5, (Object[])null);
            } catch (RuntimeException | Error var2) {
                throw var2;
            } catch (Throwable var3) {
                throw new UndeclaredThrowableException(var3);
            }
        }

        public final int age() throws  {
            try {
                return (Integer)super.h.invoke(this, m6, (Object[])null);
            } catch (RuntimeException | Error var2) {
                throw var2;
            } catch (Throwable var3) {
                throw new UndeclaredThrowableException(var3);
            }
        }

        public final Class annotationType() throws  {
            try {
                return (Class)super.h.invoke(this, m7, (Object[])null);
            } catch (RuntimeException | Error var2) {
                throw var2;
            } catch (Throwable var3) {
                throw new UndeclaredThrowableException(var3);
            }
        }

        public final int hashCode() throws  {
            try {
                return (Integer)super.h.invoke(this, m0, (Object[])null);
            } catch (RuntimeException | Error var2) {
                throw var2;
            } catch (Throwable var3) {
                throw new UndeclaredThrowableException(var3);
            }
        }

        static {
            try {
                m1 = Class.forName("java.lang.Object").getMethod("equals", Class.forName("java.lang.Object"));
                m4 = Class.forName("demo.annotation.PersoninfoAnnotation").getMethod("language");
                m3 = Class.forName("demo.annotation.PersoninfoAnnotation").getMethod("name");
                m2 = Class.forName("java.lang.Object").getMethod("toString");
                m5 = Class.forName("demo.annotation.PersoninfoAnnotation").getMethod("gender");
                m6 = Class.forName("demo.annotation.PersoninfoAnnotation").getMethod("age");
                m7 = Class.forName("demo.annotation.PersoninfoAnnotation").getMethod("annotationType");
                m0 = Class.forName("java.lang.Object").getMethod("hashCode");
            } catch (NoSuchMethodException var2) {
                throw new NoSuchMethodError(var2.getMessage());
            } catch (ClassNotFoundException var3) {
                throw new NoClassDefFoundError(var3.getMessage());
            }
        }
    }

    ```

    从类签名来看注解其实也是一个接口。

    代理类定义了`m1 - m0` 八个 `Method` 类型的成员，在类末尾的`static`代码块中分别对应着 Object类 的一些方法和一些属性调用方法。这些属性调用方法分别在类中定义

    如`language`方法
    ```java
    public final String[] language() throws  {
        try {
            return (String[])super.h.invoke(this, m4, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }
    ```
    我们可以看到 `language` 方法中，使用 `super`调用`h`变量的 `invok` 方法返回属性值。

    关于`h`变量我们可以看构造函数
    ```java
    public $Proxy1(InvocationHandler var1) throws  {
        super(var1);
    }
    ```
    继续使用`ctrl` + 鼠标左键 跟踪 `super` 方法。
    
    我们可以看到在 `Proxy` 类中方法的定义
    ```java
    protected Proxy(InvocationHandler h) {
        Objects.requireNonNull(h);
        this.h = h;
    }
    ```
    所以 `h` 就是代理类构造方法传入的 `InvocationHandler var1`。

    我们来继续看一下 `language` 方法中的`invoke`方法
    ```java
    //language 方法中的invoke调用
    return (String[])super.h.invoke(this, m4, (Object[])null);
    ```
    我们已知 `h` 是 `InvocationHandler` 类型，所以我们直接跟踪 `invoke` 方法的定义
    ```java
    public interface InvocationHandler {
        Object invoke(Object proxy, Method method, Object[] args) 
            throws Throwable;
    }
    ```
    我们可以看到，这只是一个接口方法的定义，并且该方法同反射中的`invoke` 并非反射中的 `invoke`。

    我们继续来看下实现类 `AnnotationInvocationHandler` 。实现类一般是`xxx接口名`，xxx一般是,但我们也不是漫无目的的瞎猜，我们可以使用`JVM`参数 来跟踪类加载
    > -XX:+TraceClassLoading

    在加上上面的参数后，我们可以在控制台搜索使用的接口， 如 `InvocationHandler`，我们可以看到，在加载完接口后 立马加载了 `AnnotationInvocationHandler` 实现类`。
    ```java
    [1.598s][info   ][class,load] java.lang.reflect.InvocationHandler source: jrt:/java.base
    [1.598s][info   ][class,load] sun.reflect.annotation.AnnotationInvocationHandler source: jrt:/java.base
    ```
    接下来我们可以在 `AnnotationInvocationHandler` 的 `invoke` 方法中设置断点，来看看是否执行到了该类。
    
    <img src="../../../../java-notes/img/annotationInvocationHandler-invoke-断点.png">

    到这里我们可以确认 `AnnotationInvocationHandler`  就是注解代理类的调用类。

    接下来我们可以看下 `invoke` 的实现

    ```java
    //当前注解的类型
    private final Class<? extends Annotation> type;
    //注解的成员
    private final Map<String, Object> memberValues;

    public Object invoke(Object proxy, Method method, Object[] args) {
        String member = method.getName();
        int parameterCount = method.getParameterCount();

        //如果是Object的注解的基础方法，则按照原方式处理
        // Handle Object and Annotation methods
        if (parameterCount == 1 && member == "equals" &&
                method.getParameterTypes()[0] == Object.class) {
            return equalsImpl(proxy, args[0]);
        }
        if (parameterCount != 0) {
            throw new AssertionError("Too many parameters for an annotation method");
        }

        if (member == "toString") {
            return toStringImpl();
        } else if (member == "hashCode") {
            return hashCodeImpl();
        } else if (member == "annotationType") {
            return type;
        }

        //处理自定义成员属性
        // Handle annotation member accessors
        Object result = memberValues.get(member);//获取属性值
        //为null抛出
        if (result == null)
            throw new IncompleteAnnotationException(type, member);
        //
        if (result instanceof ExceptionProxy)
            throw ((ExceptionProxy) result).generateException();
        //如果是数组则克隆一份返回
        if (result.getClass().isArray() && Array.getLength(result) != 0)
            result = cloneArray(result);

        return result;
    }
    ```

## 注解的工作原理

1. 通过键值对的形式为注解属性赋值
    ```java
    @CourseInfoAnnotation(
        courseName = "剑指Java面试",
        courseTag = "面试",
        courseProfile = "全面")
    ```

2. 编译器根据注解的使用范围，将注解信息写入元素属性表。

    我们知道从 `Java` 源码到 `Class` 字节码，是由编译器完成的，编译器会对`Java`源码进行解析，并生成 `Class` 文件，注解也是由编译器在编译时进行处理。编译器会处理注解符号，并将其附加到 `Class` 结构中，根据 `JVM` 规范`Class` 文件是严格有序的格式，唯一可以附加信息到 `Class` 结构中的方式，即使保存到 `Class` 中的 属性中。

    注解使用范围
    ```java
    @Target({ElementType.TYPE, ElementType.METHOD})
    ```
3. 运行时 `JVM` 将 `RUNTIME` 的所有注解属性取出最终存到`map`中

4. 紧接着 `JVM` 创建 `AnnotationInvocationhandler` 实例并传入前面的 `map` 

5. 最后 `JVM` 使用 `JDK` 动态代理为注解生成代理类，并初始化 `AnnotationInvocationhandler` 处理器

6. 调用时通过调用 `AnnotationInvocationhandler.invoke` 方法，通过传入的属性名返回注解对应的属性值

## 注解对自研框架的意义

1. 使用注解标记需要工厂管理的实例，并依据注解属性做精细控制

    ```java
    @WebServlet("/")
    @Slf4j
    public class DispatchServlet extends HttpServlet {
    }
    ```
    这样能够使用通用的处理方式处理某一类带有相同注解的对象，这也遵循了开闭原则
