
# 架构设计

![20210225134904](http://qiniu.liulei.life/20210225134904.png)

我们把 mybatis 的功能分为三层：

1. **API接口层：** 提供给外部使用的接口API，开发人员通过这些本地API，来操作数据库。接口层被调用后，会调用数据处理层来完成具体数据的处理

    mybatis 和 数据库的交互有两种方式

    1. 使用 mybatis 提供的 API

    1. 使用 mapper 代理的方式

1. **数据处理层**：负责具体的sql 查找、sql解析、sql执行、和结果映射等，它的主要目的是根据请求完成一次数据库操作。

1. **基础支撑层**：负责最基础的功能支撑，包括连接管理、事务管理、配置加载和缓存处理。这些都是公用的东西，将它们抽取出来，作为基础的组件，为上层的数据处理做支撑。


## 主要组件

<table>
    <tr>
        <th>组件</th>
        <th>描述</th>
    </tr>
    <tr>
        <td>SqlSession</td>
        <td>作为Mybatis工作的主要顶层API，表示和数据库交互的会话，完成必要数据库增删改查功能</td>
    </tr>
    <tr>
        <td>Exetor</td>
        <td>Mybatis执行器，是Mybatis调度的核心，负责Sql语句的生成和查询缓存的维护</td>
    </tr>
    <tr>
        <td>StatementHandler</td>
        <td>封装了JDBC Statement，负责对JDBC Statement的操作，如设置参数，将Statement结果转换成list集合</td>
    </tr>
    <tr>
        <td>ParameterHandler</td>
        <td>负责把用户传递的参数转换成JDBC Statement所需要的参数</td>
    </tr>
    <tr>
        <td>ResultSetHandler</td>
        <td>负责将JDBC返回的ResultSet结果集对象转换成List类型的集合</td>
    </tr>
    <tr>
        <td>TypeHandler</td>
        <td>负责Java数据类型和jdbc数据类型的转换</td>
    </tr>
    <tr>
        <td>MappedStatement</td>
        <td>MappedStatement维护了一条select、update、delete、insert节点的封装</td>
    </tr>
    <tr>
        <td>SqlSource</td>
        <td>负责用户传递的parameterObject，动态的生成sql语句，蒋信息封装到BoundSql对象中，并返回</td>
    </tr>
    <tr>
        <td>BondSql</td>
        <td>表示动态生成的sql语句以及相应的参数信息</td>
    </tr>
</table>

![20210225145419](http://qiniu.liulei.life/20210225145419.png)

## 总体流程

1. 加载配置并初始化

    **触发条件**：加载配置文件

    1. conf.xml、mapper.xml

    1. java 注解

    mybatis 会将配置文件中的内容解析封装到 Configuration ，将sql 的配置信息加载成为一个 mappedStatement 对象，储存在内存之中

1. 接收调用请求

    触发条件： 调用Mybatis提供的API

    传入参数： sql的ID、传入参数对象

    处理过程：将请求传递给下层的请求处理层进行处理

    1. 根据sql的ID，查找对应的 mappedStetement 对象

    1. 根据传入参数对象，解析 MappedStatement 对象，得到最终要执行的sql和执行传入参数

    1. 获取数据库连接，根据得到的最终sql和执行参数，传入到数据库执行，并得到执行结果

    1. 根据mappedStatement对象中的结果映射配置，对得到的执行结果进行转换处理，并得到最终的处理结果

    1. 释放连接资源

1. 返回处理结果

# Mybatis源码分析

## 传统方式

初始化

```java
//1. 读取配置文件，读取成字节输入流，但是现在还没有解析
InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");

//2. 解析配置文件，封装成 configuration对象。
//   创建DefaultSqlSessionFactory对象
SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder()
        .build(resourceAsStream);
```

进入源码分析

```java
//1. 我们最初调用的 build
public SqlSessionFactory build(InputStream inputStream) {
    //调用了重载方法
    return build(inputStream, null, null);
}

//2. 调用的重载方法
public SqlSessionFactory build(InputStream inputStream, 
                                String environment, 
                                Properties properties) {
    try {
        // XMLConfigBuilder 是专门解析 mybatis 配置文件的类
        XMLConfigBuilder parser = new XMLConfigBuilder(inputStream, environment, properties);
        // parser.parse()返回 configuration 对象
        return build(parser.parse());
    } catch (Exception e) {
        throw ExceptionFactory.wrapException("Error building SqlSession.", e);
    } 
}
```

Mybatis 在初始化的时候，会将 Mybatis 的配置信息全部加载到内存中，使用 `org.apache.ibatis.session.Configuration` 实例来维护

下面进入对配置文件的解析部分。

### 配置文件解析

首先对Configuration对象进行介绍:

Configuration 对象结构和XML配置文件的对象几乎相同。

首先回顾一下XML 中的 配置标签有哪些

- properties：属性

- settings：设置

- typeAliases：类型别名

- typeHandles：类型处理器

- objectFactory：对象工厂

- mappers：映射器等

配置文件解析的本质就是创建 Configuration 对象，将解析的xml数据封装到 configuration 内部属性中。

我们来看一下 XML 是如何解析成 Configuration 的

```java
//将 XML 解析成 Configuration 对象
public Configuration parse() {
    // 如果已经解析，抛出异常
    if (parsed) {
        throw new BuilderException("Each MapperConfigParser can only be used once.");
    }
    // 标记已解析
    parsed = true;
    //解析 XML 中的 configuration 节点
    parseConfiguration(parser.evalNode("/configuration"));
    return configuration;
}

private void parseConfiguration(XNode root) {
    try {
        //解析 <properties 标签
        propertiesElement(root.evalNode("properties")); //issue #117 read properties first

        // 解析 <typeAliases>
        typeAliasesElement(root.evalNode("typeAliases"));

        // 解析 <plugins>
        pluginElement(root.evalNode("plugins"));
        
        // 解析 <objectFactory>
        objectFactoryElement(root.evalNode("objectFactory"));

        // 解析 <objectWrapperFactory>
        objectWrapperFactoryElement(root.evalNode("objectWrapperFactory"));

        // 解析 <settings>
        settingsElement(root.evalNode("settings"));

        // 解析 <environments>
        environmentsElement(root.evalNode("environments"));

        // 解析 <databaseIdProvider>
        databaseIdProviderElement(root.evalNode("databaseIdProvider"));

        // 解析 <typeHandlers>
        typeHandlerElement(root.evalNode("typeHandlers"));

        // 解析 <mappers>
        mapperElement(root.evalNode("mappers"));
    } catch (Exception e) {
        throw new BuilderException("Error parsing SQL Mapper Configuration. Cause: " + e, e);
    }
}
```

上面只是解析了我们总的配置文件，还没有解析我们的 xxxmapper.xml 文件.MappedStatement 对象就是我们解析 mapper.xml的 关键。

MappedStatement 与 mapper配置文件中的 select/update/delete/insert 节点相对应。 MappedStatement 主要作用就是描述一条SQL语句。

在上面的初始化过程中，会解析 mybatis-config.xml 文件中的 mappers 标签，来引入 mapper.xml 文件或者配置mapper接口的目录。

```xml
<select id="getUser" resultType="user">
    select * from user where id = #{id}
</select>
```

这样一个select 标签，在初始化配置文件时，会被解析封装成一个 mappedStatement对象，然后储存在 Configuration 对象的 mappedStatements 属性中，该属性是一个 Map ，key = 全类名+方法名，value = 对应的mappedStatement对象

在configuration对应的属性为
```java
Map<String, MappedStatement> mappedStatements = new StrictMap<MappedStatement>("Mapped Statements collection");
```
在 XMLConfigBuilder 中的处理

```java
private void parseConfiguration(XNode root) {
    try {
    // 省略其他标签的处理
    mapperElement(root.evalNode("mappers"));
    } catch (Exception e) {
        throw new BuilderException("Error parsing SQL Mapper Configuration.Cause: " + e, e);
    }
}
```
到此 XML 配置文件的解析就结束了，回到步骤2 调用的，重载build方法

```java
// 5. 调用的重载方法
public SqlSessionFactory build(Configuration config) {
    // 创建了 DefaultSqlSessionFactory 对象 ，传入Configuration 对象
    return new DefaultSqlSessionFactory(config);
}
```

### 执行SQL流程

首先看一下sqlSession：

SqlSession 是一个接口，它有两个实现类：DefaultSqlSession（默认）和SqlSessionManager（弃用）

SqlSession 是 Mybatis 中用于和数据库交互的顶层类，通常它与ThreadLocal绑定，一个会话对应一个SqlSession，并且在使用完成后需要close。
```java
public class DefaultSqlSession implements SqlSession {
    private final Configuration configuration;
    private final Executor executor;
}
```
SqlSession 中最重要的两个参数，configuration - 配置，Executor - 执行器

我们继续来看一下 **Executorғ**：

Excecutor 也是一个接口，他有三个实现类：

- BatchExecutor：批量执行

- ReuseExecutor：重用预处理语句 prepared statements

- SimpleExecutor：默认执行器

初始化完成后，我们就要执行SQL了

```java
SqlSession sqlSession = sessionFactory.openSession();
List<Object> objectList = sqlSession.selectList("namespace.id");
```

获得 sqlSession 
```java
public SqlSession openSession() {
    // 6. configuration.getDefaultExecutorType() 传递的是 SimpleExecutor
    return openSessionFromDataSource(configuration.getDefaultExecutorType(), null, false);
}

//7. 进入openSessionFromDateSource
//    ExecutorType 为 Executor 的类型，TransactionIsolationLevel为事务隔离级别，autoCommit是否开启事务
//    openSession 的多个重载方法可以指定获得的 seqSession 的 Executor类型和事务的处理
private SqlSession openSessionFromDataSource(ExecutorType execType,TransactionIsolationLevel level, boolean autoCommit) {
    Transaction tx = null;
    try {
        final Environment environment = configuration.getEnvironment();
        final TransactionFactory transactionFactory =
        getTransactionFactoryFromEnvironment(environment);
        tx = transactionFactory.newTransaction(environment.getDataSource(),
        level, autoCommit);
        // 根据参数创建指定类型的 Executor
        final Executor executor = configuration.newExecutor(tx, execType);
        // 返回的是DefaultSqlSession
        return new DefaultSqlSession(configuration, executor, autoCommit);
    } catch (Exception e) {
        closeTransaction(tx); // may have fetched a connection so lets call
        close()
    }
}
```

执行sqlSession中的api
```java
//8. 进入selectList 方法，有多个重载
public <E> List<E> selectList(String statement) {
    return this.<E>selectList(statement, null);
}

public <E> List<E> selectList(String statement, Object parameter) {
    return this.<E>selectList(statement, parameter, RowBounds.DEFAULT);
}

public <E> List<E> selectList(String statement, Object parameter, RowBounds rowBounds) {
    try {
        //根据传入的全限定名 + 方法名 从集合中取出 mappedStatement 对象
        MappedStatement ms = configuration.getMappedStatement(statement);
        //调用 Executor 中的方法处理
        //RowBounds用来逻辑分页
        //wrapCollection(parameter) 用来装饰集合或者数组参数
        List<E> result = executor.<E>query(ms, wrapCollection(parameter), rowBounds, Executor.NO_RESULT_HANDLER);
        return result;
    } catch (Exception e) {
        throw ExceptionFactory.wrapException("Error querying database.  Cause: " + e, e);
    } finally {
        ErrorContext.instance().reset();
    }
}
```

继续源码中的步骤，进入 executor.query（）

```java
public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException {
    //根据传入的参数动态获得sql语句，最后返回 boundSql对象表示
    BoundSql boundSql = ms.getBoundSql(parameter);
    //为本次的查询创建缓存的key
    CacheKey key = createCacheKey(ms, parameter, rowBounds, boundSql);
    return query(ms, parameter, rowBounds, resultHandler, key, boundSql);
}

//进入query的重载
public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
    ErrorContext.instance().resource(ms.getResource()).activity("executing a query").object(ms.getId());
    if (closed) throw new ExecutorException("Executor was closed.");
    if (queryStack == 0 && ms.isFlushCacheRequired()) {
      clearLocalCache();
    }
    List<E> list;
    try {
      queryStack++;
      list = resultHandler == null ? (List<E>) localCache.getObject(key) : null;
      if (list != null) {
        handleLocallyCachedOutputParameters(ms, key, parameter, boundSql);
      } else {
        //如果缓存中没有本次查找的值，那么从数据库中查询
        list = queryFromDatabase(ms, parameter, rowBounds, resultHandler, key, boundSql);
      }
    } finally {
      queryStack--;
    }
    if (queryStack == 0) {
      for (DeferredLoad deferredLoad : deferredLoads) {
        deferredLoad.load();
      }
      if (configuration.getLocalCacheScope() == LocalCacheScope.STATEMENT) {
        clearLocalCache(); // issue #482
      }
    }
    return list;
}

//从数据库查询
private <E> List<E> queryFromDatabase(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
    List<E> list;
    localCache.putObject(key, EXECUTION_PLACEHOLDER);
    try {
      //查询的方法
      list = doQuery(ms, parameter, rowBounds, resultHandler, boundSql);
    } finally {
      localCache.removeObject(key);
    }
    //将查询结果放入缓存
    localCache.putObject(key, list);
    if (ms.getStatementType() == StatementType.CALLABLE) {
      localOutputParameterCache.putObject(key, parameter);
    }
    return list;
}


//simpleExecutor 中实现父类的doquery方法
public <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
    Statement stmt = null;
    try {
        
        Configuration configuration = ms.getConfiguration();
        //传入参数创建 StatementHandler 对象来执行查询
        StatementHandler handler = configuration.newStatementHandler(this, ms, parameter, rowBounds, resultHandler, boundSql);
        //创建jdbc中的 statement 对象
        stmt = prepareStatement(handler, ms.getStatementLog());
        // StatementHandler 进行处理
        return handler.<E>query(stmt, resultHandler);
    } finally {
        closeStatement(stmt);
    }
}



// 创建 statement 的方法
private Statement prepareStatement(StatementHandler handler, Log statementLog)
throws SQLException {
    Statement stmt;
    // getConnection 经过重重调用最后会调用openConnection 方法，从连接池中获得连接
    Connection connection = getConnection(statementLog);
    stmt = handler.prepare(connection, transaction.getTimeout());
    handler.parameterize(stmt);
    return stmt;
}

// 从连接池获得连接的方法
protected void openConnection() throws SQLException {
    if (log.isDebugEnabled()) {
    log.debug("Opening JDBC Connection");
    }
    // 从连接池获得连接
    connection = dataSource.getConnection();
    if (level != null) {
    connection.setTransactionIsolation(level.getLevel());
}
```
上述的 Executor.query() 方法几经转折，最后会创建一个 Statementhandler对象，然后将必要的参数传递给它，最后使用该对象完成对数据库的查询返回List结果集

从上面的代码中我们可以看出 executor 的功能和作用是：

1. 根据传递的参数，完成sql语句的动态解析，生成 BoundSql 对象，供StatementHandler使用；

1. 为查询创建缓存，以提高性能

1. 创建 JDBC 的 statement 连接对象，传递给 statementHandler 对象执行，并返回list 查询结果


`StatementHandler` 对象主要完成两个工作

1. 对于JDBC的PreparStatement对象，在创建的过程中，我们使用的是SQL语句字符串包含若干个？占位符，在后面会通过statementHandler.parameterize(statement)方法对Statement设值

1. StatementHandler 通过 List query(Statement statement, ResultHandler resultHandler) 方法来完成执行 Statement 和将 Statement 对象返回的 resultSet 封装成List 

进入到StatementHandler的parameterize(statement) 
```java
public void parameterize(Statement statement) throws SQLException {
    //使用 parameterHandler 对象来完成对statement 的设值
    parameterHandler.setParameters((PreparedStatement) statement);
}
```
进入 setParameters 方法
```java
// Parameterhandler.setParameters方法能够对 PreparedStatement 设置参数
public void setParameters(PreparedStatement ps)
      throws SQLException {
    ErrorContext.instance().activity("setting parameters").object(mappedStatement.getParameterMap().getId());
    List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
    if (parameterMappings != null) {
      MetaObject metaObject = parameterObject == null ? null : configuration.newMetaObject(parameterObject);
      for (int i = 0; i < parameterMappings.size(); i++) {
        ParameterMapping parameterMapping = parameterMappings.get(i);
        if (parameterMapping.getMode() != ParameterMode.OUT) {
          Object value;
          String propertyName = parameterMapping.getProperty();
          PropertyTokenizer prop = new PropertyTokenizer(propertyName);
          if (parameterObject == null) {
            value = null;
          } else if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
            value = parameterObject;
          } else if (boundSql.hasAdditionalParameter(propertyName)) {
            value = boundSql.getAdditionalParameter(propertyName);
          } else if (propertyName.startsWith(ForEachSqlNode.ITEM_PREFIX)
              && boundSql.hasAdditionalParameter(prop.getName())) {
            value = boundSql.getAdditionalParameter(prop.getName());
            if (value != null) {
              value = configuration.newMetaObject(value).getValue(propertyName.substring(prop.getName().length()));
            }
          } else {
            value = metaObject == null ? null : metaObject.getValue(propertyName);
          }
          //每一个Mapping 都有一个typehandler 根据 typehandler 来对preparStatement进行参数设置
          TypeHandler typeHandler = parameterMapping.getTypeHandler();
          if (typeHandler == null) {
            throw new ExecutorException("There was no TypeHandler found for parameter " + propertyName + " of statement " + mappedStatement.getId());
          }
          JdbcType jdbcType = parameterMapping.getJdbcType();
          if (value == null && jdbcType == null) jdbcType = configuration.getJdbcTypeForNull();
          //参数设置
          typeHandler.setParameter(ps, i + 1, value, jdbcType);
        }
      }
    }
}
```
从上述的代码可以看到 StatementHandler.parameterize(Statement) 方法调用了 ParameterHandler.setParameters（Statment）方法，该方法跟我我们输入的参数对 statement 对象的占位符 ？ 进行了赋值 

进入到 StatementHandler.query(Statement statement, ResultHandler resultHandler) 方法的实现
```java
public <E> List<E> query(Statement statement, ResultHandler resultHandler)
throws SQLException {

    // 1. 调用preparedStatement.execute 方法，然后将 resultSet 交给resultHandler 处理
    PreparedStatement ps = (PreparedStatement) statement;
    ps.execute();

    //2. ֵ使用 resultHandler来处理 resultSet
    return resultSetHandler.<E> handleResultSets(ps);

}
```

从上述代码中我们可以看出 Statementhandler 的 List query(Statement statement, ResultHandler resultHandler) 方法的实现是调用了 ResultSetHandler的handleResultSets(Statement) 方法。

ResultSetHandler 的 handleResultSets(Statement) 方法会将 statement 语句执行后的resultSet 结果集转换成List 结果集

## Mapper代理方式

```java
public static void main(String[] args) {
    //前三部相同
    InputStream inputStream =
    Resources.getResourceAsStream("sqlMapConfig.xml");
    SqlSessionFactory factory = new
    SqlSessionFactoryBuilder().build(inputStream);
    SqlSession sqlSession = factory.openSession();
    //这里不是调用sqlSession的API，而是获得接口对象，调用接口中的方法
    UserMapper mapper = sqlSession.getMapper(UserMapper.class);
    List<User> list = mapper.getUserByName("tom");
}
```

mybatis 在初始化时对接口有着特殊的处理：MyapperRegistry 是ConfigUration 中的一个属性，它内部维护着一个 Map 用于存放mapper接口的工厂类，每个接口对应一个工厂类诶，mappers中可以配置接口的包路径，或者某个具体的接口类
```xml
<mappers>
    <mapper class="com.lagou.mapper.UserMapper"/>
    <package name="com.lagou.mapper"/>
</mappers>
```

当解析 mappers 标签时，它会判断解析到的是mapper文件，会将对应配置文件的中的增删改查一一封装成 MappedStatement 对象，存入集合，当判断解析到接口时会建立此接口对应的 MapperProxyFactory 对象，存入Map， key = 接口的字节码对象，value = MapperProxyFactory对象

### 源码解析

进入sqlSession.getMapper(UserMapper.class)中

```java
//DefaultSqlSession 
public <T> T getMapper(Class<T> type) {
    return configuration.<T>getMapper(type, this);
}

//从configuration 中取得对应的 Mapper
public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
    return mapperRegistry.getMapper(type, sqlSession);
}

//MapperRegistry 中的 getMapper
public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
    if (!knownMappers.contains(type))
        throw new BindingException("Type " + type + " is not known to the MapperRegistry.");
    try {
        //通过代理工厂 生成 实例
        return MapperProxy.newMapperProxy(type, sqlSession);
    } catch (Exception e) {
        throw new BindingException("Error getting mapper instance. Cause: " + e, e);
    }
}

//MapperProxy
public static <T> T newMapperProxy(Class<T> mapperInterface, SqlSession sqlSession) {
    ClassLoader classLoader = mapperInterface.getClassLoader();
    Class<?>[] interfaces = new Class[]{mapperInterface};
    MapperProxy proxy = new MapperProxy(sqlSession);
    //通过JDK代理生成实例
    return (T) Proxy.newProxyInstance(classLoader, interfaces, proxy);
}
```

invoke() 解析

在动态代理返回了示例之后，我们就可以直接调用 mapper 类中的方法，但我们知道JDK代理执行的是代理 代理类的 invoke方法，Mapper 的代理类就是 MapperProxy 。


```java
public Object invoke(Object proxy, Method method, Object[] args) throwsThrowable {
    try {
    // 如果是Object 定义的方法直接调用
    if (Object.class.equals(method.getDeclaringClass())) {
    return method.invoke(this, args);
    } else if (isDefaultMethod(method)) {
    return invokeDefaultMethod(proxy, method, args);
    }
    } catch (Throwable t) {
    throw ExceptionUtil.unwrapThrowable(t);
    }
    // ឴获得 MapperMethod 对象
    final MapperMethod mapperMethod = cachedMapperMethod(method);
    // 重点 MapperMethod๋ 调用了最终执行的方法
    return mapperMethod.execute(sqlSession, args);
}
```

进入 executor 方法

```java
public Object execute(Object[] args) {
    Object result = null;
    //判断 mapper 中的方法 类型，最终调用的 还是sqlSession 中的方法
    if (SqlCommandType.INSERT == type) {
        //转换参数
        Object param = getParam(args);
        //执行insert操作
        result = sqlSession.insert(commandName, param);

    } else if (SqlCommandType.UPDATE == type) {

        //转换参数
        Object param = getParam(args);
        //执行 update 操作
        result = sqlSession.update(commandName, param);
    } else if (SqlCommandType.DELETE == type) {

        //转换参数
        Object param = getParam(args);
        //执行 delete 操作
        result = sqlSession.delete(commandName, param);
    } else if (SqlCommandType.SELECT == type) {

        if (returnsVoid && resultHandlerIndex != null) {
            //没有返回，并且有 resultHandler 参数，则将查询结果交给ResultHandler 进行处理
            executeWithResultHandler(args);
        } else if (returnsMany) {
            //执行查询返回列表
            result = executeForMany(args);
        } else if (returnsMap) {
            //执行查询返回map
            result = executeForMap(args);
        } else {

            //转换参数
            Object param = getParam(args);
            //查询单条数据
            result = sqlSession.selectOne(commandName, param);
        }
    } else {
        throw new BindingException("Unknown execution method for: " + commandName);
    }
    return result;
}
```