# Spring Cloud中的核心组件

Spring cloud 中包含了开发一个完整的微服务系统所需的所有技术组件：注册中心、网关、配置中心、消息处理、负载均衡、熔断器、数据监控等组件。


![20210301143136](http://qiniu.liulei.life/20210301143136.png)


# Spring cloud Netflux Eureka 与服务治理

![20210301143218](http://qiniu.liulei.life/20210301143218.png)

在服务治理场景下，这些组件构成了一个完整的服务注册、发现、到调用的流程


# Spring Cloud Gateway 与服务网关

针对服务网关，Spring Cloud 中提供了 Spring 家族自建的 Spring Cloud Gateway。Spring Cloud Gateway 构建在最新版本的 Spring 5 和响应式编程框架 Project Reactor 之上，提供了非阻塞的 I/O 通信机制。通过提供一系列的谓词（Predicate） 和过滤器（Filter） 的组合，我们可以通过 Spring Cloud Gateway 实现灵活的服务路由。同时，Spring Cloud Gateway 也可以集成前面介绍的 Netfix Hystrix 熔断器，以及服务限流等常见的服务容错机制。

![20210301143640](http://qiniu.liulei.life/20210301143640.png)

当然，我们也可以使用 Netflix 中的 Zuul 来构建服务网关，这是 Spring Cloud 中集成的另一种常见的网关实现机制。

#  Spring Cloud Circuit Breaker 与服务容错

Spring Cloud Circuit Breaker 是对熔断器实现方案的一种抽象。在该组件的内部，Spring Cloud Circuit Breaker 集成了Netfix Hystrix、Resilience4J、Sentinel、Spring Retry这四种熔断器实现工具。

![20210301143706](http://qiniu.liulei.life/20210301143706.png)

对外，它提供了一个一致的 API 供应用程序使用，允许开发人员选择最适合应用程序需求的熔断器实现。熔断器在 Spring Cloud 框架中应用广泛，尤其是在与 Spring Cloud Gateway 等服务网关的集成过程中。

# Spring Cloud Config 与配置中心

微服务架构中，我们通常需要构建一个集中化的配置仓库来保存各种配置信息。同时，我们也需要构建一个配置服务器来访问配置仓库并提供对外的访问入口，如下图所示。

![20210301143736](http://qiniu.liulei.life/20210301143736.png)

在 Spring Cloud 中，集中化配置中心服务器的实现依赖于 Spring Cloud Config，而配置仓库的实现方案除了本地文件系统之外，还支持Git、SVN等常见的版本控制工具。

# Spring Cloud Stream 与事件驱动

Spring Cloud 中的 Spring Cloud Stream 对整个消息发布和消费过程做了高度抽象，并提供了 Source/Sink、Channel 和 Binder 等一系列核心组件，如下图所示。

![20210301144146](http://qiniu.liulei.life/20210301144146.png)

Spring Cloud Stream 中的 Source 组件是真正生成消息的组件，然后消息通过 Channel 传送到 Binder，这里的 Binder 是一个中间层组件，通过 Binder 可以与特定的消息中间件进行通信。在 Spring Cloud Stream 中，目前已经内置集成的消息中间件包括 RabbitMQ 和 Kafka。消息消费者则同样通过 Binder 从消息传递系统中获取消息，消息通过 Channel 将流转到 Sink 组件。

# Spring Cloud Security 与服务安全

我们知道在 Spring 中存在一个用来应对安全需求的Spring Security 框架。对应的，在 Spring Cloud 中也提供了 Spring Cloud Security 专门处理微服务环境下的服务安全访问需求。

微服务架构的安全性本质上是服务访问的安全性，作为 Spring Cloud 中的一员，Spring Cloud Security 是对微服务架构中所面临的安全性问题进行抽象并实现的工具。Spring Cloud Security 具备众多特点，包括基于流行的OAuth2 协议的授权机制，以及基于 Token的资源访问保护机制。

![20210301144212](http://qiniu.liulei.life/20210301144212.png)



# Spring Cloud Sleuth 与链路跟踪

Spring Cloud Sleuth 是 Spring Cloud 的组成部分之一，对于分布式环境下的服务调用链路，我们可以通过Spring Cloud Sleuth自动完成服务调用链路的构建。任何通过 HTTP 端点接收到的请求或使用 RestTemplate 发出的请求都可以被 Spring Cloud Sleuth 自动收集日志，同时它也能无缝支持通过由 API 网关 Zuul 发送的请求，以及基于 Spring Cloud Stream 等消息传递技术发送的请求。并且，正如我们已经了解到的，Spring Cloud Sleuth 也兼容了 Zipkin、HTrace 等第三方工具的应用和集成，从而实现对服务依赖关系、服务调用时序，以及服务调用数据的可视化，如下图所示。

![20210301144244](http://qiniu.liulei.life/20210301144244.png)

# Spring Cloud Contracts 与服务测试

作为 Spring Cloud 中我们将要介绍的最后一个核心组件，服务测试本质上不属于微服务架构的技术组件，而只是一种辅助工具。但对于软件中的任何功能，我们都需要进行测试。对于微服务而言，测试是一个难点，也是经常被忽略的一点，这也是本课程专门把服务测试作为一个专题来讲解的原因所在。通常，我们会使用单元测试和集成测试来分别对一个类的内部，以及数据访问等涉及多个类交互的场景分别进行测试。而在微服务架构中，当不同服务之间进行交互和集成时，测试的关注点就变成如何确保服务定义和协议级别的正确性和稳定性，也就是所谓的端到端测试。

Spring 框架本身就提供了Spring Test 模块来满足单元测试与集成测试的实施需求。但对微服务架构而言，我们将重点介绍特有的消费者驱动的契约测试框架，这种测试解决方案能够有效应对服务与服务之间交互场景下的端到端测试需求。在 Spring Cloud 中，满足这种端到端测试需求的就是 Spring Cloud Contract 框架。Spring Cloud Contract 框架采用了服务桩（Stub） 实现机制来确保特定服务版本的各个服务之间交互过程的正确性，如下所示： 

![20210301144307](http://qiniu.liulei.life/20210301144307.png)