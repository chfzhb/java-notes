# 概述


AOP与OOP相比，传统的OOP开发中代码逻辑是顺序的，在这个过程中会产生一些横切性的问题，而这些横切性的问题与我们的业务逻辑关系不大，解决这些横斜性的问题的代码会散落在各个类中，难以维护。

AOP的思想就是把这些横切性的问题和主业务逻辑进行分离，从而起到解耦的目的。

AOP也叫面向切面编程，指在程序运行期间动态的将某段代码切入到指定方法指定位置进行运行的编程方式。


# SpringAop使用

1. 定义一个业务逻辑类 - 并加入到容器

1. 定义一个切面类 - @Aspect

    切面类决定对业务逻辑进行行为的扩展

    - 前置通知(@Before)：在目标方法运行之前运行

    - 后置通知(@After)：在目标方法运行结束之后运行（无论方法正常结束还是异常结束）

    - 返回通知(@AfterReturning)：在目标方法正常返回之后运行

    - 异常通知(@AfterThrowing)：在目标方法出现异常以后运行

    - 环绕通知(@Around)：动态代理，手动推进目标方法运行（joinPoint.procced()）

1. 在切面类中定义切点 - @Pointcut("execution (public int  com.*(..))")

1. 开启基于注解模式的aop - @EnableAspectJAutoProxy

## 示例

业务逻辑类

```java
@Component
public class Div {
    public int div(int i, int j) {
        return i / j;
    }
}
```

切面类
```java
@Aspect
@Component
public class AOP {

    //抽取公共的切入掉表达式
    @Pointcut("execution (public int  com.aop.Div.*(..))")
    public void pointCut() throws Exception {

    }

    @Before("pointCut()")
    public void logStart(JoinPoint joinPoint) {
        System.out.println(joinPoint.getSignature().getName() +
            "运行，参数列表{" + Arrays.asList(joinPoint.getArgs()) + "}");
    }

    @After("pointCut()")
    public void logEnd(JoinPoint joinPoint) {
        System.out.println(joinPoint.getSignature().getName() +
                "结束");
    }


    @AfterReturning(value = "pointCut()", returning = "result")
    public void logReturn(JoinPoint joinPoint, Object result) {
        System.out.println(joinPoint.getSignature().getName() 
            + "结束，运行结果{" + result + "}");
    }

    @AfterThrowing(value = "pointCut()", throwing = "e")
    public void logReException(JoinPoint joinPoint, Exception e) {
        System.out.println(joinPoint.getSignature().getName() 
            + "异常，异常信息{" + e.toString() + "}");
    }
}
```
开启AOP注解
```java
@EnableAspectJAutoProxy
@ComponentScan("com.aop")
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = 
            new AnnotationConfigApplicationContext(Main.class);

        Div bean = context.getBean(Div.class);
        bean.div(2, 1);
        bean.div(1, 0);
        context.close();
    }
}
```

# AOP原理


## @EnableAspectJAutoProxy


1. 注册AnnotationAwareAspectJAutoProxyCreator

    首先来看一下 `@EnableAspectJAutoProxy` 注解类的实现

    ```java
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @Import(AspectJAutoProxyRegistrar.class)
    public @interface EnableAspectJAutoProxy {}
    ```

    它向容器内注册了一个 `AspectJAutoProxyRegistrar` 类，我们继续看一下它的实现。

    ```java
    class AspectJAutoProxyRegistrar 
        implements ImportBeanDefinitionRegistrar {}
    ```

    这个类继承了 `ImportBeanDefinitionRegistrar` 接口，重写了 registerBeanDefinitions，实现了自定义导入bean的功能。
    ```java
    @Override
    public void registerBeanDefinitions(
            AnnotationMetadata importingClassMetadata
            , BeanDefinitionRegistry registry) {
        //我们看着一句即可
        //如果有必要向容器内注册 `AspectJ自动代理创建器`
        AopConfigUtils
           .registerAspectJAnnotationAutoProxyCreatorIfNecessary(registry);

        AnnotationAttributes enableAspectJAutoProxy =
                AnnotationConfigUtils.attributesFor(
                    importingClassMetadata, EnableAspectJAutoProxy.class);
        if (enableAspectJAutoProxy.getBoolean("proxyTargetClass")) {
            AopConfigUtils.forceAutoProxyCreatorToUseClassProxying(registry);
        }
        if (enableAspectJAutoProxy.getBoolean("exposeProxy")) {
            AopConfigUtils.forceAutoProxyCreatorToExposeProxy(registry);
        }
    }
    ```

    我们继续向下走，直到下面这一行。

    ```java
    static BeanDefinition registerAspectJAnnotationAutoProxyCreatorIfNecessary
        (BeanDefinitionRegistry registry, Object source) {
        return registerOrEscalateApcAsRequired(
            //重点对象
            AnnotationAwareAspectJAutoProxyCreator.class
            , registry, source);
    }
    ```

    这里注册了 `AnnotationAwareAspectJAutoProxyCreator` 对象。

1. 接下来我们来看一下 `AnnotationAwareAspectJAutoProxyCreator` 的类图

    <img src="../../java-notes/img/AnnotationAwareAspectJAutoProxyCreateor-ClassDiagram.jpg">

    我们可以看到它实现了很多类与接口，这里我们重点关注 
  
    `SmartInstantiationAwareBeanPostProcessor`、    

    `BeanFactoryAware`

    这两个接口。通过类图我们还可以看到是 `AbstractAutoProxyCreator` 实现了这两个接口，我们跳到这两个类中查看。

1. 我们先看`BeanFactoryAware` 的接口是如何被实现的。

    在`AbstractAutoProxyCreator`类中 `setBeanFactory`功能如下
    ```java
    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }
    ```
    这里是向 `AbstractAutoProxyCreator` 中注入了一个 `BeanFactory` 实例。
    但是它的子类 `AbstractAdvisorAutoProxyCreator` 重写了该方法

    ```java

    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        super.setBeanFactory(beanFactory);
        if (!(beanFactory instanceof ConfigurableListableBeanFactory)) {
            throw new IllegalArgumentException(
                    "AdvisorAutoProxyCreator requires a 
                    ConfigurableListableBeanFactory: " 
                    + beanFactory);
        }
        //重点
        initBeanFactory((ConfigurableListableBeanFactory) beanFactory);
    }
    ```
    我们来看一下 `initBeanFactory`方法做了什么
    ```java
    void initBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        //这段代码向类中注册了一个检索 BeanFactory 的工具类
        this.advisorRetrievalHelper = 
            new BeanFactoryAdvisorRetrievalHelperAdapter(beanFactory);
    }
    ```
    但是`AnnotationAwareAspectJAutoProxyCreator`重写了`initBeanFactory`方法
    ```java
    @Override
     void initBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        //这里实际上调用了所有父类的方法
        super.initBeanFactory(beanFactory);
        //下面这两个对象，会帮助aspectj构建对象
        if (this.aspectJAdvisorFactory == null) {
            this.aspectJAdvisorFactory = 
                new ReflectiveAspectJAdvisorFactory(beanFactory);
        }
        this.aspectJAdvisorsBuilder =
                new BeanFactoryAspectJAdvisorsBuilderAdapter(beanFactory
                    , this.aspectJAdvisorFactory);
    }
    ```
    

1. 接下来继续看 `SmartInstantiationAwareBeanPostProcessor`的实现


## JDK代理与CGLib