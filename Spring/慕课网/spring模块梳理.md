# Spring简史

在2000年 Rod Johnson 为伦敦金融界，提供咨询业务时所创造的。

<img width="300px" src="../../../java-notes/img/Rod-johnson.jpg">

在这本书中提出了一个问题即：**如何让应用程序能以超出当时大众所惯于接受的易用性和稳定性与J2EE平台上的不同组件合作？**

在早期java开发中，一个应用程序是由一组相互协作的对象完成的，所以除了开发业务逻辑之外，还需要编写代码来实现对象间的相互协作。而spring解决的这个问题。

# Spring的设计初衷

Spring适用于构造java应用程序的轻量级框架

- 可以采用Spring来构造任何程序，而不局限于Web程序

- **轻量级**：最少的侵入，与应用程序低耦合，接入成本低

# Springcore

<img src="../../../java-notes/img/Spring core.jpg">

Spring 架构

<img src="../../../java-notes/img/spring架构.jpg">

- Test模块：是为测试提供支持

- Core Container：容器是Spring的核心部分，而Core Container模块时Spring框架的基础，所有的模块都是构建于该模块之上

    - Beans：它包含访问配置文件，创建和管理Bean，以及进行IOC和DI相关操作的类

    - Core：主要包含Spring的核心工具类

    - Context：是Spring的上下文，也称作IOC容器，通过上下文可以操作容器中的bean

    - SpEL：提供了一个强大的查询表达式语言，用于在运行时查询和操作对象。可用于bean或者属性注入，还支持bean 的方法调用

- AOP：基于JDK动态代理和CGlib实现

- Aspects和instrumentation：为AOP模块提供多种实现方法

- instrumentation：它是AOP的一个资源模块，前面两个模块主要是针对方法级别的，切面编程，而该模块主要支持对象级别的切面编程，它会在Jvm启动时，生成一个代理类，通过代理类在运行时，修改类的字节，从而改变类的功能。其多用于面向有状态的切面编程。

- Messaging：主要集成Message Api和消息协议，可以与各种消息队列集成。


- Data Access/Intergration：Spring提供的数据库访问层

    - JDBC：提供对Jdbc连接的封装功能，简化了JDBC的使用

    - Transactions：Spring对事务的完整封装

    - ORM：

    - JMS：提供对消息队列的支持

- Web：提供对Web的支持

    - WebSocker：

    - WebMVC：

    - webFlux：异步的web框架


# Spring核心模块

- Spring core

    - 包含框架的核心工具类：其他组件都要使用这个包中的类

    - 定义并提供资源的访问方式，如读取解析xml，yml文件等，基于此它为Spring提供了IoC和DI的功能

- Spring-beans：Spring主要面向bean编程 (BOP)

    - bean的定义

    - bean解析

    - bean的创建

    - 该模块最重点的是BeanFactory


- Spring-Context

    - 为Spring提供运行时环境，保存对象的状态

    - 扩展了BeanFactory，为它添加了Bean的生命周期控制、框架事件体系、资源透明化、事件监听、远程访问等功能

    - ApplicationContext是该模块的核心接口，它是BeanFactory的超类。它与BeanFactory的默认延迟加载策略不同，当它加载后会自动对所有的单实例Bean进行实例化以及相关依赖关系的装配使之处于待用状态。

- Spring-aop：最小化的动态代理实现，它使用的 JDK动态代理、Cglib只能运行时织入，仅支持方法级别编织，仅支持方法执行切入点

    - 三种织入切面的方式

        - 编译期Weaving：在Java的编译期采用特殊的编译器(如AspectJ的ajc)，利用相关切面织入到Java类中，进而改变或者增强类的行为

        - 类加载期Weaving：通过特殊的类加载器，在类字节码加载到JVM时，织入切面。

        - 运行期Weaving：一般是采用JDk动态代理或者CGlib进行切面织入

    - SpringAop与AspectJ对比

        <img src="../../../java-notes/img/springaop-aspectJ.jpg">