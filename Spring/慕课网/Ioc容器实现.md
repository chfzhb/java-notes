
# 自研IoC容器思路

1. 编写类并为其设置注解标签

2. 实现容器，将编写的类放置到容器中

3. 在运行时，使用容器管理对象的生命周期：创建、赋值、销毁等

## 框架的最基本功能

我们了解完思路，来看下框架的基础功能

- 解析配置：将相关的框架加载到内存中，存储到固定的数据结构中

- 定位与注册对象：解析配置完成之后，能够获取内存中的配置信息，去定位到目标对象，定位到对象后，需要将对象注册到容器中

- 注入对象：在用户需要使用时，需要提供给用户想要的对象

- 通用的工具类

# IoC容器的实现

容器实现共需要以下几步

1. 定义注解：


1. 使用注解标记目标对象

1. 提取标记的对象

1. 创建容器：并将对象的信息存储到容器中

1. 依赖注入：将对象实例注入到需要的地方


## 自定义注解

1. @Controller：标记controller层组件

1. @Service：标记service层组件

1. @Repository：标记Dao层组件

1. @Component：标记为通用组件


## 提取被标记的对象

要提取被标记的对象需要以下几步

1. 指定范围，获取范围内所有的类 / 遍历所有的类，获取被标记的类并加载进容器中

    1. 我们需要定义一个通过的 `类加载工具`，这个工具需要完成的事情有

        1. 获取到类的加载器

        1. 通过类加载器获取加载的资源信息

        1. 通过不同的资源类型，采用不同的方式获取资源

### 获取项目的类加载器的目的

这是因为我们需要获取项目发布的实际路径，如用户传入 `com.ll` ，我们是无法定位到 class文件所在的位置，所以必须得先拿到真实路径才能加载所有 `class` 文件。


如果传入绝对路径，路径在不同机器间不同，如果项目被打成`war/jar` ，更没法定位到路径，通用的做法是通过项目的类加载器来获取

### 类加载器

<img src="../../../java-notes/img/classLoader-执行过程.png">

`ClassLoader` 提供了一个 `getResource` 方法用于查找特定名称的资源，返回值为`URL`

### 统一资源定位符URL

某个资源的唯一地址，

<img src="../../../java-notes/img/URL-获取资源.png">

常用方法

String getProtocol() - 获取协议名

String getPath() - 获取实例路径，该路径指的是绝对路径，并且文件名也会包含在其中

## 容器的单例模式

因为需要用同一个容器实例，将所有的被标记的目标对象，集中管理起来，所以我们需要一个单例的容器。

常用的单例模式有懒汉、饿汉实现方式，在这种模式下一般，会将构造函数设置为 `private` 


### 反射破坏单例

```java
StarvingSingleton instance = StarvingSingleton.getInstance();
System.out.println(instance);

Constructor<StarvingSingleton> constructor = StarvingSingleton.class.getDeclaredConstructor();
constructor.setAccessible(true);
StarvingSingleton starvingSingleton = constructor.newInstance();
System.out.println(starvingSingleton);
```

输出

```java
demo.pattern.singleton.StarvingSingleton@56ef9176
demo.pattern.singleton.StarvingSingleton@4566e5bd
```

我们可以看到，输出的对象并不一致。

### 序列化破坏单例-todo

```java

```


### 真正线程安全的单例模式-todo

## 容器的组成部分

- 保存 `Class` 对象及其实例的载体

- 容器的加载

- 容器的操作方式

## 容器的加载

- 配置的管理与获取

- 获取指定范围的Class对象

- 依据配置提取Class独享，连同实例一并注入

## 容器的操作方式

涉及到容器的增删改查

- 增加、删除操作

- 根据Class获取对应实例

- 获取所有的Class和实例

- 通过注解来获取被注解标注的Class

- 通过超类获取对应的子类Class以及实例

- 获取容器载体保存class的数量


## 依赖注入

- 定义相关注解

- 实现创建被注解标记的成员变量实例，并将其注入到成员变量中

