# 概述

在学Java SE时，我们学了一个标准类 `java.net.URL`。该类在Java SE 中定位为统一资源定位器，但是它的功能只局限于网络形式发布的资源的查找和定位，然而实际上资源的定义比较广泛，除了网络形式的资源，还有以文件、字节、二进制等形式存在的资源。而且它可以存在任何场所，比如网络、文件系统、应用程序中，所以 java.net.URL的局限性，迫使Spring必须实现自己的资源加载策略。该策略需要满足如下要求：

1. 职能划分清楚：资源的定义和加载应该要有一个明确的界限

1. 统一的抽象：统一的资源定义和资源加载策略。资源加载后要返回统一的抽象给客户端，客户端要对资源进行怎样的处理，应该由抽象资源接口来定。

# 统一资源Resource

`Resource` 接口位于 `org.springframeword.core.io` 包下。是spring 所有资源抽象的访问接口，它继承了`InputStreamSource` 接口。作为所有资源的统一抽象，resource定义了一些通用的方法由子类 `AbstractResource` 提供默认实现。

```java
public interface extends InputStreamSource{

    /**
    * 资源是否存在
    */
    boolean exists();

    /**
    * 资源是否可读
    */
    default boolean isReadable() {
        return true;
    }

    /**
    * 资源所代表的句柄是否被一个 stream 打开了
    */
    default boolean isOpen() {
        return false;
    }

    /**
    * 是否为 File
    */
    default boolean isFile() {
        return false;
    }

    /**
    * 返回资源的 URL 的句柄
    */
    URL getURL() throws IOException;

    /**
    * 返回资源的 URI 的句柄
    */
    URI getURI() throws IOException;

    /**
    * 返回资源的 File 的句柄
    */
    File getFile() throws IOException;

    /**
    * 返回 ReadableByteChannel
    */
    default ReadableByteChannel readableChannel() throws IOException {
        return java.nio.channels.Channels.newChannel(getInputStream());
    }

    /**
    * 资源内容的长度
    */
    long contentLength() throws IOException;

    /**
    * 资源最后的修改时间
    */
    long lastModified() throws IOException;

    /**
    * 根据资源的相对路径创建新资源
    */
    Resource createRelative(String relativePath) throws IOException;

    /**
    * 资源的文件名
    */
    @Nullable
    String getFilename();

    /**
    * 资源的描述
    */
    String getDescription();
}
```

## 子类

<img src="../../java-notes/img/resource-子类-类图.jpg" alt="resource类图">

我们可以看到`resource`有着几种实现：

- **FileSystemResource**：是对 `java.io.File` 类型的资源封装。只要是使用file，基本上也可以使用 `FileSystemResource` 。它支持文件和URL形式，实现了 `WritableResource` 接口，且从Spring frameword5.0，开始， `FileSystemResource` 使用`NIO2` API进行读写操作。

- **ByteArrayResource**：对字节数组提供的封装。如果通过InputStream形式访问该类型的资源，该实现会根据字节数组构造一个对应的ByteArrayInputStream。

- **UrlResource**：对`java.net.URL` 类型资源的封装。内部委派URL进行具体的资源操作

- **ClassPathResource**：`class path` 类型资源的实现。使用给定的Class loader或者给定的Class来加载资源


- **InputStreamResource**：将给定的InputStream作为一种资源的Resource的实现类

## AbstractResource

`org.springframework.core.io.AbstractResource` ，为 `Resource` 接口的默认抽象实现。它实现了 `Resource` 接口的大部分的公共实现，作为 `Resource` 接口中的重中之重。如果要自定义Resource 直接继承AbstractResource，然后根据资源特性重写相应方法即可。


# 统一资源定位ResourceLoader

Spring 将资源的定义和加载分开了，Resource定义了资源，那么资源的加载由ResourceLoader来统一定义。

> ResourceLoader 统一资源加载器，主要用于根据句给定的资源文件地址返回对应Resource

```java
public interface ResourceLoader {

    // CLASSPATH URL 前缀。默认为："classpath:"
    String CLASSPATH_URL_PREFIX = ResourceUtils.CLASSPATH_URL_PREFIX;

    Resource getResource(String location);

    ClassLoader getClassLoader();

}
```

- **getResource(String location)** 方法，根据所提供资源的路径 location 返回 Resource 实例，但是它不确保该 Resource 一定存在，需要调用 Resource#exist() 方法来判断。

    该方法支持以下资源的加载：

    - URL资源：如file:/test.dat

    - ClassPath资源：classpath：A.class

    - 相对路径资源：如WEB-INF/xx.jar

    该方法的主要实现是在其子类 DefaultResourceLoader 中实现。

- **getClassloader()**：返回ClassLoader实例

## 子类结构

作为 Spring 统一的资源加载器，它提供了统一的抽象，具体的实现则由相应的子类来负责实现，其类的类结构图如下：

<img src="../../java-notes/img/ResourceLoader 类图.jpg">


## DefaultResourceLoader

与AbstractResource相似，`org.springframework.core.io.DefaultResourceLoader` 是 `ResourceLoader` 的默认实现。

### 构造函数

它接收`ClassLoader`作为构造函数的参数，或者使用不带参数的构造函数。


- 无参构造：默认的`ClassLoader`为 `Thread.currentThread().getContextClassLoader();`

    ```java
    public DefaultResourceLoader() {
        this.classLoader = ClassUtils.getDefaultClassLoader();
    }
    ```
    使用无参构造时也可以使用`setClassLoader`方法，来指定`ClassLoader`
    ```java
    public void setClassLoader(@Nullable ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
    ```


- 有参构造
    ```java
    public DefaultResourceLoader(@Nullable ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
    ```

还可以使用 `getClassLoader`方法获取 `ClassLoader`

```java
@Override
@Nullable
public ClassLoader getClassLoader() {
    return (this.classLoader != null ? this.classLoader : 
        ClassUtils.getDefaultClassLoader());
}
```

### getResource

`ResourceLoader` 中最核心的方法就是 `getResourceLoader(String location)`，它会根据提供的`location`返回相应的`Resource`。

`DefaultResourceLoader`对这个方法提供了核心实现(它的子类没有实现该方法)。

```java
// DefaultResourceLoader.java

@Override
public Resource getResource(String location) {
    Assert.notNull(location, "Location must not be null");

    // 首先，通过 ProtocolResolver 来加载资源
    //ProtocolResolver 协议解析器是一个set集合，默认为空
    for (ProtocolResolver protocolResolver : this.protocolResolvers) {
        Resource resource = protocolResolver.resolve(location, this);
        if (resource != null) {
            return resource;
        }
    }
    // 其次，以 / 开头，返回 ClassPathContextResource 类型的资源
    if (location.startsWith("/")) {
        return getResourceByPath(location);
    // 再次，以 classpath: 开头，返回 ClassPathResource 类型的资源
    } else if (location.startsWith(CLASSPATH_URL_PREFIX)) {
        return new ClassPathResource(location.substring(CLASSPATH_URL_PREFIX.length()), getClassLoader());
    // 然后，根据是否为文件 URL ，是则返回 FileUrlResource 类型的资源，否则返回 UrlResource 类型的资源
    } else {
        try {
            // Try to parse the location as a URL...
            URL url = new URL(location);
            return (ResourceUtils.isFileURL(url) ? new FileUrlResource(url) : new UrlResource(url));
        } catch (MalformedURLException ex) {
            // 最后，返回 ClassPathContextResource 类型的资源
            // No URL -> resolve as resource path.
            return getResourceByPath(location);
        }
    }
}
```

1. 首先通过`ProtocolResolver` 来加载资源，成功返回`Resource`。

    `ProtocolResolver` - 协议解析器 ，它是一个HashSet默认为空，它需要我们自定义资源解析策略。

1. 其次如果 `location` 以 `/` 开头则调用 `getResourceByPath()` 方法构造 `ClasspathContextResource`类型的资源并返回。

    ```java
    protected Resource getResourceByPath(String path) {
        return new ClassPathContextResource(path, getClassLoader());
    }
    ```
    我们再看一下 `ClassPathContextResource` 构造方法的实现.
    ```java
    public ClassPathContextResource(String path, 
                                    @Nullable ClassLoader classLoader) {
       super(path, classLoader);
    }
    ```
    再看一下super调用
    ```java
    public ClassPathResource(String path, @Nullable ClassLoader classLoader) {
        Assert.notNull(path, "Path must not be null");
        String pathToUse = StringUtils.cleanPath(path);
        if (pathToUse.startsWith("/")) {
            pathToUse = pathToUse.substring(1);
        }
        //将路径赋值给属性
        this.path = pathToUse;
        //将classloader赋值给属性
        this.classLoader = (classLoader != null ? classLoader : 
                ClassUtils.getDefaultClassLoader());
    }
    ```
    所以以`/`开头的路径资源，就是返回一个 `ClasspathContextResource` 对象，这个对象并且没有对资源做出任何处理。

1. 如果`location` 以 `classpath:` 开头，则构造 `ClassPathResource` 类型资源返回

    我们继续来看一下 `ClassPathResource` 构造器
    ```java
    public ClassPathResource(String path, @Nullable ClassLoader classLoader) {
        Assert.notNull(path, "Path must not be null");
        String pathToUse = StringUtils.cleanPath(path);
        if (pathToUse.startsWith("/")) {
            pathToUse = pathToUse.substring(1);
        }
        this.path = pathToUse;
        this.classLoader = (classLoader != null ? classLoader : 
            ClassUtils.getDefaultClassLoader());
    }
    ```
    实际上与`ClasspathContextResource`构造器做了类似的事情，都是对`path`和`ClassLoader`属性赋值。

1. 如果以上都不符合。 尝试构造 `java.net.URL` 对象加载资源，

    1. 如果 `URL` 加载资源时没有抛出异常，判断是否为 fileURL，如果是则构造`FileUrlResource`资源，否则构造出 `UrlResource` 资源

    1. 如果 `URL` 加载资源时抛出异常，则与第二步相同，使用 `getResourcePath(location)` 方法,实现资源加载，构造并返回 `ClassPathContextResource` 对象。

通过以上我们得出 getResource(String location) 共有四种类型返回

1. **ClassPathContextResource**：

    1. `/` 开头的路径资源返回该对象，

    1. 非`/`和`classpath:` 也不是FileUrl类型资源

1. **ClassPathResource**：`classpath:` 开头的资源返回该对象

1. **FileUrlResource**：是FileUrl类型资源返回该对象

1. **UrlResource**：非以上路径开头并且不是FileUrl类型资源返回该对象




### ProtocolResolver


`org.springframework.core.io.ProtocolResolver` ，用户自定义协议资源解决策略，作为 `DefaultResourceLoader` 的 `SPI`：它允许用户自定义资源加载协议，而不需要继承 ResourceLoader 的子类。

在介绍 `Resource` 时，提到如果要实现自定义 `Resource`，我们只需要继承 `AbstractResource` 即可，但是有了 `ProtocolResolver` 后，我们不需要直接继承 `DefaultResourceLoader` ，改为实现 `ProtocolResolver` 接口也可以实现自定义的 `ResourceLoader。`

`ProtocolResolver` 接口，仅有一个方法` Resource resolve(String location, ResourceLoader resourceLoader)` 。代码如下：

```java
/**
 * 使用指定的 ResourceLoader ，解析指定的 location 。
 * 若成功，则返回对应的 Resource 。
 *
 * Resolve the given location against the given resource loader
 * if this implementation's protocol matches.
 * 资源路径
 * @param location the user-specified resource location 
 * 指定的加载器 ResourceLoader
 * @param resourceLoader the associated resource loader 
 * 返回为相应的 Resource
 * @return a corresponding {@code Resource} handle if the given location
 * matches this resolver's protocol, or {@code null} otherwise 
 */
@Nullable
Resource resolve(String location, ResourceLoader resourceLoader);
```

在 `Spring` 中你会发现该接口并没有实现类，它需要用户自定义，自定义的 `Resolver` 如何加入 `Spring` 体系呢？调用 `DefaultResourceLoader#addProtocolResolver(ProtocolResolver)` 方法即可。代码如下：

```java
/**
 * ProtocolResolver 集合
 */
private final Set<ProtocolResolver> protocolResolvers = new LinkedHashSet<>(4);

public void addProtocolResolver(ProtocolResolver resolver) {
    Assert.notNull(resolver, "ProtocolResolver must not be null");
    this.protocolResolvers.add(resolver);
}
```

### DefaultResourceLoader使用示例

```java
Resource fileResource1 = loader.getResource("C:\\a.txt");
System.out.println("fileResource1 is FileSystemResource:" +
    (fileResource1 instanceof FileSystemResource));

Resource fileResource2 = loader.getResource("/a.txt");
System.out.println("fileResource2 is FileSystemResource:" + 
    (fileResource2 instanceof ClassPathResource));


Resource urlResource1 = loader.getResource("file:/a.txt");
System.out.println("urlResource1 is UrlResource:" + 
    (urlResource1 instanceof UrlResource));

Resource urlResource2 = loader.getResource("http://www.baidu.com");
System.out.println("urlResource1 is urlResource:" + 
    (urlResource2 instanceof  UrlResource));
```

输出
```
fileResource1 is FileSystemResource:false
fileResource2 is FileSystemResource:true
urlResource1 is UrlResource:true
urlResource1 is urlResource:true
```

## FileSystemResourceLoader


从上面的实例中我们可以看到，`DefaultResourceLoader` 的 `getResourceByPath` 方法处理一些资源时不是很合适，这时我们可以用 `org.springfreamework.FileSystemResourceLoader`。它继承了`DefaultResourceLoader` ，重写了 `getResource` 方法，可以从文件系统加载资源并返回 `FileSystemResource` 类型。

```java
@Override
protected Resource getResourceByPath(String path) {
    //截掉首 /
    if (path.startsWith("/")) {
        path = path.substring(1);
    }
    //创建 FileSystemContextResource 资源
    return new FileSystemContextResource(path);
}
```


### FileSystemContextResource

`FileSystemContextResource` 是 `FileSystemResourceLoader` 的内部类，它继承了 `FileSystemResource` 类实现了 `ContextResource` 接口。

```java
private static class FileSystemContextResource 
    extends FileSystemResource implements ContextResource {

    //调用父类方法
    public FileSystemContextResource(String path) {
        super(path);
    }

    @Override
    public String getPathWithinContext() {
        return getPath();
    }
}
```
1. 在构造器中，该类调用了`FileSystemResource` 的构造器来构造对象

1. 该类实现了`ContextResource`接口的 `getPathWithinContext` 方法，来表示上下文路径。

### 示例

```java
ResourceLoader loader = new FileSystemResourceLoader();

Resource fileResource1 = loader.getResource("C:\\a.txt");
System.out.println("fileResource1 is FileSystemResource:" 
    + (fileResource1 instanceof FileSystemResource));
```
输出
```
fileResource1 is FileSystemResource:true
```
与`DefaultResourceLoader`示例相比，这次返回的对象是 `FileSystemResource` 类型。


## ClassRelativeResourceLoader

`org.springframwork.core.io.ClassRelativeResourceLoader` 是 `DefaultResourceLoader` 的另一个子类实现。

它的实现和`FileSystemResource` 相似，也是重写 `getResourceByPath(String path)` 方法，返回`ClassRelativeContextResource`资源类型。

代码如下：

```java
public class ClassRelativeResourceLoader extends DefaultResourceLoader {

    //属性
    private final Class<?> clazz;

    //构造函数
    public ClassRelativeResourceLoader(Class<?> clazz) {
        Assert.notNull(clazz, "Class must not be null");
        this.clazz = clazz;
        //设置了传入类对象的ClassLoader
        setClassLoader(clazz.getClassLoader());
    }

    //重写的方法
    @Override
    protected Resource getResourceByPath(String path) {
        return new ClassRelativeContextResource(path, this.clazz);
    }
}
```

ClassRelativeContextResource 类代码如下：
```java
/**
* ClassPathResource that explicitly expresses a context-relative path
* through implementing the ContextResource interface.
*/
private static class ClassRelativeContextResource 
    extends ClassPathResource implements ContextResource {

    private final Class<?> clazz;

    public ClassRelativeContextResource(String path, Class<?> clazz) {
        super(path, clazz);
        this.clazz = clazz;
    }

    @Override
    public String getPathWithinContext() {
        return getPath();
    }

    @Override
    public Resource createRelative(String relativePath) {
        String pathToUse = StringUtils.applyRelativePath(getPath(), relativePath);
        return new ClassRelativeContextResource(pathToUse, this.clazz);
    }
}
```
> `ClassRelativeResourceLoader` 扩展的功能是，可以根据给定的 `class` 所在包或者所在包的子包下加载资源。



## ResourcePatternResolver-接口

`ResourceLoader` 的 `Resource getResource(String location)` 方法，每次只能根据 location 返回一个 `Resource` 。当需要加载多个资源时，我们只能多次调用 `getResource(String location)` 方法。

`org.springframework.core.io.support.ResourcePatternResolver` 是 `ResourceLoader` 的扩展，它支持根据指定的资源路径匹配模式每次返回多个 `Resource` 实例，其定义如下：

```
public interface ResourcePatternResolver extends ResourceLoader {

    String CLASSPATH_ALL_URL_PREFIX = "classpath*:";

    Resource[] getResources(String locationPattern) throws IOException;

}
```

- `ResourcePatternResolver` 在 `ResourceLoader` 的基础上增加了 `getResources(String locationPattern)` 方法，以支持根据路径匹配模式返回多个 `Resource` 实例。

- 同时，也新增了一种新的协议前缀 `classpath*:`，该协议前缀由其子类负责实现。

### PathMatchingResourcePatternResolver

`org.springframework.core.io.support.PathMatchingResourcePatternResolver` 是 `ResourcePatternResolver` 最常用的子类。

`PathMatchingResourcePatternResolver`除了支持`ResourceLoader` 和 新增的 `classpath*:` 前缀外 ，还支持 **Ant风格的路径匹配模式**(类似于 `**/*.xml`)。


### 构造函数

`PathMatchingResourcePatternResolver` 提供了三个构造函数，如下：

```java
/**
 * 内置的 ResourceLoader 资源定位器
 */
private final ResourceLoader resourceLoader;

/**
 * Ant 路径匹配器
 */
private PathMatcher pathMatcher = new AntPathMatcher();

public PathMatchingResourcePatternResolver() {
    this.resourceLoader = new DefaultResourceLoader();
}

public PathMatchingResourcePatternResolver(ResourceLoader resourceLoader) {
    Assert.notNull(resourceLoader, "ResourceLoader must not be null");
    this.resourceLoader = resourceLoader;
}

public PathMatchingResourcePatternResolver(@Nullable ClassLoader classLoader) {
    this.resourceLoader = new DefaultResourceLoader(classLoader);
}
```
- `PathMatchingResourcePatternResolver` 在实例化的时候，可以指定一个`ResourceLoader`，如果不指定就会在内部构造一个默认的`DefaultResourceLoader`

- `PathMatcher` 属性，默认的 `AntPathMatcher` 对象，用于支持 `ant` 类型的路径匹配。


### getResource

```java
@Override
public Resource getResource(String location) {
    return getResourceLoader().getResource(location);
}
```

该方法直接委托给相应的 `ResourceLoader` 来实现。在我们实例化  `PathMatchingResourcePatternResovler` 时如果未指定 `Resourceloader` 参数的情况下，那么在加载资源时，其实就是 `DefaultResourceLoader` 加载资源的过程。


### getResource

```java
@Override
public Resource[] getResources(String locationPattern) throws IOException {
    Assert.notNull(locationPattern, "Location pattern must not be null");
    // 以 "classpath*:" 开头
    if (locationPattern.startsWith(CLASSPATH_ALL_URL_PREFIX)) {
        // 路径包含通配符
        // a class path resource (multiple resources for same name possible)
        if (getPathMatcher().isPattern(locationPattern.substring(CLASSPATH_ALL_URL_PREFIX.length()))) {
            // a class path resource pattern
            return findPathMatchingResources(locationPattern);
        // 路径不包含通配符
        } else {
            // all class path resources with the given name
            return findAllClassPathResources(locationPattern.substring(CLASSPATH_ALL_URL_PREFIX.length()));
        }
    // 不以 "classpath*:" 开头
    } else {
        // Generally only look for a pattern after a prefix here, 
        // 通常只在这里的前缀后面查找模式
        // and on Tomcat only after the "*/" separator for its "war:" protocol. 
        //而在 Tomcat 上只有在 “*/ ”分隔符之后才为其 “war:” 协议
        int prefixEnd = (locationPattern.startsWith("war:") ? locationPattern.indexOf("*/") + 1 :
                locationPattern.indexOf(':') + 1);
        // 路径包含通配符
        if (getPathMatcher().isPattern(locationPattern.substring(prefixEnd))) {
            // a file pattern
            return findPathMatchingResources(locationPattern);
        // 路径不包含通配符
        } else {
            // a single resource with the given name
            return new Resource[] {getResourceLoader().getResource(locationPattern)};
        }
    }
}
```

- 非 `classpath*:` 开头，且路径不包含通配符，直接委托给相应的 `ResourceLoader` 来实现。

- 其他情况，调用 `findAllClassPathResources(...)` 或 `findPathMatchingResources(...)` 方法，返回多个 `Resource` 。下面，我们来详细分析。


### findAllClassPathResources

当 `locationPattern` 以 `classpath*:` 开头但是不包含通配符，则调用 `findAllClassPathResources(...)` 方法加载资源。该方法返回 `classes` 路径下和所有 `jar` 包中的所有相匹配的资源。


```java
protected Resource[] findAllClassPathResources(String location) throws IOException {
    String path = location;
    // 去除首个 /
    if (path.startsWith("/")) {
        path = path.substring(1);
    }
    // 真正执行加载所有 classpath 资源
    Set<Resource> result = doFindAllClassPathResources(path);
    if (logger.isTraceEnabled()) {
        logger.trace("Resolved classpath location [" + location + "] to resources " + result);
    }
    // 转换成 Resource 数组返回
    return result.toArray(new Resource[0]);
}
```

真正执行加载的是在 `doFindAllClassPathResources(...)` 方法，代码如下：

```java
protected Set<Resource> doFindAllClassPathResources(String path) throws IOException {
    Set<Resource> result = new LinkedHashSet<>(16);
    ClassLoader cl = getClassLoader();
    // <1> 根据 ClassLoader 加载路径下的所有资源
    Enumeration<URL> resourceUrls = (cl != null ? cl.getResources(path) : ClassLoader.getSystemResources(path));
    // <2> 遍历 URL 集合，调用 convertClassLoaderURL(URL url) 方法，
    //     将 URL 转换成 UrlResource 对象
    while (resourceUrls.hasMoreElements()) {
        URL url = resourceUrls.nextElement();
        // 将 URL 转换成 UrlResource
        result.add(convertClassLoaderURL(url));
    }
    // <3> 加载路径下得所有 jar 包
    if ("".equals(path)) {
        // The above result is likely to be incomplete, i.e. only containing file system references.
        // We need to have pointers to each of the jar files on the classpath as well...
        addAllClassLoaderJarRoots(cl, result);
    }
    return result;
}
```

`<1>` 处，根据 `ClassLoader` 加载路径下的所有资源。在加载资源过程时，如果在构造 `PathMatchingResourcePatternResolver` 实例的时候如果传入了 `ClassLoader`，则调用该 `ClassLoader` 的 `getResources()` 方法，否则调用 `ClassLoader.getSystemResources(path)` 方法。另外，`ClassLoader#getResources()` 方法，代码如下:
```java
// java.lang.ClassLoader.java
public Enumeration<URL> getResources(String name) throws IOException {
    @SuppressWarnings("unchecked")
    Enumeration<URL>[] tmp = (Enumeration<URL>[]) new Enumeration<?>[2];
    //如果当前父类加载器不为 null ，则通过父类向上迭代获取资源
    if (parent != null) {
        tmp[0] = parent.getResources(name);
    } else {//调用 #getBootstrapResources()
        tmp[0] = getBootstrapResources(name);
    }
    tmp[1] = findResources(name);

    return new CompoundEnumeration<>(tmp);
}
```


`<2>` 处，遍历 `URL` 集合，调用 `convertClassLoaderURL(URL url)` 方法，将 `URL` 转换成 `UrlResource` 对象。代码如下：

```java
protected Resource convertClassLoaderURL(URL url) {
    return new UrlResource(url);
}
```

`<3> `处，若 `path` 为空时，则调用 `addAllClassLoaderJarRoots(...)`方法。该方法主要是加载路径下得所有 `jar` 包.

### findPathMatchingResources
当 `locationPattern` 中包含了通配符，则调用该方法进行资源加载。代码如下：
```java
protected Resource[] findPathMatchingResources(String locationPattern) throws IOException {
    // 确定根路径、子路径
    String rootDirPath = determineRootDir(locationPattern);
    String subPattern = locationPattern.substring(rootDirPath.length());
    // 获取根据路径下的资源
    Resource[] rootDirResources = getResources(rootDirPath);
    // 遍历，迭代
    Set<Resource> result = new LinkedHashSet<>(16);
    for (Resource rootDirResource : rootDirResources) {
        rootDirResource = resolveRootDirResource(rootDirResource);
        URL rootDirUrl = rootDirResource.getURL();
        // bundle 资源类型
        if (equinoxResolveMethod != null && rootDirUrl.getProtocol().startsWith("bundle")) {
            URL resolvedUrl = (URL) ReflectionUtils.invokeMethod(equinoxResolveMethod, null, rootDirUrl);
            if (resolvedUrl != null) {
                rootDirUrl = resolvedUrl;
            }
            rootDirResource = new UrlResource(rootDirUrl);
        }
        // vfs 资源类型
        if (rootDirUrl.getProtocol().startsWith(ResourceUtils.URL_PROTOCOL_VFS)) {
            result.addAll(VfsResourceMatchingDelegate.findMatchingResources(rootDirUrl, subPattern, getPathMatcher()));
        // jar 资源类型
        } else if (ResourceUtils.isJarURL(rootDirUrl) || isJarResource(rootDirResource)) {
            result.addAll(doFindPathMatchingJarResources(rootDirResource, rootDirUrl, subPattern));
        // 其它资源类型
        } else {
            result.addAll(doFindPathMatchingFileResources(rootDirResource, subPattern));
        }
    }
    if (logger.isTraceEnabled()) {
        logger.trace("Resolved location pattern [" + locationPattern + "] to resources " + result);
    }
    // 转换成 Resource 数组返回
    return result.toArray(new Resource[0]);
}
```
上面的代码主要分两步：

1. 确定目录，获取该目录下得所有资源。

1. 在所获得的所有资源后，进行迭代匹配获取我们想要的资源。

在这个方法里面，我们要关注两个方法，一个是 `determineRootDir(String location)` 方法，一个是 `doFindPathMatchingXXXResources(...)` 等方法。

### determineRootDir

`determineRootDir(String location)` 方法，主要是用于确定根路径。代码如下：

```java
protected String determineRootDir(String location) {
    // 找到冒号的后一位
    int prefixEnd = location.indexOf(':') + 1;
    // 根目录结束位置
    int rootDirEnd = location.length();
    // 在从冒号开始到最后的字符串中，循环判断是否包含通配符，
    //    如果包含，则截断最后一个由”/”分割的部分。
    // 例如：在我们路径中，就是最后的ap?-context.xml这一段。
    //       再循环判断剩下的部分，直到剩下的路径中都不包含通配符。
    while (rootDirEnd > prefixEnd && getPathMatcher().isPattern(
            location.substring(prefixEnd, rootDirEnd))) {
        rootDirEnd = location.lastIndexOf('/', rootDirEnd - 2) + 1;
    }
    // 如果查找完成后，rootDirEnd = 0 了，
    //   则将之前赋值的 prefixEnd 的值赋给 rootDirEnd ，也就是冒号的后一位
    if (rootDirEnd == 0) {
        rootDirEnd = prefixEnd;
    }
    // 截取根目录
    return location.substring(0, rootDirEnd);
}
```
<table>
    <tr>
        <th>原路径</th>
        <th>确定根路径</th>
    </tr>
    <tr>
        <td>classpath*:test/cc*/spring-*.xml</td>
        <td>classpath*:test/</td>
    </tr>
    <tr>
        <td>classpath*:test/aa/spring-*.xml</td>
        <td>classpath*:test/aa/</td>
    </tr>
</table>

### doFindPathMatchingXXXResources

`doFindPathMatchingXXXResources(...) `方法，是个泛指，一共对应三个方法：

- doFindPathMatchingJarResources(rootDirResource, rootDirUrl, subPatter)

- doFindPathMatchingFileResources(rootDirResource, subPattern)

- VfsResourceMatchingDelegate.findMatchingResources(rootDirUrl, subPattern, pathMatcher) 

# 小结

- spring提供了Resource h和 ResourceLoader 来统一抽象资源以及定位。使得资源与资源的定位有了一个更加清晰的分界线。并且提供了合适的Default 类，使得自定义实现更加方便和清晰。

- AbstractResource为Resource默认的抽象实现，它对Resource接口欧做了一个统一的实现，子类继承该类后只需要覆盖相应的方法即可，同时对于自定义的Resource我们也是继承该类

- DefaultResourceLoader同样是ResourceLoader的默认实现，在自定义ResourceLoader的时候我们除了可以继承该类，还可以实现ProtocolResolver接口来实现自定义资源加载协议

- DefaultResourceLoader 每次只能返回单一的资源，所以Spring提供了另外一个接口ResourcePatternResolver，该接口提供了根据指定的locationpattern返回多个资源的策略。其子类pathMachingResourcePatternResolver是一个集大成者的ResourceLoader，因为它实现了`Resource getResource(String location)` 方法，也实现了 `Resource[] getResource(String locationPattern)`方法


# 思维导图

<https://gitmind.cn/app/doc/55f409126>

