# 对象发布与逸出

- **发布对象**： 是一个对象能够被当前范围之外的代码所使用

- **对象逸出**： 一种错误的发布。当一个对象还没有完全构造完成时，就使它被其他线程所见


## 发布对象-错误示例

```
@Slf4j
public class UnsafePublish {
    private String[] status = {"a", "b", "c"};

    public String[] getStatus() {
        return status;
    }

    public static void main(String[] args) {
        UnsafePublish p = new UnsafePublish();
        log.info("{}", Arrays.toString(p.getStatus()));
        p.getStatus()[0] = "e";
        log.info("{}", Arrays.toString(p.getStatus()));
    }
}
```
输出
```
20:37:50.523 [main] INFO com.concurrency.publish.UnsafePublish - [a, b, c]
20:37:50.539 [main] INFO com.concurrency.publish.UnsafePublish - [e, b, c]
```

此时 `status` 属性能够被外部程序所操作，此时我们无法肯定 `status` 没有被其他线程所修改，所以可能会发生值错误的情况。


## 对象溢出-示例

```
@Slf4j
public class Escape {
    private int e = 0;

    public Escape() {
        new InnerClass();
    }

    private class InnerClass {
        public InnerClass() {
            log.info("{}", Escape.this.e);
        }
    }

    public static void main(String[] args) {
        new Escape();
    }
}
```
输出
```
20:43:40.038 [main] INFO com.concurrency.publish.Escape - 0
```
这里在对象未完成构造之前被发布，即对象内部可能未被构造完成即被使用，可能造成对象逸出。

## 安全发布对象

- 在静态初始化函数中初始化一个对象引用  
    如 `static` 块中


- 将对象的引用保存到 `volatile` 域或者 `AtomicReference` 中

- 将对象的引用保存到某个正确构造对象的 `final` 域中

- 将对象的引用保存到一个由锁保护的域中