# ThreadLocal

当使用 `ThreadLocal` 维护变量时，`ThreadLocal` 为每个使用该变量的线程提供独立的变量副本，所以每一个线程都可以独立地改变自己的副本，而不会影响其它线程所对应的副本,这是一种以空间换时间的方式。

## 使用方式 
```
public class ThreadLocalDemo {
    private static ThreadLocal<String> local = new ThreadLocal<>();

    public static void main(String[] args) {
        new Thread(() -> {
            local.set("a");
            System.out.println(local.get());

        }, "T1").start();
        new Thread(() -> {
            local.set("b");
            System.out.println(local.get());
        }, "T2").start();
    }
}
```

输出： 
```
a
b
```
在两个线程中，分别向 `local` 使用 `set` 方法赋值，再从两个线程中取出刚赋好的值，两个值互相不影响。

## 使用场景

在 `Java` 的多线程编程中，为保证多个线程对共享变量的安全访问，通常会使用 `synchronized` 来保证同一时刻只有一个线程对共享变量进行操作。这种情况下可以将类变量放到 `ThreadLocal` 类型的对象中，使变量在每个线程中都有独立拷贝，不会出现一个线程读取变量时而被另一个线程修改的现象。

最常见的 `ThreadLocal` 使用场景为用来解决数据库连接、`Session` 管理等。

## 类结构

### 泛型类
`ThreadLocal` 定义类时带有泛型，源码如下：
```
public class ThreadLocal<T> {}
```

### 关键属性

`ThreadLocal` 使用一个自定的 `ThreadLocalMap` 来储存使用线程的值。

> private final int threadLocalHashCode = nextHashCode();


```
//ThreadLocal 使用一个自定义的 Map - ThreadLocalMap 来存储当前线程(使用线程)的值。
//使用threadLocalHashCode进行散列算法运算，可以将哈希码均匀的分布在2de N次方数组中，从而存储在ThreadLocalMap中
private final int threadLocalHashCode = nextHashCode();

// 计算 threadLocalHashCode 的 hashCode 值(就是递增)
private static int nextHashCode() {
    return nextHashCode.getAndAdd(HASH_INCREMENT);
}

//static + AtomicInteger 保证了在一台机器中每个 ThreadLocal 的 threadLocalHashCode 是唯一的
// 被 static 修饰非常关键，因为一个线程在处理业  务的过程中，
//ThreadLocalMap 是会被 set 多个 ThreadLocal 的，
//多个 ThreadLocal 就依靠 threadLocalHashCode 进行区分
private static AtomicInteger nextHashCode = new AtomicInteger();
```




## ThreadLocal原理

