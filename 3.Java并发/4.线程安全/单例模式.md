# 单例模式

单例模式保证一个类仅有一个实例，并提供一个访问它的全局访问点。这种类在程序的运行期中只实例化一次，一般将构造函数设置为私有的。



## 饿汉模式

```
public class HungerySingleton {
    //加载的时候就产生的实例对象
    private static HungerySingleton instance=new HungerySingleton();
    private HungerySingleton(){
    }

    //返回实例对象
    public static HungerySingleton getInstance(){
        return instance;
    }
}
```

**线程安全：**


在加载的时候已经被实例化，是线程安全的

## 懒汉模式
```
public class LazySingleton {
    private static LazySingleton instance = null;

    private LazySingleton() {
    }

    public static LazySingleton getInstance() {
        if (null == instance)
            instance = new LazySingleton();
        return instance;
    }
```

**线程安全：**

这种模式下是非线程安全的，因为在 `instance` 实例化未完成时，第二个线程在判断 `instance` 依然为 `null` 值所以会实例化两次。

**解决方法：**

为 `getInstance` 添加 `synhronized` 修饰，但是这样，就会将方法执行串行化，降低性能，所以应使用其他模式。


## 双重检查模式(Double-Check-Locking)
```
public class DCL {
    private static DCL instance = null;
    private DCL(){
    }
    public static DCL getInstance(){
        if(null==instance)
            synchronized (DCL.class){
                if(null==instance)
                    instance=new DCL();
            }
        return instance;
    }
}
```

**线程安全：**

这种方式依然是非线程安全的，虽然已经使用 `synchronized` 关键字来同步代码块，但是。`instance` 的 `new` 操作会经历三个过程
1. 分配内存
1. 执行Singleton构造函数
1. 将实例指向分配的内存

由于是`synchronized`修饰的，所以不保证操作的顺序，只保证原子性.
如果按照 1-> 3 -> 2去执行的话，当线程A执行完3的时候，`instance` 是有值了，但是还没执行 `DCL` 构造函数初始化实例，然后线程B在`synchronized`外面的if判断时，由于`instance`非空，所以直接返回，获取到一个没有实例化完成的对象.

**解决方法：**

既然是由于 `jvm` 可能发生的指令重排产生的错误，那么使用 `volatile` 修饰 `instance` 即可禁止指令重排，从而避免获取到没有实例化完成的对象。

```
private static volatile DCL instance = null;
```
## 静态内部类模式-推荐

```
public class HolderDemo {
    private static class Holder{
        private static HolderDemo instance=new HolderDemo();
    }

    public static HolderDemo getInstance(){
        return Holder.instance;
    }
}
```

**线程安全：**
该种方式是线程安全的，并且由于 `java` 的静态内部类的机制(在被调用时才初始化的机制)，是懒加载模式的。

## 枚举

```
class EnumSingleton {
    private EnumSingleton() {
    }

    public static EnumSingleton getInstance() {
        return Singleton.INSTANCE.getInstance();
    }

    private enum Singleton {
        INSTANCE;
        private EnumSingleton singleton;

        //JVM会保证此方法绝对只调用一次
        Singleton() {
            singleton = new EnumSingleton();
        }

        public EnumSingleton getInstance() {
            return singleton;
        }
    }
}
```


## 序列化破坏单例

<https://blog.csdn.net/qq_34203492/article/details/84326571>

## 反射攻击破坏单例

<https://blog.csdn.net/qq_34203492/article/details/84336492>