# 创建线程的方式

## 继承Thread类
`Thread` 类本质上是实现了 `Runnable` 接口的一个实例，代表一个线程的实例。 启动线程的唯一方法就是通过 `Thread` 类的 `start()` 实例方法。 `start()` 方法是一个 `native` 方法，它将启动一个新线程，并执行 `run()`方法
```
public class MyThread extends Thread {

    @Override
    public void run() {
        System.out.println("MyThread.run()");
    }

    public static void main(String[] args) {
        new MyThread().start();
    }
}
```
输出
```
MyThread.run()
```



## 实现Runnable接口
如果自己的类已经 extends 另一个类，就无法直接 extends Thread，此时，可以实现一个Runnable 接口。
```
public class RunnableDemo implements Runnable {
    @Override
    public void run() {
        System.out.println("RunnableDemo.run() ");
    }

    public static void main(String[] args) {
        new Thread(new RunnableDemo()).start();
    }
}
```
输出
```
RunnableDemo.run()
```

## 有返回值的线程

有返回值的任务必须实现 `Callable` 接口，类似的，无返回值的任务必须 `Runnable` 接口。执行`Callable` 任务后，可以获取一个 `Future` 的对象，在该对象上调用 `get` 就可以获取到 `Callable` 任务返回的 `Object` 了，再结合线程池接口 `ExecutorService` 就可以实现有返回结果的多线程了。

```
public class ExecutorServiceDemo {

    public static void main(String[] args) throws   
                ExecutionException, InterruptedException {

        ExecutorService s = Executors.newCachedThreadPool();

        Future<String> submit = s.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "callable";
            }
        });
        String r = submit.get();
        System.out.println("r = " + r);
    }
}
```
输出
```
r = callable
```

## 基于线程池的方式

线程和数据库连接这些资源都是非常宝贵的资源。那么每次需要的时候创建，不需要的时候销毁，是非常浪费资源的。那么我们就可以使用缓存的策略，也就是使用线程池。