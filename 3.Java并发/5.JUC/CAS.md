# CAS

CAS 全称为 Compare and Swap ，中文名为比较并交换。属于乐观锁操作。

CAS包含三个操作数: 内存位置(V)、预期值(A)、新值(B)。更新操作如下：

- 将内存位置的值与预期值比较，如果相匹配，则将内存位置的值更新为新值B

- CAS失败时则由开发者决定是继续尝试，还是执行别的操作。操作失败的线程并不会被挂起

使用场景：

由于CAS支持原子更新操作，适用于**计数器**，**序列发生**的场景

## Java中的CAS类

Java中的原子类在 `java.util.concurrent.atomic` 包下，如下：
<img src="../../../java-notes/img/JUC-原子类.png">

我们以 `AtomicInteger` 为例，看一下常用方法：

- set() - 设置值
- addAndGet()- 以原子方式将给定值添加到当前值，并在添加后返回新值。
- getAndAdd() - 以原子方式将给定值添加到当前值并返回旧值。
- incrementAndGet()- 以原子方式将当前值递增1并在递增后返回新值。它相当于i ++操。
- getAndIncrement() - 以原子方式递增当前值并返回旧值。它相当于++ i操作。
- decrementAndGet()- 原子地将当前值减1并在减量后返回新值。它等同于i-操作。
- getAndDecrement() - 以原子方式递减当前值并返回旧值。它相当于-i操作。


## CAS原理

下面将从几个层面分析CAS是如何实现的。


### 代码层面

以AtomicInteger为例：


AtomicInteger属性:
```
//使用 Unsafe 类为基础，原子更新数据
private static final Unsafe unsafe = Unsafe.getUnsafe();
//由于Unsafe使用的是堆外内存必须设置偏移量 - 忽略即可
private static final long valueOffset;

static {
    try {
        valueOffset = unsafe.objectFieldOffset
            (AtomicInteger.class.getDeclaredField("value"));
    } catch (Exception ex) { throw new Error(ex); }
}
//该变量为AtomicInteger操作的值
private volatile int value;
```
AtomicInteger的incrementAndGet方法：
```
public final int incrementAndGet() {
    //实际上在这里循环调用了unsafe的compareAndSwapInt方法进行原子更新
    for (;;) {
        int current = get();
        int next = current + 1;
        if (compareAndSet(current, next))
            return next;
    }
}

public final boolean compareAndSet(int expect, int update) {
    return unsafe.compareAndSwapInt(this, valueOffset, expect, update);
}
```
unsafe类的比较并交换方法，它是由native修饰的，是由c++实现的方法。
```
public final native boolean compareAndSwapInt(Object var1, long var2, int var4, int var5);
```



### JVM层面

jdk8u: unsafe.cpp:

cmpxchg = compare and exchange
```
UNSAFE_ENTRY(jboolean, Unsafe_CompareAndSwapInt(JNIEnv *env, 
        jobject unsafe, jobject obj, jlong offset, jint e, jint x))
  UnsafeWrapper("Unsafe_CompareAndSwapInt");
  oop p = JNIHandles::resolve(obj);
  jint* addr = (jint *) index_oop_from_field_offset_long(p, offset);
  return (jint)(Atomic::cmpxchg(x, addr, e)) == e;
UNSAFE_END
```
jdk8u: atomic_linux_x86.inline.hpp

is_MP = Multi Processor  

拼接CPU指令 
```
inline jint  Atomic::cmpxchg  (jint  exchange_value,
     volatile jint* dest, jint     compare_value) {
  int mp = os::is_MP();
  __asm__ volatile (LOCK_IF_MP(%4) "cmpxchgl %1,(%3)"
        : "=a" (exchange_value)
        : "r" (exchange_value), "a" (compare_value), "r" (dest), "r" (mp)
        : "cc", "memory");
  return exchange_value;
}
```
jdk8u: os.hpp is_MP()  
如果是多核CPU则加上Lock指令

```
  static inline bool is_MP() {
    return (_processor_count != 1) || AssumeMP;
  }
```
jdk8u: atomic_linux_x86.inline.hpp
```
#define LOCK_IF_MP(mp) "cmp $0, " #mp "; je 1f; lock; 1: "
```
所以一般在多核CPU上一般调用的是 `lock cmpxchg` 指令



### CPU层面

CAS在底层的实现是CPU的一条原子指令，在`Intel`的cpu中使用的是 `cmpxchg` 指令。在`JVM`层面实现方式是基于硬件平台的汇编指令。

对于多核CPU/多CPU，保证单条指令并不能解决并行问题，因为多个核心可能同时运行同一个赋值语句。所以对CAS操作加锁,锁的种类如下：

1. **总线锁** 

    总线锁定其实就是处理器使用了总线锁，所谓总线锁就是使用处理器提供的一个 `Lock`  信号，当一个处理器在总线上输出此信号时，其他处理器的请求将被阻塞住，那么该处理器可以独占共享内存。但是该方法成本太大。因此有了下面的方式。

2. **缓存锁**  

    所谓 缓存锁定 是指内存区域如果被缓存在处理器的缓存行中，并且在 `Lock` 操作期间被锁定，那么当他执行锁操作写回到内存时，处理器不在总线上声言 `Lock`# 信号，而时修改内部的内存地址，并允许他的缓存一致性机制来保证操作的原子性，因为缓存一致性机制会阻止同时修改两个以上处理器缓存的内存区域数据（这里和 `volatile` 的可见性原理相同），当其他处理器回写已被锁定的缓存行的数据时，会使缓存行无效。

所以实际上`CAS`操作在`CPU`级别执行的是 `lock cmpxchg` 指令。

## ABA问题

CAS需要检查操作值有没有发生改变，如果没有发生改变则更新。但是存在这样一种情况：如果一个值原来是A，变成了B，然后又变成了A，那么在CAS检查的时候会发现没有改变，但是实质上它已经发生了改变，这就是所谓的ABA问题。对于ABA问题其解决方案是加上版本号，即在每个变量都加上一个版本号，每次改变时加1，即A —> B —> A，变成1A —> 2B —> 3A。

### Java示例
```
public static void main(String[] args) {

    //初始值为1 ，版本号1        
    AtomicStampedReference<Integer> a = new AtomicStampedReference<>(1, 1);

    //更新，原值1，新值10，原版本号1，新版本号2        
    boolean b = a.compareAndSet(1, 10,
            1, 2);
    System.out.println("b = " + b);

    //获取值        
    Integer reference = a.getReference();
    System.out.println("reference = " + reference);

    //获取版本号        
    int stamp = a.getStamp();
    System.out.println("stamp = " + stamp);
}
```